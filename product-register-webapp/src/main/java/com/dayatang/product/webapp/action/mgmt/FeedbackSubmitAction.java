package com.dayatang.product.webapp.action.mgmt;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.webapp.action.BaseAction;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@ParentPackage(value = "web")
public class FeedbackSubmitAction extends BaseAction {

	private static final long serialVersionUID = -711524009209730412L;

	private String help;
	private String easy;
	private String beautiful;
	private String problem;
	private String wholeEvaluate;
	private String suggestion;
	private String companyName;
	private String email;
	private String phone;
	private String address;
	private String mark;
	private boolean result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		String url = BASEURL + "feedback-information";
		if (mark.equals("cpms") && suggestion.length() < 255) {
			MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			queryParams.add("help", help);
			queryParams.add("easy", easy);
			queryParams.add("beautiful", beautiful);
			queryParams.add("problem", problem);
			queryParams.add("wholeEvaluate", wholeEvaluate);
			queryParams.add("suggestion", suggestion);
			queryParams.add("companyName", companyName);
			queryParams.add("email", email);
			queryParams.add("phone", phone);
			queryParams.add("address", address);
			queryParams.add("mark", "cpms");
			getWebServicesResult(url, queryParams).getEntity(String.class);
			result = true;
			return SUCCESS;
		}
		result = false;
		return SUCCESS;
	}
	
	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public String getEasy() {
		return easy;
	}

	public void setEasy(String easy) {
		this.easy = easy;
	}

	public String getBeautiful() {
		return beautiful;
	}

	public void setBeautiful(String beautiful) {
		this.beautiful = beautiful;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getWholeEvaluate() {
		return wholeEvaluate;
	}

	public void setWholeEvaluate(String wholeEvaluate) {
		this.wholeEvaluate = wholeEvaluate;
	}

	public String getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
	
}


package com.dayatang.product.webapp.action.mgmt;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class CheckTrialCodeAction extends BaseAction {

	private static final long serialVersionUID = 3167565843195117361L;

	private String trialCode;
	
	private String result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		String url = BASEURL + "check-trial-code/" + trialCode;
		result = getWebServicesResult(url, null).getEntity(String.class);
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(String trialCode) {
		this.trialCode = trialCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}

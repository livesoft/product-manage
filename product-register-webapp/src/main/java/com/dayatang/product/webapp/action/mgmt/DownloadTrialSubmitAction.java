package com.dayatang.product.webapp.action.mgmt;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.webapp.action.BaseAction;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * 文件名:DownloadTrialSubmitAction.java
 * 描述:提交申请试用信息
 * 创建时间:2012-6-4 下午10:36:30
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class DownloadTrialSubmitAction extends BaseAction {

	private static final long serialVersionUID = -3888781584923463158L;

	private String companyName;
	private String phone;
	private String contact;
	private String email;
	private String address;
	private String mobile;
	private String productId;
	private String trench;
	private boolean result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		String url = BASEURL + "apply-trial";
		if (companyName != null && phone != null && contact != null && email != null && mobile != null) {
			MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			queryParams.add("productId", productId);
			queryParams.add("companyName", companyName);
			queryParams.add("phone", phone);
			queryParams.add("contact", contact);
			queryParams.add("email", email);
			queryParams.add("address", address);
			queryParams.add("mobile", mobile);
			queryParams.add("trench", trench);
			getWebServicesResult(url, queryParams).getEntity(String.class);
			result = true;
			return SUCCESS;
		}
		result = false;
		return SUCCESS;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getTrench() {
		return trench;
	}

	public void setTrench(String trench) {
		this.trench = trench;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}

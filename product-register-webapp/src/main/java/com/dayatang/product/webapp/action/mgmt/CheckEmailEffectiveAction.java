package com.dayatang.product.webapp.action.mgmt;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.action.util.EmailEffectiveUtil;

@ParentPackage(value = "web")
public class CheckEmailEffectiveAction extends BaseAction {

	private static final long serialVersionUID = 3641274025167104762L;

	private String email;
	
	private String result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (EmailEffectiveUtil.emailIsEffective(email)) {
			result = "ok";
		} else {
			result = "error";
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}

package com.dayatang.product.webapp.action.common;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class CheckVerifyCode extends BaseAction {

	private static final long serialVersionUID = -9059406877686543814L;

	private String code;
	
	private int result;
	
	private String sessionKey;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (code.equals(getSessinoValue(sessionKey))) {
			result = 1;
		} else {
			result = 0;
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@JSON(serialize = false)
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
}

package com.dayatang.product.webapp.action.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.webapp.action.util.SecurityImageUtil;
import com.opensymphony.xwork2.ActionSupport;

public class RandomPictureAction extends ActionSupport {

	private static final long serialVersionUID = 5378609656791595210L;

	private ByteArrayInputStream inputStream;
	
	private String sessionKey;

	@Action(results = {
			@Result(name = "success", type = "stream", params = {"inputName", "inputStream","contentType","image/jpeg"})
			})
	public String execute() throws Exception {
		BufferedImage image = SecurityImageUtil.getRandImage(sessionKey);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageOutputStream imageOut = ImageIO.createImageOutputStream(output);
		ImageIO.write(image, "JPEG", imageOut);
		imageOut.close();
		ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
		this.setInputStream(input);
		return SUCCESS;
	}

	public void setInputStream(ByteArrayInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ByteArrayInputStream getInputStream() {
		return inputStream;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
}

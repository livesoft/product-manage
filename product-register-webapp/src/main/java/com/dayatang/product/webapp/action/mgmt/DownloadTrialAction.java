package com.dayatang.product.webapp.action.mgmt;

import java.util.HashMap;
import java.util.Map;

import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:DownloadTrialAction.java
 * 描述:下载试用
 * 创建时间:2012-6-4 上午11:39:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public class DownloadTrialAction extends BaseAction {

	private static final long serialVersionUID = 9210488498774742515L;

	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public Map<Long,String> getProductMaps() {
		Map<Long,String> results = new HashMap<Long,String>();
		results.put(1L, "通信工程管理系统");
		return results;
	}
	
}



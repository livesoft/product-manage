package com.dayatang.product.webapp.action.mgmt;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.webapp.action.BaseAction;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@ParentPackage(value = "web")
public class RegisterInfoAction extends BaseAction {

	private static final long serialVersionUID = 1388503024703116174L;

	private String trialCode;
	private String companyName;
	private String phone;
	private String contact;
	private String email;
	private String address;
	private String mobile;
	private boolean result;

	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		String url = BASEURL + "get-register-code";
		if (checkTrialCode(trialCode) && companyName != null && phone != null && contact != null && email != null && address != null && mobile != null) {
			MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
			queryParams.add("trialCode", trialCode);
			queryParams.add("companyName", companyName);
			queryParams.add("phone", phone);
			queryParams.add("contact", contact);
			queryParams.add("email", email);
			queryParams.add("address", address);
			queryParams.add("mobile", mobile);
			try {
				getWebServicesResult(url, queryParams).getEntity(String.class);
				result = true;
			} catch (Exception e) {
				result = false;
			}
		} else {
			result = false;
		}
		return SUCCESS;
	}

	public void setTrialCode(String trialCode) {
		this.trialCode = trialCode;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	private boolean checkTrialCode(String trialCode) {
		String url = BASEURL + "check-trial-code/" + trialCode;
		String result = getWebServicesResult(url, null).getEntity(String.class);
		if (result.equals("ok")) {
			return true;
		} else {
			return false;
		}
	}
	
}

package com.dayatang.product.webapp.action;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = -8076768072825288275L;
	
	protected static final String CHAREST = "UTF-8";
	protected static final String ERROR = "error";
	
	protected static final String BASEURL = "http://www.dayatang.com:8080/product-manage/webservices/";
	// protected static final String BASEURL = "http://localhost:8888/product-manage/webservices/";
	
	protected URI getURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	protected ClientResponse getWebServicesResult(String url, MultivaluedMap<String, String> queryParams) {
		ClientConfig config = new DefaultClientConfig();  
		Client client = Client.create(config);
		WebResource resource = client.resource(getURI(url));
		if (queryParams == null) {
			return resource.type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
		}
        return resource.queryParams(queryParams).type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
	}
	
	protected String getSessinoValue(String sessionKey) {
		String sessionValue = (String) ActionContext.getContext().getSession().get(sessionKey);
		return  sessionValue == null ? "" : sessionValue;
	}
	
	protected void setSessiolnValue(String sessionKey, String sessionValue) {
		ActionContext.getContext().getSession().put(sessionKey, sessionValue);
	}
}

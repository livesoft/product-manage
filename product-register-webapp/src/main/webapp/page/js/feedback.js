function changeValidateCode(obj) {
	var currentTime = new Date().getTime();
	obj.src = "../common/random-picture.action?sessionKey=rand&d=" + currentTime;
}

$(function(){
	
	$("#feedbackForm").submit(function(){
		
		var applicability= $('input:radio[name="applicability"]:checked').val();
		var usability= $('input:radio[name="usability"]:checked').val();
		var capability= $('input:radio[name="capability"]:checked').val();
		var companyName = $("#companyName").val();
		var email = $("#email").val();
		var code = $("#code").val();
		
		if (applicability == null || usability == null || capability == null)
		{
			$("#alertInfo").text("*请填写使用反馈");
			return false;
		}
		else if (companyName == "")
		{
			$("#alertInfo").text("*请填写公司名称");
			return false;
		}
		else if (email == "")
		{
			$("#alertInfo").text("*请填写邮箱");
			return false;
		}
		else if (code == "")
		{
			$("#alertInfo").text("*请填写验证码");
			return false;
		}
		else
		{
			checkCode(code);
			if ($("#alertInfo").text() != "")
			{
				return false;
			}
		}
		return true;
	});
	
});

function cleanInfo()
{
	$("#alertInfo").text("");
}

function checkCode(code)
{
	$.ajax({
		 type: "POST",	   	   
		 async: false,
		 data: {"code" : code, "sessionKey" : "rand"},
		 url: "../common/check-verify-code.action",
		 success: function(data){
			 if (data.result == 0) {
				$("#alertInfo").text("*验证码错误");
			 }
		 }
	});
}
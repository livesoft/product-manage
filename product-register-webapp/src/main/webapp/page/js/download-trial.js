	$(function(){
		$("#companyName").blur(function(){
			CheckCompanyName();
		});
		
		$("#companyName").focus(function(){
			$("#companyDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#phone").blur(function(){
			CheckPhone();
		});
		
		$("#phone").focus(function(){
			$("#phoneDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#contact").blur(function(){
			CheckContact();
		});
		
		$("#contact").focus(function(){
			$("#contactDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#mobile").blur(function(){
			CheckMobile();
		});
		
		$("#mobile").focus(function(){
			$("#mobileDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#email").blur(function(){
			CheckEmail();
		});
		
		$("#email").focus(function(){
			$("#emailDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#code").blur(function(){
			CheckCode();
		});
		
		$("#code").focus(function(){
			$("#codeDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
		
	$(function(){
		$("#registerBtn").click(function(){
			if (CheckCompanyName() == true && CheckPhone() == true && CheckContact() == true && CheckMobile() == true && CheckEmail() == true && CheckCode() == true)
			{
				return true;
			}
			else
			{
				$("#alertDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写试用信息</font>");
				return false;
			}
		});
		
	});
		
	function CheckCompanyName()
	{
		var companyName = $("#companyName").val();
		if (companyName != "")
		{
			 $("#companyDiv").html("<img src='../page/images/icon_right.gif'>");
			 return true;
		}
		else
		{
			$("#companyDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写公司名称</font>");
			return false;
		}
	}

	function CheckPhone()
	{
		var phone = $("#phone").val();
		var isPhone=/^((0\d{2,3})-)?(\d{7,10})(-(\d{3,}))?$/;
		if (phone != "")
		{
			if (isPhone.test(phone))
			{
				$("#phoneDiv").html("<img src='../page/images/icon_right.gif'>");
				return true;
			}
			else
			{
				$("#phoneDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如020-xxxxxxxx</font>");
				return false;
			}
		}
		else
		{
			$("#phoneDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写电话号码</font>");
			return false;
		}
	}
	
	function CheckContact()
	{
		var contact = $("#contact").val();
		if (contact != "")
		{
			$("#contactDiv").html("<img src='../page/images/icon_right.gif'>");
			 return true;
		}
		else
		{
			$("#contactDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写联系人</font>");
			return false;
		}
	}
	
	function CheckMobile()
	{
		var mobile = $("#mobile").val();
		var isMobile=/^(?:1\d\d|15\d)\d{5}(\d{3}|\*{3})$/;   
		if (mobile != "")
		{
			if (isMobile.test(mobile))
			{
				$("#mobileDiv").html("<img src='../page/images/icon_right.gif'>");
				return true;
			}
			else
			{
				$("#mobileDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如137xxxxxxxx</font>");
				return false;
			}
		}
		else
		{
			$("#mobileDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写手机号码</font>");
			return false;
		}
	}
	
	function CheckEmail()
	{
		var email = $("#email").val();
		if (email != "")
		{
			if (email.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/))
			{
				$("#emailDiv").html("<img src='../page/images/icon_right.gif'>");
				return true;
			}
			else
			{
				$("#emailDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如xxxxx@xxx.com</font>");
				return false;
			}
		}
		else
		{
			$("#emailDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写邮箱</font>");
			return false;
		}
	}
	
	function changeValidateCode(obj) {
		var currentTime = new Date().getTime();
		obj.src = "../common/random-picture.action?sessionKey=downloadTrial&d=" + currentTime;
	}

	function CheckCode()
	{
		var code = $("#code").val();
		if (code != "")
		{
			checkCode(code,"downloadTrial");
			if ($("#codeDiv").text() != "")
			{
				$("#codeDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>验证码错误</font>");
				return false;
			}
			else
			{
				$("#codeDiv").html("<img src='../page/images/icon_right.gif'>");
				 return true;
			}
		}
		else
		{
			$("#codeDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写验证码</font>");
			return false;
		}
	}
	
function cleanInfo()
{
	$("#alertInfo").text("");
}

function checkCode(code,sessionKey)
{
	$.ajax({
		 type: "POST",	   	   
		 async: false,
		 data: {"code" : code, "sessionKey" : sessionKey},
		 url: "../common/check-verify-code.action",
		 success: function(data){
			 if (data.result == 0) {
				$("#codeDiv").text("*验证码错误");
			 }
		 }
	});
}
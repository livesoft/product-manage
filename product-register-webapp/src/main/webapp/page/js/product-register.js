	$(function(){
		$("#trialNum").blur(function(){
			CheckTrialCode();
		});
		
		$("#trialNum").focus(function(){
			$("#trialDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#companyName").blur(function(){
			CheckCompanyName();
		});
		
		$("#companyName").focus(function(){
			$("#companyDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#phone").blur(function(){
			CheckPhone();
		});
		
		$("#phone").focus(function(){
			$("#phoneDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#contact").blur(function(){
			CheckContact();
		});
		
		$("#contact").focus(function(){
			$("#contactDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#mobile").blur(function(){
			CheckMobile();
		});
		
		$("#mobile").focus(function(){
			$("#mobileDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#email").blur(function(){
			CheckEmail();
		});
		
		$("#email").focus(function(){
			$("#emailDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#address").blur(function(){
			CheckAddress();
		});
		
		$("#address").focus(function(){
			$("#addressDiv").html("");
			$("#alertDiv").html("");
		});	
		
	});
	
	$(function(){
		$("#registerBtn").click(function(){
			if (CheckTrialCode() == true && CheckCompanyName() == true && CheckPhone() == true && CheckContact() == true && CheckMobile() == true && CheckEmail() == true && CheckAddress() == true )
			{
				return true;
			}
			else
			{
				$("#alertDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写注册信息</font>");
				return false;
			}
		});
		
	});
	
	
	function CheckTrialCode()
	{
		var trialCode = $("#trialNum").val();
		if (trialCode != "")
		{
			$.ajax({
				 type: "POST",	   	   
				 async: false,
				 data: {"trialCode" : trialCode},
				 url: "check-trial-code.action",
				 success: function(data){
					 $("#trialResult").val(data.result);
				 }
			});
			
			var res = $("#trialResult").val();
			if (res == "CodeIsActivate") 
			{
				 $("#trialDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>该试用号已被激活</font>");
				 return false;
			}
			else if (res == "CodeNotExist")
			{
				 $("#trialDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>该试用号不存在</font>");
				 return false;
			}
			else if (res == "" || res.length > 100)
			{
				$("#trialDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>服务器出错,抱歉!</font>");
				 return false;
			}
			else
			{
				 $("#trialDiv").html("<img src='../page/images/icon_right.gif'>");
				 return true;
			}
			
		}
		else
		{
			$("#trialDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写试用号</font>");
			return false;
		}
	}
	
	function CheckCompanyName()
	{
		var companyName = $("#companyName").val();
		if (companyName != "")
		{
			 $("#companyDiv").html("<img src='../page/images/icon_right.gif'>");
			 return true;
		}
		else
		{
			$("#companyDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写公司名称</font>");
			return false;
		}
	}

	function CheckPhone()
	{
		var phone = $("#phone").val();
		var isPhone=/^((0\d{2,3})-)?(\d{7,10})(-(\d{3,}))?$/;
		if (phone != "")
		{
			if (isPhone.test(phone))
			{
				$("#phoneDiv").html("<img src='../page/images/icon_right.gif'>");
				return true;
			}
			else
			{
				$("#phoneDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如020-xxxxxxxx</font>");
				return false;
			}
		}
		else
		{
			$("#phoneDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写电话号码</font>");
			return false;
		}
	}
	
	function CheckContact()
	{
		var contact = $("#contact").val();
		if (contact != "")
		{
			$("#contactDiv").html("<img src='../page/images/icon_right.gif'>");
			 return true;
		}
		else
		{
			$("#contactDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写联系人</font>");
			return false;
		}
	}
	
	function CheckMobile()
	{
		var mobile = $("#mobile").val();
		var isMobile=/^(?:1\d\d|15\d)\d{5}(\d{3}|\*{3})$/;   
		if (mobile != "")
		{
			if (isMobile.test(mobile))
			{
				$("#mobileDiv").html("<img src='../page/images/icon_right.gif'>");
				return true;
			}
			else
			{
				$("#mobileDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如137xxxxxxxx</font>");
				return false;
			}
		}
		else
		{
			$("#mobileDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写手机号码</font>");
			return false;
		}
	}
	
	function CheckEmail()
	{
		var email = $("#email").val();
		if (email != "")
		{
			if (email.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/))
			{
				if (CheckEmailEffective(email) == true)
				{
					$("#emailDiv").html("<img src='../page/images/icon_right.gif'>");
					return true;
				}
				else
				{
					$("#emailDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>邮箱不正确</font>");
					return false;
				}
			}
			else
			{
				$("#emailDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>格式不正确,如xxxxx@xxx.com</font>");
				return false;
			}
		}
		else
		{
			$("#emailDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写邮箱</font>");
			return false;
		}
	}
	
	function CheckEmailEffective(email)
	{
		$("#canvasloader-container").show();
		$.ajax({
			 type: "POST",	   	   
			 async: false,
			 data: {"email" : email},
			 url: "check-email-effective.action",
			 success: function(data){
				 $("#emailEffective").val(data.result);  
			 }
		});
		$("#canvasloader-container").hide();
		var res = $("#emailEffective").val();
		if (res == "ok")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function CheckAddress()
	{
		var address = $("#address").val();
		if (address != "")
		{
			$("#addressDiv").html("<img src='../page/images/icon_right.gif'>");
			return true;
		}
		else
		{
			$("#addressDiv").html("<img src='../page/images/icon_fault.gif'><font color=red>请填写地址</font>");
			return false;
		}
	}
	
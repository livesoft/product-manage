<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>反馈信息</title>
<link rel="stylesheet" type="text/css" href="../page/style/style-ie.css"/>
<link rel="stylesheet" type="text/css" href="../page/style/feedback.css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/feedback.js" type="text/javascript"></script>
<script type="text/javascript">
	function onLoad()
	{
		var result = $("#res").val();
		if (result != "") {
			alert("信息反馈成功！");
		}
	}
</script>

<script>
//初始化
var def="1";
function mover(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li_a";
  //初始主菜单先隐藏效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="block";
  //初始子菜单先隐藏效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="none";
  }
}

function mout(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li";
  //初始主菜单还原效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li_a";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="none";
  //初始子菜单还原效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="block";
  }
}
</script>

</head>

<body onload="onLoad()">

<div id="da">
<div id="header">
<div class="logo"><img src="../page/images/dayatang.png" width="504" height="87" /></div>

<div class="logo_right_top"><a href="http://www.dayatang.com">返回官网</a> 丨 <a href="http://www.dayatang.com/contact.html">联系我们</a> </div>
</div>
 
<div id="menux" style=" width:930px; margin:0 auto;">
<div id="menu">
  <ul>
    
    <li id="m_1" class='m_li'><a href="http://products.dayatang.com/index.html">首页</a></li>
    <li id="m_2" class='m_li' onmouseover='mover(2);' onmouseout='mout(2);'><a href="#">新闻中心</a></li>
    <li id="m_3" class='m_li' onmouseover='mover(3);' onmouseout='mout(3);'><a href="#">产品介绍</a></li>
    <li id="m_4" class='m_li' onmouseover='mover(4);' onmouseout='mout(4);'><a href="#">产品注册</a></li>
    <li id="m_5" class='m_li' onmouseover='mover(5);' onmouseout='mout(5);'><a href="#">成功案例</a></li>
    <li id="m_6" class='m_li' onmouseover='mover(6);' onmouseout='mout(6);'><a href="#">软件下载</a></li>
	 <li id="m_7" class='m_li' onmouseover='mover(7);' onmouseout='mout(7);'><a href="#">产品帮助</a></li>
   
  </ul>
</div>
<div style="height:32px; background-color:#F1F1F1; width:930px;" >
   <ul class="smenu"><li id="s_1" class='s_li_a'><MARQUEE scrollAmount=3 behavior=alternate>
     我们可根据贵公司的需求，提供适合企业发展的软件定制服务。
   </MARQUEE></li>
<li style="padding-left:121px;" id="s_2" class='s_li' onmouseover='mover(2);' onmouseout='mout(2);'>
    │	<a href="http://products.dayatang.com/new.html" >公司新闻</a>│
    <a href="http://products.dayatang.com/cpms-products.html">最新产品</a>│
	<a href="marketing%20strategy-products.html">营销思路</a>│</li>
<li style="padding-left:232px;" id="s_3" class='s_li' onmouseover='mover(3);' onmouseout='mout(3);'>
	 │<a href="http://products.dayatang.com/cpms-products.html">通信工程项目管理系统 </a>│</li>
  <li style="padding-left:362px;" id="s_4" class='s_li' onmouseover='mover(4);' onmouseout='mout(4);'>
	  │<a href="product-register.action">DayaCPMS注册</a>│ </li>
 <li style="padding-left:454px;" id="s_5" class='s_li' onmouseover='mover(5);' onmouseout='mout(5);'>
	 │<a href="http://products.dayatang.com/consultant-experience.html" >顾问经验</a>│
     <a href="http://products.dayatang.com/development-experience.html" >开发经验</a>│</li>
<li style="padding-left:600px;" id="s_6" class='s_li' onmouseover='mover(6);' onmouseout='mout(6);'>
	 │<a href="http://products.dayatang.com/cpms-products.html" >电脑版客户端</a>│
	<a href="http://products.dayatang.com/android.html" >Android版</a>│
	<a href="http://products.dayatang.com/android.html" >IPhone版</a>│</li>
<li style="padding-left:670px;" id="s_7" class='s_li' onmouseover='mover(7);' onmouseout='mout(7);'>
	 │<a href="feedback.action" >信息反馈</a>│
	<a href="serve-help.html" >产品咨询</a>│</li>
   </ul>
</div></div>

<br style="font-size:0; height:0; clear:both;" />
<!-- Home Content Body Starts Here -->
<div id="home_content_body"   style=" background:url(../page/images/background.jpg) no-repeat bottom #FFFFFF; margin:0 auto;">
	


	<s:hidden id="res" value="%{#parameters.result}"/>
	<div align="center">
		<form action="feedback-submit.action" id="feedbackForm">

			<p class="themain">我要提建议</p><p>
				带<span class="txt3">*</span>为必须填写
			</p>

			<table width="850" height="450" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" class="txt1">
				<tbody>
					<tr bgcolor="FBFBFB">
						<td class="font9" width="133"><div align="right">&nbsp;产品:</div></td>
						<td class="font9" bgcolor="FBFBFB" width="708" align="left"><input size="15" class="text1" value="通信工程管理系统" readonly="readonly" /></td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td width="133" class="font9"><div align="right"><span class="txt3">*</span>使用反馈:</div></td>
						<td width="708" class="font9"><table width="709" border="0">
								<tr>
									<td width="64"><div align="right">适用性:</div></td>
									<td width="493" align="left">&nbsp;是否契合您的业务显示需求</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td align="left">
										<input type="radio" name="applicability" value="GOOD" onchange="cleanInfo()" /> 较适用
										<input type="radio" name="applicability" value="SOSO" onchange="cleanInfo()" /> 一般 
										<input type="radio" name="applicability" value="BAD" onchange="cleanInfo()" /> 差</td>
								</tr>
								<tr>
									<td><div align="right">易用性:</div></td>
									<td align="left">&nbsp;使用是否方便,页面显示是否美观</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td align="left">
										<input type="radio" name="usability" value="GOOD" onchange="cleanInfo()" /> 较易用 
										<input type="radio" name="usability" value="SOSO" onchange="cleanInfo()" /> 一般 
										<input type="radio" name="usability" value="BAD" onchange="cleanInfo()" /> 差</td>
								</tr>
								<tr>
									<td><div align="right">产品性能:</div></td>
									<td align="left">&nbsp;是否出现卡死,页面没反应等不良情况</td>
								</tr>
								<tr>
									<td><div align="right"></div></td>
									<td align="left">
										<input type="radio" name="capability" value="GOOD" onchange="cleanInfo()" /> 好 
										<input type="radio" name="capability" value="SOSO" onchange="cleanInfo()" /> 一般
										<input type="radio" name="capability" value="BAD" onchange="cleanInfo()" /> 差
									</td>
								</tr>
								<tr>
									<td><div align="right">整体评价:</div></td>
									<td align="left"><input size="100" name="assessment" class="text1" onfocus="cleanInfo()" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr bgcolor="FBFBFB">
						<td class="font9" width="133"><div align="right">改进意见:</div></td>
						<td class="font9" width="708">
							<table width="708" border="0">
								<tr>
									<td align="left">产品有无不足或错误？如有,请指正:</td>
								</tr>
								<tr>
									<td align="left"><input size="120" name="shortage" class="text1" onfocus="cleanInfo()" /></td>
								</tr>
								<tr>
									<td align="left">您觉得本产品需要增加的内容或改进的地方？</td>
								</tr>
								<tr>
									<td align="left"><input size="120" name="improve" class="text1" onfocus="cleanInfo()" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td width="133" class="font9"><div align="right"><span class="txt3">*</span>公司名称:</div></td>
						<td width="708" class="font9" align="left"><input size="30" id="companyName" name="companyName" class="text1" onfocus="cleanInfo()" /></td>
					</tr>
					<tr bgcolor="FBFBFB">
						<td class="font9" width="133"><div align="right"><span class="txt3">*</span>Email:</div></td>
						<td class="font9" width="708" align="left"><input size="30" id="email" name="email" class="text1" onfocus="cleanInfo()" /></td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td width="133" class="font9"><div align="right">联系电话:</div></td>
						<td width="708" class="font9" align="left"><input size="30" name="phone" class="text1" onfocus="cleanInfo()" /></td>
					</tr>
					<tr bgcolor="FBFBFB">
						<td class="font9" width="133"><div align="right">地址:</div></td>
						<td class="font9" width="708" align="left"><input size="80" name="address" class="text1" onfocus="cleanInfo()" /></td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td width="133" class="font9"><div align="right"><span class="txt3">*</span>验证码:</div></td>
						<td width="708" align="left"><div><input size="5" id="code" name="code" onfocus="cleanInfo()" /><img src="../common/random-picture.action?sessionKey=rand" onclick="changeValidateCode(this)" /> </div> </td>
					</tr>
				</tbody>
			</table>
			<div id="alertInfo" class="txt3"></div>
			<center>
				<br/>
					 <input type="submit" value="提交" /> <br/>
			</center>
		</form>
		<br><br><br><br>
	</div>
</div>

<div class="footer">Copyright<span class="c">&copy;</span>2012 广州大雅堂信息科技有限公司.粤11011680号-2. All rights reserved.</div>

</div>
	

</body>
</html>
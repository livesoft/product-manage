<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>下载试用</title>
<link rel="stylesheet" type="text/css" href="../page/style/style-ie.css"/>
<link rel="stylesheet" type="text/css" href="../page/style/style.css"/>
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script src="../page/js/download-trial.js" type="text/javascript"></script>


<style type="text/css">
<!--
.STYLE1 {color: #FF0000}
-->
</style>

<script>
//初始化
var def="1";
function mover(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li_a";
  //初始主菜单先隐藏效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="block";
  //初始子菜单先隐藏效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="none";
  }
}

function mout(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li";
  //初始主菜单还原效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li_a";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="none";
  //初始子菜单还原效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="block";
  }
}
</script>

</head>

<body>

<div id="da">
<div id="header">
<div class="logo"><img src="../page/images/dayatang.png" width="504" height="87" /></div>

<div class="logo_right_top"><a href="http://www.dayatang.com">返回官网</a> 丨 <a href="http://www.dayatang.com/contact.html">联系我们</a> </div>
</div>
 
<div id="menux" style=" width:930px; margin:0 auto;">
<div id="menu">
  <ul>
    
    <li id="m_1" class='m_li'><a href="http://products.dayatang.com/index.html">首页</a></li>
    <li id="m_2" class='m_li' onmouseover='mover(2);' onmouseout='mout(2);'><a href="#">新闻中心</a></li>
    <li id="m_3" class='m_li' onmouseover='mover(3);' onmouseout='mout(3);'><a href="#">产品介绍</a></li>
    <li id="m_4" class='m_li' onmouseover='mover(4);' onmouseout='mout(4);'><a href="#">产品注册</a></li>
    <li id="m_5" class='m_li' onmouseover='mover(5);' onmouseout='mout(5);'><a href="#">成功案例</a></li>
    <li id="m_6" class='m_li' onmouseover='mover(6);' onmouseout='mout(6);'><a href="#">软件下载</a></li>
	 <li id="m_7" class='m_li' onmouseover='mover(7);' onmouseout='mout(7);'><a href="#">产品帮助</a></li>
   
  </ul>
</div>
<div style="height:32px; background-color:#F1F1F1; width:930px;" >
   <ul class="smenu"><li id="s_1" class='s_li_a'><MARQUEE scrollAmount=3 behavior=alternate>
     我们可根据贵公司的需求，提供适合企业发展的软件定制服务。
   </MARQUEE></li>
<li style="padding-left:121px;" id="s_2" class='s_li' onmouseover='mover(2);' onmouseout='mout(2);'>
    │	<a href="http://products.dayatang.com/new.html" >公司新闻</a>│
    <a href="http://products.dayatang.com/cpms-products.html">最新产品</a>│
	<a href="marketing%20strategy-products.html">营销思路</a>│</li>
<li style="padding-left:232px;" id="s_3" class='s_li' onmouseover='mover(3);' onmouseout='mout(3);'>
	 │<a href="http://products.dayatang.com/cpms-products.html">通信工程项目管理系统 </a>│</li>
  <li style="padding-left:362px;" id="s_4" class='s_li' onmouseover='mover(4);' onmouseout='mout(4);'>
	  │<a href="product-register.action">DayaCPMS注册</a>│ </li>
 <li style="padding-left:454px;" id="s_5" class='s_li' onmouseover='mover(5);' onmouseout='mout(5);'>
	 │<a href="http://products.dayatang.com/consultant-experience.html" >顾问经验</a>│
     <a href="http://products.dayatang.com/development-experience.html" >开发经验</a>│</li>
<li style="padding-left:600px;" id="s_6" class='s_li' onmouseover='mover(6);' onmouseout='mout(6);'>
	 │<a href="http://products.dayatang.com/cpms-products.html" >电脑版客户端</a>│
	<a href="http://products.dayatang.com/android.html" >Android版</a>│
	<a href="http://products.dayatang.com/android.html" >IPhone版</a>│</li>
<li style="padding-left:670px;" id="s_7" class='s_li' onmouseover='mover(7);' onmouseout='mout(7);'>
	 │<a href="feedback.action" >信息反馈</a>│
	<a href="serve-help.html" >产品咨询</a>│</li>
   </ul>
</div></div>

<br style="font-size:0; height:0; clear:both;" />
<!-- Home Content Body Starts Here -->
<div id="home_content_body"   style=" background:url(../page/images/background.jpg) no-repeat bottom #FFFFFF; margin:0 auto;">

<!-- Left Content Starts Here -->
<table width="680" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(102, 102, 102); font-family: 宋体; font-size: 12px; line-height: 22px; "><tbody style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">
<tr style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; "><td bgcolor="F0F0F0" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; background-color: rgb(255, 255, 255); " class="">
<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: gray; "><strong style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(10, 126, 206); "><font size="5">申请试用信息登记</font></strong><br style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">
<font size="3">
提交试用申请的用户，均可获得售前技术支持服务。&nbsp;<span class=" " style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">提交试用信息后即可下载！</span><br style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">
<strong style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(234, 71, 4); ">注意：</strong>加<span style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(234, 71, 4); ">*</span>号的为必填项，请填写真实信息，以便我们依据资料提供服务！</font></p>
</td></tr>
<tr style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; "><td style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " class=""><form action="http://www.jee-soft.cn/front/customers/save.htm" method="post" name="form" id="form" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">
<table cellspacing="0" cellpadding="3" width="100%" border="0" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; "><tbody style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; ">
<tr style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; "><td colspan="2" bgcolor="F0F0F0" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; background-color: rgb(255, 255, 255); " class=""><font size="3">在试用过程中，存在任何疑问，</font><strong style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; "><font size="3">欢迎咨询：020-87266226</font></strong></td></tr></tbody></table>
</form></td></tr></tbody></table>
<div>
<form action="download-trial-submit.action" method="post">
	<table width="700" border="0" height="360" style=" margin-top:40px;" align="center">
	  <tr>
	    <td class="one" align="right">试用产品:</td>
	    <td class="two"><s:select list="productMaps" listKey="key" listValue="value" theme="simple" name="productId" /></td>
	  	<td class="three"><div id="trialDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>公司名称:</td>
	    <td class="two"><input name="companyName" class="text" type="text" id="companyName"/></td>
	  	<td class="three"><div id="companyDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>公司电话:</td>
	    <td class="two"><input name="phone" class="text" type="text" id="phone"/></td>
	    <td class="three"><div id="phoneDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>联系人:</td>
	    <td class="two"><input name="contact" class="text" type="text" id="contact"/></td>
	  	<td class="three"><div id="contactDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>手机:</td>
	    <td class="two"><input name="mobile" class="text" type="text" id="mobile"/></td>
	    <td class="three"><div id="mobileDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>邮箱:</td>
	    <td class="two"><input name="email" class="text" type="text" id="email"/></td>
	  	<td class="three"><div id="emailDiv"><strong><span style="color: red;">*请填写正确的邮箱来接收注册码</span></strong></div><div id="canvasloader-container" class="wrapper" style="display: none;"></div></td>
	  </tr>
	  <tr>
	    <td class="one">地址:</td>
	    <td class="two"><input name="address" class="text" type="text" id="address"/></td>
	  	<td class="three"><div id="addressDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one">获取渠道:</td>
	    <td class="two">
	    	<select name="trench">
				<option value=""></option>
				<option value="Google搜索">Google搜索</option>
				<option value="百度搜索">百度搜索</option>
				<option value="论坛">论坛</option>
				<option value="网页广告">网页广告</option>
				<option value="朋友介绍">朋友介绍</option>
				<option value="公司业务员电话兜售">公司业务员电话兜售</option>
				<option value="公司业务员邮件兜售">公司业务员邮件兜售</option>
				<option value="公司业务员QQ兜售">公司业务员QQ兜售</option>
				<option value="其它">其它</option>
			</select>
	    </td>
	  	<td class="three"><div id="addressDiv"></div></td>
	  </tr>
	  <tr>
	    <td class="one"><span style="color: red;">*</span>验证码:</td>
	    <td class="two"><input size="5" id="code" name="code" onfocus="cleanInfo()" /><img src="../common/random-picture.action?sessionKey=downloadTrial" onclick="changeValidateCode(this)" /></td>
	  	<td class="three"><div id="codeDiv"></div></td>
	  </tr>
	</table>
	<br><br>
	<div class="botton" style="text-align:center; ">
  		<input name="button" type="submit" class="button" value="申请试用" id="registerBtn"/><div id="alertDiv"></div>
	</div>
</form>

  <br><br><br><br><br><br><br>
  <!-- Content Ends Here -->

</div>
</div>

<div class="footer">Copyright<span class="c">&copy;</span>2012 广州大雅堂信息科技有限公司.粤11011680号-2. All rights reserved.</div>

</div>

 </body>
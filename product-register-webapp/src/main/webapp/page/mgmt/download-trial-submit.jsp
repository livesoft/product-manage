<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>申请试用</title>
<link rel="stylesheet" type="text/css" href="../page/style/style.css" />
<link rel="stylesheet" type="text/css" href="../page/style/style-ie.css"/>
<script>
//初始化
var def="1";
function mover(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li_a";
  //初始主菜单先隐藏效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="block";
  //初始子菜单先隐藏效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="none";
  }
}

function mout(object){
  //主菜单
  var mm=document.getElementById("m_"+object);
  mm.className="m_li";
  //初始主菜单还原效果
  if(def!=0){
    var mdef=document.getElementById("m_"+def);
    mdef.className="m_li_a";
  }
  //子菜单
  var ss=document.getElementById("s_"+object);
  ss.style.display="none";
  //初始子菜单还原效果
  if(def!=0){
    var sdef=document.getElementById("s_"+def);
    sdef.style.display="block";
  }
}
</script>

</head>

<body>

<div id="da">
<div id="header">
<div class="logo"><img src="../page/images/dayatang.png" width="504" height="87" /></div>

<div class="logo_right_top"><a href="http://www.dayatang.com">返回官网</a> 丨 <a href="http://www.dayatang.com/contact.html">联系我们</a> </div>
</div>
 
<div id="menux" style=" width:930px; margin:0 auto;">
<div id="menu">
  <ul>
    
    <li id="m_1" class='m_li'><a href="http://products.dayatang.com/index.html">首页</a></li>
    <li id="m_2" class='m_li' onmouseover='mover(2);' onmouseout='mout(2);'><a href="#">新闻中心</a></li>
    <li id="m_3" class='m_li' onmouseover='mover(3);' onmouseout='mout(3);'><a href="#">产品介绍</a></li>
    <li id="m_4" class='m_li' onmouseover='mover(4);' onmouseout='mout(4);'><a href="#">产品注册</a></li>
    <li id="m_5" class='m_li' onmouseover='mover(5);' onmouseout='mout(5);'><a href="#">成功案例</a></li>
    <li id="m_6" class='m_li' onmouseover='mover(6);' onmouseout='mout(6);'><a href="#">软件下载</a></li>
	 <li id="m_7" class='m_li' onmouseover='mover(7);' onmouseout='mout(7);'><a href="#">产品帮助</a></li>
   
  </ul>
</div>
<div style="height:32px; background-color:#F1F1F1; width:930px;" >
   <ul class="smenu"><li id="s_1" class='s_li_a'><MARQUEE scrollAmount=3 behavior=alternate>
     我们可根据贵公司的需求，提供适合企业发展的软件定制服务。
   </MARQUEE></li>
<li style="padding-left:121px;" id="s_2" class='s_li' onmouseover='mover(2);' onmouseout='mout(2);'>
    │	<a href="http://products.dayatang.com/new.html" >公司新闻</a>│
    <a href="http://products.dayatang.com/cpms-products.html">最新产品</a>│
	<a href="marketing%20strategy-products.html">营销思路</a>│</li>
<li style="padding-left:232px;" id="s_3" class='s_li' onmouseover='mover(3);' onmouseout='mout(3);'>
	 │<a href="http://products.dayatang.com/cpms-products.html">通信工程项目管理系统 </a>│</li>
  <li style="padding-left:362px;" id="s_4" class='s_li' onmouseover='mover(4);' onmouseout='mout(4);'>
	  │<a href="product-register.action">DayaCPMS注册</a>│ </li>
 <li style="padding-left:454px;" id="s_5" class='s_li' onmouseover='mover(5);' onmouseout='mout(5);'>
	 │<a href="http://products.dayatang.com/consultant-experience.html" >顾问经验</a>│
     <a href="http://products.dayatang.com/development-experience.html" >开发经验</a>│</li>
<li style="padding-left:600px;" id="s_6" class='s_li' onmouseover='mover(6);' onmouseout='mout(6);'>
	 │<a href="http://products.dayatang.com/cpms-products.html" >电脑版客户端</a>│
	<a href="http://products.dayatang.com/android.html" >Android版</a>│
	<a href="http://products.dayatang.com/android.html" >IPhone版</a>│</li>
<li style="padding-left:670px;" id="s_7" class='s_li' onmouseover='mover(7);' onmouseout='mout(7);'>
	 │<a href="feedback.action" >信息反馈</a>│
	<a href="serve-help.html" >产品咨询</a>│</li>
   </ul>
</div></div>

<br style="font-size:0; height:0; clear:both;" />
<!-- Home Content Body Starts Here -->
<div id="home_content_body"   style=" background:url(../page/images/background.jpg) no-repeat bottom #FFFFFF; margin:0 auto;">

<h1>申请试用产品</h1>
<!-- Title -->

 </div>    
<!--Inner Page Title Ends Here -->

<!-- Clear -->
<div class="clear">

</div>
<!-- Clear -->

<br style="font-size:0; height:0; clear:both;" />
<!-- Home Content Body Starts Here -->
<div id="home_content_body"   style=" background:url(../page/images/background.jpg) no-repeat bottom #FFFFFF; margin:0 auto;">

<!-- Left Content Starts Here -->

<div style="height: 500px;margin-top: 50px;margin-left: 50px;">
	<span style="font-size: x-large">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:property value="companyName"/>,您好！<br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;恭喜您！您的下载申请已经成功提交,我们将尽快审阅您的申请并给您相应的答案.<br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如有任何疑问,您也可以直接与我们联系,欢迎咨询：020-87266226！<br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://products.dayatang.com/downloads/daya-cpms-data.zip">点击这里下载通信工程管理系统</a>
	</span>
  <!-- Content Ends Here -->

</div>
<!-- Left Content Ends Here -->

<!-- Right Content Starts Here -->
<!-- Right Content Ends here -->
<!-- Clear -->
<div class="clear">
</div>
<!-- Clear -->

</div>
<!-- Home Content Body Ends Here -->


<!-- Content Part Ends Here -->

<!-- Footer Starts Here -->

<div class="footer">Copyright<span class="c">&copy;</span>2012 广州大雅堂信息科技有限公司.粤11011680号-2. All rights reserved.</div>


</div>

 </body>
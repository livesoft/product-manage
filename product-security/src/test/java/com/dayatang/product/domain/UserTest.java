package com.dayatang.product.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.domain.QuerySettings;

public class UserTest {

	@Mock
	private EntityRepository repository;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);   // 声明测试用例类   
		MyAbstractEntity.setRepository(repository);
	}

	@After
	public void tearDown() throws Exception {
		MyAbstractEntity.setRepository(null);
	}
	
	@Test
	public void getByUserId() {
		final User user = new User("luomin");
		stub(repository.get(User.class, 1L)).toReturn(user);
		assertEquals(user, User.getByUserId(1L));
	}

	@Test
	public void getByUsername() {
		String username = "luomin";
		QuerySettings<User> querySettings = QuerySettings.create(User.class).eq("username", username);
		final User user = new User(username);
		System.out.println(user.getUsername());
		when(repository.getSingleResult(querySettings)).thenReturn(user);
		assertEquals(user, User.getByUsername(username));
	}
	
	

}

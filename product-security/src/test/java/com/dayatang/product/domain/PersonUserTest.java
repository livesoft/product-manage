package com.dayatang.product.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.domain.QuerySettings;

public class PersonUserTest {
	
	@Mock
	private EntityRepository repository;
	private Person person = new Person("罗敏");
	private String username = "luomin";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);   // 声明测试用例类   
		MyAbstractEntity.setRepository(repository);
	}

	@After
	public void tearDown() throws Exception {
		MyAbstractEntity.setRepository(null);
	}

	@Test
	public void getByPerson() {
		final PersonUser personUser = new PersonUser(username, person);
		QuerySettings<PersonUser> querySettings = QuerySettings.create(PersonUser.class).eq("person", person);
		when(repository.getSingleResult(querySettings)).thenReturn(personUser);
		assertEquals(personUser, PersonUser.getByPerson(person));
	}
	
	@Test
	public void getByUsername() {
		final PersonUser personUser = new PersonUser(username, person);
		QuerySettings<PersonUser> querySettings = QuerySettings.create(PersonUser.class).eq("username", username);
		when(repository.getSingleResult(querySettings)).thenReturn(personUser);
		assertEquals(personUser, PersonUser.getByUsername(username));
	}

}

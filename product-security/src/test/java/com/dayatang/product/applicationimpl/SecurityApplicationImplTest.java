package com.dayatang.product.applicationimpl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.impl.SecurityApplicationImpl;
import com.dayatang.product.domain.MyAbstractEntity;
import com.dayatang.product.domain.PersonUser;

/**
 * 文件名:SecurityApplicationImplTest.java
 * 描述:用户权限接口实现测试类
 * 创建时间:2012-5-18 下午5:01:47
 * 创建者:mluo
 * 版本号:v2.0
 */
public class SecurityApplicationImplTest {

	private SecurityApplicationImpl applicationImpl = new SecurityApplicationImpl();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private PersonUser personUser;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MyAbstractEntity.setRepository(repository);
	}
	
	@After
	public void tearDown() throws Exception {
		MyAbstractEntity.setRepository(null);
	}

	@Test
	public void createUser() {
//		applicationImpl.createPersonUser(personUser);
//		verify(personUser).save();
	}
	
	@Test
	public void resetPasswrod() {
		applicationImpl.resetPassword(personUser, "1234567");
		verify(personUser).save();
	}

}



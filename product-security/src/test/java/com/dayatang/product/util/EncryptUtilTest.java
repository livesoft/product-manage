package com.dayatang.product.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 文件名:EncryptUtilTest.java
 * 描述:加密工具测试类
 * 创建时间:2012-5-18 下午4:42:34
 * 创建者:mluo
 * 版本号:v2.0
 */
public class EncryptUtilTest {
	
	private String str = "luomin";
	
	@Test
	public void encryptString() {
		String encryptStr = "bc01bd2d1d4a78831b8d4b8a6b938c84";
		assertEquals(null,EncryptUtil.encryptString(null));
		assertEquals(encryptStr,EncryptUtil.encryptString(str));
	}
}



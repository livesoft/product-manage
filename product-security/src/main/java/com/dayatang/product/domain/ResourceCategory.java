package com.dayatang.product.domain;
/**
 * 文件名:ResourceCategory.java
 * 描述:资源类型
 * 创建时间:2012-6-12 上午10:22:21
 * 创建者:mluo
 * 版本号:v2.0
 */
public enum ResourceCategory {
	COMMON,     // 公共
	PRIVATE     // 私有
}
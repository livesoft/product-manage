package com.dayatang.product.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

/**
 * 文件名:Permission.java
 * 描述:权限表
 * 创建时间:2012-5-24 下午5:20:17
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "permissions")
@Cacheable
public class Permission extends MyAbstractEntity implements Comparable<Permission> {

	private static final long serialVersionUID = 37608901158764624L;

	private String name;
	
	private String description;
	
	@Column(name = "sort_order")
	private int sortOrder;
	
	@Column(name = "link_url")
	private String linkUrl;

	@ManyToOne
	@JoinTable(name = "permission_relations", joinColumns = @JoinColumn(name = "child_id"), inverseJoinColumns = @JoinColumn(name = "parent_id"))
	private Permission parent;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Permission> children = new HashSet<Permission>();

	@OneToMany(mappedBy = "permission", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Resource> resources = new HashSet<Resource>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Permission getParent() {
		return parent;
	}

	public void setParent(Permission parent) {
		this.parent = parent;
	}

	public List<Permission> getChildren() {
		List<Permission> results = new ArrayList<Permission>(children);
		Collections.sort(results);
		return results;
	}
	
	public Set<Resource> getResources() {
		return resources;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(name).append(sortOrder).toHashCode();
	}

	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (!(other instanceof Permission))
			return false;
		Permission that = (Permission) other;
		return new EqualsBuilder().append(name, that.name).append(sortOrder, that.sortOrder).isEquals();
	}

	public String toString() {
		return "Permission [name=" + name + "]";
	}

	public int compareTo(Permission other) {
		return new CompareToBuilder().append(sortOrder, other.sortOrder).toComparison();
	}
	
	public static List<Permission> getMenuPermissions() {
		return getRepository().find(QuerySettings.create(Permission.class).isNull("parent"));
	}

}



package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:Resource.java
 * 描述:资源表
 * 创建时间:2012-6-8 下午3:43:54
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "resources")
@Cacheable
@NamedQueries({
	@NamedQuery(name = "findByCategory", query = "select r from Resource r where r.category = ?")
})
public class Resource extends MyAbstractEntity {

	private static final long serialVersionUID = -4492193684994082762L;

	@Column(name = "resource_type")
	private String resourceType;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private ResourceCategory category;
	
	@ManyToOne
	@JoinColumn(name = "permission_id")
	private Permission permission;
	
	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	public ResourceCategory getCategory() {
		return category;
	}

	public void setCategory(ResourceCategory category) {
		this.category = category;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(resourceType).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Resource))
			return false;
		Resource castOther = (Resource) other;
		return new EqualsBuilder().append(resourceType, castOther.resourceType).isEquals();
	}

	public String toString() {
		return resourceType;
	}
	
	/**
	 * 查找公共资源
	 * @return
	 */
	public static List<Resource> findCommonResource() {
		return getRepository().findByNamedQuery("findByCategory", new Object[]{ResourceCategory.COMMON}, Resource.class);
	}
	
}



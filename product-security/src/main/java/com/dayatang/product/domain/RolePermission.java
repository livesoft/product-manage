package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:RolePermission.java
 * 描述:角色权限表
 * 创建时间:2012-6-8 下午2:43:44
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "role_permissions")
@Cacheable
@NamedQueries({
	@NamedQuery(name = "findByRole", query = "select r from RolePermission r where r.role = ?")
})
public class RolePermission extends MyAbstractEntity {

	private static final long serialVersionUID = -5640427609147215457L;

	@ManyToOne
	@JoinColumn(name = "permission_id")
	private Permission permission;

	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(permission).append(role).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof RolePermission))
			return false;
		RolePermission castOther = (RolePermission) other;
		return new EqualsBuilder().append(this.permission, castOther.getPermission()).append(this.role, castOther.getRole()).isEquals();
	}

	public String toString() {
		return permission.toString() + ":" + role.toString();
	}

	/**
	 * 查找角色的权限
	 * @param role
	 * @return
	 */
	public static List<RolePermission> findByRole(Role role) {
		return getRepository().findByNamedQuery("findByRole", new Object[]{role}, RolePermission.class);
	}
	
}



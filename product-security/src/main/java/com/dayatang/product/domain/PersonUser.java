package com.dayatang.product.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.dayatang.domain.QuerySettings;

@Entity
@DiscriminatorValue("PERSON")
public class PersonUser extends User {

	private static final long serialVersionUID = -4976250400152978335L;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person person;

	public PersonUser() {
	}
	
	public PersonUser(String username, Person person) {
		super(username);
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}
	
	public void setPerson(Person person) {
		this.person = person;
	}

	/**
	 * 查找人员对应的用户
	 * @param person
	 * @return
	 */
	public static PersonUser getByPerson(Person person) {
		return getRepository().getSingleResult(QuerySettings.create(PersonUser.class).eq("person", person));
	}

	/**
	 * 根据用户名查找对应的用户
	 * @param username
	 * @return
	 */
	public static PersonUser getByUsername(String username) {
		return getRepository().getSingleResult(QuerySettings.create(PersonUser.class).eq("username", username));
	}
	
}

package com.dayatang.product.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CATEGORY", discriminatorType = DiscriminatorType.STRING)
@Table(name = "users")
@Cacheable
public class User extends MyAbstractEntity {

	private static final long serialVersionUID = -5855623489453705568L;

	@NotNull
	@Column(nullable = false, unique = true)
	private String username;
	
	private String password;
	
	private String email;
	
	private String mobile;
	
	private String qq;
	
	@Column(name = "account_disabled")
	private boolean disabled = false;
	
	public User() {
	}
	
	public User(String username) {
		this.username = username;
	}

	public User(String username, String password, String email, String mobile,
			String qq, boolean disabled) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.mobile = mobile;
		this.qq = qq;
		this.disabled = disabled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(username).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof User))
			return false;
		User castOther = (User) other;
		return new EqualsBuilder().append(username, castOther.username).isEquals();
	}

	public String toString() {
		return username;
	}
	
	public static User getByUserId(long userId) {
		return getRepository().get(User.class, userId);
	}

	public static User getByUsername(String username) {
		return getRepository().getSingleResult(QuerySettings.create(User.class).eq("username", username));
	}
	
}

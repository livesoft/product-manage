package com.dayatang.product.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

/**
 * 文件名:Role.java
 * 描述:角色表
 * 创建时间:2012-5-24 下午5:09:50
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "roles")
@Cacheable
@NamedQueries({
	@NamedQuery(name = "findByName", query = "SELECT r FROM Role r where r.name = :name")
})
public class Role extends MyAbstractEntity {

	private static final long serialVersionUID = 3660106750646228893L;

	@NotNull
	@Column(nullable = false, unique = true)
	private String name;

	@Column(name = "role_describe")
	private String roleDescribe;
	
	// 状态
	private boolean state;
	
	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoleDescribe() {
		return roleDescribe;
	}

	public void setRoleDescribe(String roleDescribe) {
		this.roleDescribe = roleDescribe;
	}

	public boolean getIsState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(name).append(roleDescribe).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Role))
			return false;
		Role castOther = (Role) other;
		return new EqualsBuilder().append(name, castOther.name).append(roleDescribe, castOther.roleDescribe).isEquals();

	}

	public String toString() {
		return name + ":" + roleDescribe;
	}

	public static Role findByName(String roleName) {
		return getRepository().getSingleResult(QuerySettings.create(Role.class).eq("name", roleName));
	}

}



package com.dayatang.product.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 文件名:EncryptUtil.java
 * 描述: 加密工具
 * 创建时间:2012-5-18 下午4:45:56
 * 创建者:mluo
 * 版本号:v2.0
 */
public class EncryptUtil {

	public static String encryptString(String str) {
		if (str == null) {
			return null;
		}
		return md5Encrypt(str.getBytes());
	}
	
	private static String md5Encrypt(byte[] src) {
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(src);
			for (byte b : md.digest()) {
				sb.append(Integer.toString(b >>> 4 & 0xF, 16)).append(Integer.toString(b & 0xF, 16));
			}
		} catch (NoSuchAlgorithmException e) {
			e.toString();
		}
		return sb.toString();
	}

}



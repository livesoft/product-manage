package com.dayatang.product.application;

import java.util.List;

import com.dayatang.product.domain.Permission;
import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.domain.Role;

/**
 * 文件名:SecurityApplication.java
 * 描述:用户权限模块的接口
 * 创建时间:2012-5-18 下午3:51:30
 * 创建者:mluo
 * 版本号:V2.0
 */

public interface SecurityApplication {
	
	/**
	 * 创建用户并加密密码
	 * @param personUser 创建的用户
	 * @param orgIds 所属部门
	 * @param roleIds 拥有角色
	 */
	void createPersonUser(PersonUser personUser, long[] orgIds, long[] roleIds);
	
	/**
	 * 重置密码
	 * @param personUser 目标用户
	 * @param newPassword 新密码
	 */
	void resetPassword(PersonUser personUser, String newPassword);
	
	/**
	 * 获取用户权限范围内可操作的资源
	 * @param personUser 用户
	 * @return 资源
	 */
	List<String> getUserPermissionResource(PersonUser personUser, Role role);
	
	/**
	 * 角色授权
	 * @param role 目标角色
	 * @param permissions 权限集合
	 */
	void givePermissionToRole(Role role, List<Permission> permissions);
}

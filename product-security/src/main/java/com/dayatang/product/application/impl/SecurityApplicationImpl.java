package com.dayatang.product.application.impl;

import java.util.ArrayList;
import java.util.List;

import com.dayatang.product.application.SecurityApplication;
import com.dayatang.product.domain.Organization;
import com.dayatang.product.domain.Permission;
import com.dayatang.product.domain.Person;
import com.dayatang.product.domain.PersonOrganization;
import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.domain.Resource;
import com.dayatang.product.domain.Role;
import com.dayatang.product.domain.RoleAssignment;
import com.dayatang.product.domain.RolePermission;
import com.dayatang.product.util.EncryptUtil;

/**
 * 文件名:SecurityApplicationImpl.java
 * 描述:用户权限模块的接口实现
 * 创建时间:2012-5-18 下午3:52:02
 * 创建者:mluo
 * 版本号:V2.0
 */

public class SecurityApplicationImpl implements SecurityApplication {

	public void createPersonUser(PersonUser personUser, long[] orgIds, long[] roleIds) {
		Person person = personUser.getPerson();
		person.setUsername(personUser.getUsername());
		person.save();
		personUser.setPerson(person);
		personUser.setMobile(person.getMobile());
		personUser.setEmail(person.getEmail());
		personUser.setQq(person.getQq());
		if (personUser.getPassword().length() < 20) {
			personUser.setPassword(EncryptUtil.encryptString(personUser.getPassword()));
		}
		personUser.save();
		if (orgIds != null) {
			for (long orgId : orgIds) {
				PersonOrganization personOrganization = new PersonOrganization();
				personOrganization.setPerson(person);
				personOrganization.setOrganization(Organization.get(Organization.class, orgId));
				personOrganization.save();
			}
		}
		if (roleIds != null) {
			for (long roleId : roleIds) {
				RoleAssignment roleAssignment = new RoleAssignment();
				roleAssignment.setUser(personUser);
				roleAssignment.setRole(Role.get(Role.class, roleId));
				roleAssignment.save();
			}
		}
	}

	public void resetPassword(PersonUser personUser, String newPassword) {
		personUser.setPassword(EncryptUtil.encryptString(newPassword));
		personUser.save();
	}

	public List<String> getUserPermissionResource(PersonUser personUser, Role role) {
		List<String> results = new ArrayList<String>();
		for (RolePermission rolePermission : RolePermission.findByRole(role)) {
			for (Resource resource : rolePermission.getPermission().getResources()) {
				results.add(resource.getResourceType());
			}
		}
		for (Resource resource : Resource.findCommonResource()) {
			results.add(resource.getResourceType());
		}
		results.add("IndexAction");
		return results;
	}

	public void givePermissionToRole(Role role, List<Permission> permissions) {
		List<RolePermission> rolePermissions = RolePermission.findByRole(role);
		if (rolePermissions.size() > 0) {
			for (RolePermission rolePermission : rolePermissions) {
				rolePermission.remove();
			}
		}
		for (Permission permission : permissions) {
			RolePermission rolePermission = new RolePermission();
			rolePermission.setRole(role);
			rolePermission.setPermission(permission);
			rolePermission.save();
		}
	}
	
}

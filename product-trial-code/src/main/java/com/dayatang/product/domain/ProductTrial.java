package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.AbstractEntity;
import com.dayatang.product.util.EncryptUtil;

@Entity
@Table(name = "product_trials")
public class ProductTrial extends AbstractEntity {
	
	private static final long serialVersionUID = 6890452084678982686L;

	@Column(name = "trial_code")
	private String trialCode;
	
	@Column(name = "register_code")
	private String registerCode;
	
	@Column(name = "trial_day")
	private long trialDay;
	
	private String first;
	
	private String two;
	
	private String three;
	
	public String getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(String trialCode) {
		this.trialCode = trialCode;
	}

	public String getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	public long getTrialDay() {
		return trialDay;
	}

	public void setTrialDay(long trialDay) {
		this.trialDay = trialDay;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getTwo() {
		return two;
	}

	public void setTwo(String two) {
		this.two = two;
	}

	public String getThree() {
		return three;
	}

	public void setThree(String three) {
		this.three = three;
	}


	public static ProductTrial getEncryptProductTrial(String result) {
		String[] strings = EncryptUtil.splitString(result);
		ProductTrial productTrial = new ProductTrial();
		productTrial.setRegisterCode(strings[1]);
		productTrial.setTrialCode(strings[0]);
		productTrial.setTrialDay(Long.valueOf(strings[2]));
		List<String> lists = EncryptUtil.getEncryptString(strings[0] + strings[1] + strings[2]);
		productTrial.setTwo(lists.get(0));
		productTrial.setFirst(lists.get(2));
		productTrial.setThree(lists.get(1));
		return productTrial;
	}
	
	public static ProductTrial trialDayUpdate(ProductTrial productTrial) {
		if (checkDataValidity(productTrial)) {
			productTrial.setTrialDay(productTrial.getTrialDay() == 0 ? 0 : productTrial.getTrialDay()-1);
			List<String> lists = EncryptUtil.getEncryptString(productTrial.getTrialCode() + productTrial.getRegisterCode() + productTrial.getTrialDay());
			productTrial.setTwo(lists.get(0));
			productTrial.setFirst(lists.get(2));
			productTrial.setThree(lists.get(1));
			return productTrial;
		}
		return null;
	}
	
	public static boolean checkDataValidity(ProductTrial productTrial) {
		String md5 = "a" + EncryptUtil.getMD5((productTrial.getTrialCode() + productTrial.getRegisterCode() + productTrial.getTrialDay()).getBytes()) + "b";
		String mymd5 = productTrial.getTwo() + productTrial.getThree() + productTrial.getFirst();
		if (md5.equals(mymd5)) {
			return true;
		}
		return false;
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(first).append(two).append(three).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof ProductTrial))
			return false;
		ProductTrial castOther = (ProductTrial) other;
		return new EqualsBuilder().append(first,castOther.first).append(two,castOther.two).append(three,castOther.three).isEquals();

	}

	public String toString() {
		return first + two + three;
	}

}

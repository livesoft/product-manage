package com.dayatang.product.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EncryptUtil {

	public static String[] splitString(String result) {
		return result.split("\\.");
	}

	public static List<String> getEncryptString(String string) {
		String encrypt = getMyMD5(string);
		int[] is = getInt(encrypt.length());
		List<String> strings = new ArrayList<String>();
		strings.add(encrypt.substring(0, is[0]));
		strings.add(encrypt.substring(is[0], is[1]));
		strings.add(encrypt.substring(is[1], encrypt.length()));
		return strings;
	}
	
	public static int[] getInt(int n) {
		int[] is = new int[2];
		while(true) {
			is[0] = getRandomInt(n);
			is[1] = getRandomInt(n);
			if (is[1] - is[0] > 1) {
				break;
			}
		}
		return is;
	}
	
	private static int getRandomInt(int n) {
		Random random = new Random();
		return random.nextInt(n);
	}
	
	private static String getMyMD5(String string) {
		return "a" + getMD5(string.getBytes()) + "b";
	}
	
	public static String getMD5(byte[] src) {
		StringBuffer sb = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(src);
			for (byte b : md.digest()) {
				sb.append(Integer.toString(b >>> 4 & 0xF, 16)).append(Integer.toString(b & 0xF, 16));
			}
		} catch (NoSuchAlgorithmException e) {
			e.toString();
		}
		return sb.toString();
	}

}

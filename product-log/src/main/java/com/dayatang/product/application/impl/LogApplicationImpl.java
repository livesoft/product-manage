package com.dayatang.product.application.impl;

import java.util.Date;
import java.util.List;

import com.dayatang.product.Page;
import com.dayatang.product.application.LogApplication;
import com.dayatang.product.log.AbstractEntity;
import com.dayatang.product.log.UserLog;

/**
 * 文件名:LogApplicationImpl.java
 * 描述:日志模块接口实现
 * 创建时间:2012-6-19 下午9:46:58
 * 创建者:mluo
 * 版本号:v2.0
 */
public class LogApplicationImpl implements LogApplication {

	public void saveEntity(AbstractEntity entity) {
		entity.save();
	}

	public void removeEntity(long logId) {
		AbstractEntity entity = AbstractEntity.get(UserLog.class, logId);
		entity.remove();
	}

	public List<UserLog> findAll(Page page, Date fromDate, Date toDate) {
		page.setDataTotal(AbstractEntity.findAll(UserLog.class,null,fromDate,toDate).size());
		return AbstractEntity.findAll(UserLog.class,page,fromDate,toDate);
	}

}

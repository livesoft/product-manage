package com.dayatang.product.application;

import java.util.Date;
import java.util.List;

import com.dayatang.product.Page;
import com.dayatang.product.log.AbstractEntity;
import com.dayatang.product.log.UserLog;

/**
 * 文件名:LogApplication.java
 * 描述:日志模块接口
 * 创建时间:2012-6-19 下午5:04:02
 * 创建者:mluo
 * 版本号:v2.0
 */
public interface LogApplication {
	
	void saveEntity(AbstractEntity entity);
	
	void removeEntity(long logId);

	List<UserLog> findAll(Page page, Date fromDate, Date toDate);
}



package com.dayatang.product.log;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.validation.ValidationException;

import com.dayatang.domain.Entity;
import com.dayatang.domain.InstanceFactory;
import com.dayatang.product.Page;
import com.dayatang.product.repository.EntityRepository;

/**
 * 文件名:AbstractEntity.java
 * 描述:抽象实体类，可作为所有领域实体的基类，提供ID和版本属性。
 * 创建时间:2012-6-19 下午5:19:04
 * 创建者:mluo
 * 版本号:v2.0
 */
@MappedSuperclass
public abstract class AbstractEntity implements Entity {

	private static final long serialVersionUID = -6040126886605953455L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	private static EntityRepository logRepository;
	
	@Inject
	public static void setRepository(EntityRepository logRepository) {
		AbstractEntity.logRepository = logRepository;
	}
	
	public static EntityRepository getLogRepository() {
		if (logRepository == null) {
			logRepository = InstanceFactory.getInstance(EntityRepository.class);
		}
		return logRepository;
	}

	public void save() throws ValidationException {
		getLogRepository().save(this);
	}

	public void remove() {
		logRepository.remove(this);
	}
	
	public static <T extends AbstractEntity> List<T> findAll(Class<T> clazz, Page page, Date fromDate, Date toDate) {
		return getLogRepository().findAll(clazz,page,fromDate,toDate);
	}
	
	public static <T extends AbstractEntity> T get(Class<T> clazz, Serializable id) {
		return getLogRepository().get(clazz, id);
	}

	
}



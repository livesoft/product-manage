package com.dayatang.product.log;

import java.io.Serializable;

/**
 * 文件名:Entity.java
 * 描述:实体的基接口
 * 创建时间:2012-6-19 下午3:31:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public interface Entity extends Serializable {
	
	/**
	 * 实体Id必须是可序列化的
	 * @return 实体实例的 Id
	 */
	Serializable getId();
}



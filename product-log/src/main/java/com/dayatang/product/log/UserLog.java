package com.dayatang.product.log;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * 文件名:UserLog.java
 * 描述:用户日志记录
 * 创建时间:2012-6-19 下午3:35:47
 * 创建者:mluo
 * 版本号:v2.0
 */

@Entity
@Table(name = "user_logs")
public class UserLog extends AbstractEntity  {

	private static final long serialVersionUID = -560201079138529405L;

	private String name;
	
	@NotNull
	private String username;
	
	@Column(name = "module_name")
	private String moduleName;
	
	private String operate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "option_date")
	private Date optionDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public Date getOptionDate() {
		return optionDate;
	}

	public void setOptionDate(Date optionDate) {
		this.optionDate = optionDate;
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((optionDate == null) ? 0 : optionDate.hashCode());
		return result;
	}
	
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (!(that instanceof UserLog))  {
			return false;
		}
		UserLog other = (UserLog) that;
		return new EqualsBuilder().append(this.moduleName, other.getModuleName()).append(this.optionDate, other.getOptionDate()).isEquals();
	}
	
	public String toString() {
		return moduleName + ":" + operate;
	}
	
}



package com.dayatang.product.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.dayatang.product.Page;
import com.dayatang.product.log.AbstractEntity;

/**
 * 文件名:EntityRepository.java
 * 描述:基本仓储接口
 * 创建时间:2012-6-19 下午4:44:05
 * 创建者:mluo
 * 版本号:v2.0
 */
public interface EntityRepository {
	
	<T extends AbstractEntity> T save(T entity);
	
	void remove(AbstractEntity entity);
	
	<T extends AbstractEntity> T get(Class<T> clazz, Serializable id);
	
	<T extends AbstractEntity> List<T> findAll(Class<T> clazz, Page page, Date fromDate, Date toDate);
	
	<T extends AbstractEntity> List<T> findByNamedQuery(final String queryName, final Object[] params, final Class<T> resultClass);
	
}


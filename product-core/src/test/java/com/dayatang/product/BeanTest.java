package com.dayatang.product;

/*
 * 文 件 名:  BeanTest.java
 * 版    权:  Huawei Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  通过反射测试实体类的get,set,toString方法
 * 修 改 人:  lvwu
 * 修改时间:  2009-12-29
 * 修改内容:  新增
 */

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * 通过反射测试实体类的get,set,toString,以及Info类当中的一些方法。
 * 
 * @author lvwu
 * @version [版本号, Dec 29, 2009]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BeanTest extends TestCase
{
    /** 数字类型的值 */
    public static final int NUMBER_VALUE = 1;
    
    /** boolean类型的值 */
    public static final boolean BOOLEAN_VALUE = true;
    
    /** char类型的值 */
    public static final char CHAR_VALUE = 'c';
    
    /** String类型的值 */
    public static final String STRING_VALUE = "StringValue";
    
    /** set字符串 */
    private static final String SET = "set";
    
    /** get字符串 */
    private static final String GET = "get";
    
    /** is字符串 */
    private static final String IS = "is";
    
    /** 数字：3 */
    private static final int THREE = 3;
    
    /** 数字：2 */
    private static final int TWO = 2;
    
    /**
     * 测试实体类，如果没有覆盖完整，可以通过该对象设置好的值，再进行处理
     * 
     * @param obj 当前Object对象
     * @see [类、类#方法、类#成员]
     */
    public void testBean(Object obj)
    {
        Class currentClass = obj.getClass();
        
        // 获得当前对象当中所有的字段
        Map filedMap = getFields(currentClass);
        
        Method[] methods = currentClass.getMethods();
        
        try
        {
            // 通过反射set方法设置相应值到对象当中
            invokeSetter(obj, filedMap, methods);
            
            // 通过反射get方法，并执行单元测试
            invokeGetter(obj, filedMap, methods);
            
            // 测试toString方法
            testMethod("toString", currentClass, obj);
            
            // 测试Info类当中 transToArrayList方法
            testMethod("transToArrayList", currentClass, obj);
            
            // 测试Info类当中 transPrimarykeyToArrayList 方法
            testMethod("transPrimarykeyToArrayList", currentClass, obj);
            
            // 测试Info类当中getTableName方法
            testMethod("getTableName", currentClass, obj);
        }
        catch (Exception ex)
        {
            Assert.fail(ex.toString());
        }
    }
    
    /**
     * 将对象当中的字段的值都设置好
     * 
     * @param obj 当前对象
     * @see [类、类#方法、类#成员]
     */
    public void set(Object obj)
    {
        Class currentClass = obj.getClass();
        
        // 获得当前对象当中所有的字段
        Map filedMap = getFields(currentClass);
        
        Method[] methods = currentClass.getMethods();
        
        try
        {
            // 通过反射set方法设置相应值到对象当中
            invokeSetter(obj, filedMap, methods);
        }
        catch (Exception e)
        {
            Assert.fail(e.toString());
        }
    }
    
    /**
     * 测试对象当中已经有数据的对象
     * 
     * @param obj 当前对象
     * @see [类、类#方法、类#成员]
     */
    public void testObject(Object obj)
    {
        Class currentClass = obj.getClass();
        
        // 获得当前对象当中所有的字段
        Map filedMap = getFields(currentClass);
        
        Method[] methods = currentClass.getMethods();
        
        try
        {
            // 通过反射get方法，并执行单元测试
            invokeGetter(obj, filedMap, methods);
            
            // 测试toString方法
            testMethod("toString", currentClass, obj);
            
            // 测试Info类当中 transToArrayList方法
            testMethod("transToArrayList", currentClass, obj);
            
            // 测试Info类当中 transPrimarykeyToArrayList 方法
            testMethod("transPrimarykeyToArrayList", currentClass, obj);
            
            // 测试Info类当中getTableName方法
            testMethod("getTableName", currentClass, obj);
        }
        catch (Exception ex)
        {
            Assert.fail(ex.toString());
        }
    }
    
    /**
     * 单元测试代码测试类
     * 
     * @param clss Class数组
     * @see [类、类#方法、类#成员]
     */
    public void testBeans(Class[] clss)
    {
        for (int i = 0; i < clss.length; i++)
        {
            this.testBean(clss[i]);
        }
    }
    
    /**
     * 单元测试代码测试类
     * 
     * @param objs Object数组
     * @see [类、类#方法、类#成员]
     */
    public void testBeans(Object[] objs)
    {
        for (int i = 0; i < objs.length; i++)
        {
            this.testBean(objs[i]);
        }
    }
    
    /**
     * 测试实体类
     * 
     * @param cls Class
     * @see [类、类#方法、类#成员]
     */
    public void testBean(Class cls)
    {
        try
        {
            // 当前对象
            Object currentObj = cls.newInstance();
            
            // 获得当前对象当中所有的字段
            Map filedMap = getFields(cls);
            
            Method[] methods = cls.getMethods();
            
            // 通过反射set方法设置相应值到对象当中
            invokeSetter(currentObj, filedMap, methods);
            
            // 通过反射get方法，并执行单元测试
            invokeGetter(currentObj, filedMap, methods);
            
            // 测试toString方法
            testMethod("toString", cls, currentObj);
            
            // 测试Info类当中 transToArrayList方法
            testMethod("transToArrayList", cls, currentObj);
            
            // 测试Info类当中 transPrimarykeyToArrayList 方法
            testMethod("transPrimarykeyToArrayList", cls, currentObj);
            
            // 测试Info类当中getTableName方法
            testMethod("getTableName", cls, currentObj);
        }
        catch (Exception ex)
        {
            Assert.fail(ex.toString());
        }
    }
    
    /**
     * 获得当前对象当中所有字段
     * 
     * @param cls Class
     * @return Map{字段名称,字段类型}
     * @see [类、类#方法、类#成员]
     */
    private Map getFields(Class cls)
    {
        // 获取对象当中所有的属性
        Field[] fields = cls.getDeclaredFields();
        
        Field field = null;
        Map filedMap = new HashMap();
        for (int i = 0; i < fields.length; i++)
        {
            field = fields[i];
            filedMap.put(field.getName(), field.getType());
        }
        return filedMap;
    }

    /**
     * 执行并测试get方法
     * 
     * @param currentObj 当前对象
     * @param filedMap 字段属性
     * @param methods 所有方法
     * @throws Exception 执行异常
     * @see [类、类#方法、类#成员]
     */
    private void invokeGetter(Object currentObj, Map filedMap, Method[] methods)
        throws Exception
    {
    	
        Method method;
        String methodName;
        Object getResult = null;
        Class returnType = null;
        String returnTypeName = null;
        String fieldName = null;
        System.out.println("++aa" + methods.length);
        for (int i = 0; i < methods.length; i++)
        { 
            method = methods[i];
            methodName = method.getName();
            
            // 如果方法名称不以get且不以is开头，则继续下一个
            if (!methodName.startsWith(GET) && !methodName.startsWith(IS))
            {
                continue;
            }
            
            if (methodName.startsWith(GET))
            {
                fieldName = methodName.substring(THREE, methodName.length()); 
            }
            else
            {
                fieldName = methodName.substring(TWO, methodName.length()); 
            }
            
            fieldName = startCharToLowerCase(fieldName);
            
            // 判断当前get方法是否是该属性中有的
            if (!filedMap.containsKey(fieldName))
            {
                continue;
            }
           
            // 获取结果
            getResult = method.invoke(currentObj, null);
            System.out.println("++");
            returnType = method.getReturnType();
            returnTypeName = returnType.getName().trim();
            // 单元测试代码覆盖，测试结果
            testResult(getResult, returnTypeName);
        }
    }

    /**
     * 进行单元测试代码覆盖
     * 
     * @param getResult get方法执行结果
     * @param returnTypeName 返回参数类型
     * @see [类、类#方法、类#成员]
     */
    private void testResult(Object getResult, String returnTypeName)
    {
        if ("int".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(NUMBER_VALUE, ((Integer)getResult).intValue());
        }
        else if ("short".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(NUMBER_VALUE, ((Short)getResult).intValue());
        }
        else if ("long".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(NUMBER_VALUE, ((Long)getResult).intValue());
        }
        else if ("double".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(NUMBER_VALUE, ((Double)getResult).intValue());
        }
        else if ("float".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(NUMBER_VALUE, ((Float)getResult).intValue());
        }
        else if ("boolean".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(BOOLEAN_VALUE, ((Boolean)getResult).booleanValue());
        }
        else if ("char".equalsIgnoreCase(returnTypeName))
        {
            super.assertEquals(CHAR_VALUE, ((Character)getResult).charValue());
        }
        else if ("java.lang.String".equalsIgnoreCase(returnTypeName))
        {
            super.assertNotNull(getResult);
        }
        else
        {
            if (null == getResult)
            {
                super.assertNull(getResult);
            }
            else
            {
                super.assertNotNull(getResult);
            }
        }
    }

    /**
     * 反射执行set方法，设置值
     * 
     * @param currentObj 当前对象
     * @param filedMap 对象当中字段值
     * @param methods 对象当中所有方法
     * @throws Exception 设置异常
     * @see [类、类#方法、类#成员]
     */
    private void invokeSetter(Object currentObj, Map filedMap, Method[] methods)
        throws Exception
    {
        Method method;
        String methodName;
        
        // 实体类的set方法始终只有一个参数
        Object[] values = new Object[1];
        String fieldName = null;
        for (int i = 0; i < methods.length; i++)
        {
            method = methods[i];
            methodName = method.getName();
            if (!methodName.startsWith(SET))
            {
                continue;
            }
            
            // 截掉set字符
            fieldName = methodName.substring(THREE, methodName.length());
            
            // 将首字母变成小写
            fieldName = startCharToLowerCase(fieldName);
            
            // 如果该实体类当中不包含该字段，则退出
            if (!filedMap.containsKey(fieldName))
            {
                continue;
            }
            
            // 获取参数类型，由于实体类的只有一个参数，则只取一个参数的类型
            
            Class[] types = method.getParameterTypes();
            Class type = types[0];

            if (type.isArray())
            {
                values[0] = null;
            }
            else
            {
                values[0] = getValueByType(type.getName());
            }
            
            method.invoke(currentObj, values);
        }
    }
    
    /**
     * 测试实体类当中的方法，该方法可能不存在
     * 
     * @param methodName 方法名称
     * @param cls Class
     * @param currentObj 当前对象
     * @throws Exception 执行异常
     * @see [类、类#方法、类#成员]
     */
    private void testMethod(String methodName, Class cls, Object currentObj) throws Exception
    {
        // 如果类中不存在该方法，则直接返回，不做处理
        Method method = null;
        try
        {
            method = cls.getMethod(methodName, null);
        }
        catch (SecurityException e)
        {
            return;
        }
        catch (NoSuchMethodException e)
        {
            return;
        }
        
        if (null != method)
        {
            Object result = method.invoke(currentObj, null);
            if (null != result)
            {
                super.assertNotNull(result);
                return;
            }
            
            super.assertNull(result);
        }
    }
    
    /**
     * 获取方法名，让首字母小写
     * 
     * @param str 当前字符串
     * @return 转换过后的字符串
     * @see [类、类#方法、类#成员]
     */
    private String startCharToLowerCase(String str)
    {
        str = str.replaceFirst(str.substring(0, 1), 
            str.substring(0, 1).toLowerCase(Locale.US));
        return str;
    }
    
    /**
     * 根据类型返回相应的值
     * 
     * @param type 类型字符串
     * @return 设置好的值
     * @see [类、类#方法、类#成员]
     */
    private Object getValueByType(Object type)
    {
        String temp = type.toString().trim();
        
        if ("int".equals(temp))
        {
            return new Integer(NUMBER_VALUE);
        }
        else if ("double".equals(temp))
        {
            return new Double(NUMBER_VALUE);
        }
        else if ("boolean".equals(temp))
        {
            return new Boolean(BOOLEAN_VALUE);
        }
        else if ("java.lang.String".equals(temp))
        {
            return STRING_VALUE;
        }
        else if ("char".equals(temp))
        {
            return new Character(CHAR_VALUE);
        }
        else if ("float".equals(temp))
        {
            return new Float(NUMBER_VALUE);
        }
        else if ("long".equals(temp))
        {
            return new Long(NUMBER_VALUE);
        }
        else if ("short".equals(temp))
        {
            return new Short(String.valueOf(NUMBER_VALUE));
        }
        else if ("java.util.List".equals(temp) || "java.util.ArrayList".equals(temp))
        {
            return new ArrayList();
        }
        return null;
    }
    
    /**
     * 让其编译通过
     */
    public void test()
    {
        super.assertEquals(true, true);
    }
}


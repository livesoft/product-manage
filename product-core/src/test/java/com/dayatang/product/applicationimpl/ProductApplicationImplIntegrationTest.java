package com.dayatang.product.applicationimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.dayatang.product.AbstractIntegrationTest;
import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.domain.InfoCategory;
import com.dayatang.product.domain.Organization;

public class ProductApplicationImplIntegrationTest extends AbstractIntegrationTest {
	
	@Test
	public void saveEntity() {
		assertNotNull(createNewCustomerInfo());
	}
	
	@Test
	public void removeEntity() {
		CustomerInfo customerInfo = createNewCustomerInfo();
		assertNotNull(customerInfo);
		application.removeEntity(customerInfo);
		customerInfo = CustomerInfo.get(CustomerInfo.class, customerInfo.getId());
		assertNull(customerInfo);
	}
	
	private CustomerInfo createNewCustomerInfo() {
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setCompanyName("广州大雅堂信息科技有限公司");
		customerInfo.setAddress("云景花园316室");
		customerInfo.setCategory(InfoCategory.CUSTOMERINFO);
		return application.saveEntity(customerInfo);
	}
	
	//@Test
	public void createLowerOrganization() {
		Organization parent = createParentOrg();
		Organization marketOrg =  new Organization("市场部");
		application.createLowerOrganization(marketOrg,parent);
		assertEquals(Integer.valueOf(2),marketOrg.getLeftValue());
		assertEquals(Integer.valueOf(3),marketOrg.getRightValue());
		assertTrue(marketOrg.getLevel() - parent.getLevel() == 1);
		System.out.println(parent.getChildren().size());
		assertTrue(parent.getChildren().contains(marketOrg));
		
		Organization softwareOrg =  new Organization("软件部");
		application.createLowerOrganization(softwareOrg,parent);
		assertEquals(Integer.valueOf(4),softwareOrg.getLeftValue());
		assertEquals(Integer.valueOf(5),softwareOrg.getRightValue());
		assertTrue(softwareOrg.getLevel() - parent.getLevel() == 1);
		assertTrue(parent.getChildren().contains(marketOrg));
		
		assertEquals(Integer.valueOf(6),parent.getRightValue());
		
		application.removeEntity(parent);
		application.removeEntity(marketOrg);
		application.removeEntity(softwareOrg);
	}
	
	private Organization createParentOrg() {
		Organization result = new Organization("root");
		result.setLeftValue(1);
		result.setRightValue(2);
		result.setLevel(0);
		return application.saveEntity(result);
	}
	
	@Test
	public void applyTrial() {
		
	}
	
}

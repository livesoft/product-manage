package com.dayatang.product.domain;


import static org.junit.Assert.*;

import org.junit.Test;

import com.dayatang.product.AbstractIntegrationTest;

/**
 * 文件名:OrganizationTest.java
 * 描述:组织机构单元测试类
 * 创建时间:2012-5-21 下午2:55:04
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationTest extends AbstractIntegrationTest {

	@Test
	public void getChildren() {
		Organization parent = createParentOrg();
		Organization market = new Organization("市场部");
		application.createLowerOrganization(market, parent);
		Organization software = new Organization("软件部");
		application.createLowerOrganization(software, parent);
		Organization java = new Organization("JAVA组");
		application.createLowerOrganization(java, software);
		
		System.out.println(parent.getChildren().size());
		
		application.removeEntity(parent);
		application.removeEntity(market);
		application.removeEntity(software);
		application.removeEntity(java);
		
	}
	
	private Organization createParentOrg() {
		Organization result = new Organization("root");
		result.setLeftValue(1);
		result.setRightValue(2);
		result.setLevel(0);
		return application.saveEntity(result);
	}
	
	@Test
	public void getRootOrg() {
		Organization rootOrg = Organization.getRootOrg();
		assertNull(rootOrg);
		rootOrg = createParentOrg();
		assertNotNull(rootOrg);
		assertEquals(Integer.valueOf(0), rootOrg.getLevel());
		application.removeEntity(rootOrg);
	}

}



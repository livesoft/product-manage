package com.dayatang.product;

import org.junit.After;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.unitils.UnitilsJUnit4;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import com.dayatang.domain.InstanceFactory;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.spring.factory.SpringProvider;

@SpringApplicationContext("/applicationContext.xml")
public abstract class AbstractIntegrationTest extends UnitilsJUnit4 {
	
	@SpringApplicationContext
	private ApplicationContext applicationContext;

	@SpringBeanByType
	protected ProductApplication application;
	
	@Before
	public void setUp() throws Exception {
		SpringProvider provider = new SpringProvider(applicationContext);
		InstanceFactory.setInstanceProvider(provider);
	}

	@After
	public void tearDown() throws Exception {
		InstanceFactory.setInstanceProvider(null);
	}

}

package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:DownloadInfo.java
 * 描述:下载试用产品客户信息登记表 
 * 创建时间:2012-6-3 下午10:27:17
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
public class DownloadInfo extends Customer {

	private static final long serialVersionUID = -1356767877146686730L;

	private String contact;
	
	private String mobile;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "apply_date")
	private Date applyDate;
	
	// 获取渠道
	private String trench;
	
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	
	public String getTrench() {
		return trench;
	}

	public void setTrench(String trench) {
		this.trench = trench;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(contact).append(mobile).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof RegisterInfo))
			return false;
		DownloadInfo castOther = (DownloadInfo) other;
		return new EqualsBuilder().append(contact, castOther.contact).append(mobile, castOther.mobile).isEquals();
	}

}

package com.dayatang.product.domain;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "product_trials")
public class ProductTrial extends MyAbstractEntity {

	private static final long serialVersionUID = 6890452084678982686L;

	@ManyToOne
	@JoinColumn(name = "customer_info_id")
	private CustomerInfo customerInfo;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@OneToOne
	@JoinColumn(name = "trial_code_id")
	private TrialCode trialCode;
	
	@OneToOne
	@JoinColumn(name = "register_code_id")
	private RegisterCode registerCode;
	
	// 试用期
	@Column(name = "trial_period")
	private long trialPeriod;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private TrialStatus status;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public TrialCode getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(TrialCode trialCode) {
		this.trialCode = trialCode;
	}

	public RegisterCode getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(RegisterCode registerCode) {
		this.registerCode = registerCode;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public long getTrialPeriod() {
		return trialPeriod;
	}

	public void setTrialPeriod(long trialPeriod) {
		this.trialPeriod = trialPeriod;
	}

	public TrialStatus getStatus() {
		return status;
	}

	public void setStatus(TrialStatus status) {
		this.status = status;
	}

	public static ProductTrial getProductTrialByTrialCode(String trialCode) {
		return getRepository().getSingleResult(QuerySettings.create(ProductTrial.class).eq("trialCode.trialNum", trialCode));
	}
	
	public static ProductTrial getProductTrialByTrialCode(TrialCode trialCode) {
		return getRepository().getSingleResult(QuerySettings.create(ProductTrial.class).eq("trialCode", trialCode));
	}
	
	public static ProductTrial getProductTrialByRegisterCode(String registerCode) {
		return getRepository().getSingleResult(QuerySettings.create(ProductTrial.class).eq("registerCode.registerCode", registerCode));
	}
	
	public static ProductTrial getProductTrialByRegisterCode(RegisterCode registerCode) {
		return getRepository().getSingleResult(QuerySettings.create(ProductTrial.class).eq("registerCode", registerCode));
	}
	
	public static List<ProductTrial> findByStatus(TrialStatus status) {
		return getRepository().find(QuerySettings.create(ProductTrial.class).eq("status", status));
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(trialCode).append(registerCode).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof ProductTrial))
			return false;
		ProductTrial castOther = (ProductTrial) other;
		return new EqualsBuilder().append(trialCode, castOther.trialCode).append(registerCode, castOther.registerCode).isEquals();
	}

	public String toString() {
		return trialCode + ":" + registerCode;
	}

}

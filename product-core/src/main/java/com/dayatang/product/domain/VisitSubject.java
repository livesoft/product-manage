package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:VisitSubject.java
 * 描述:回访主题
 * 创建时间:2012-6-26 上午11:03:24
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "visit_subjects")
@NamedQueries({
	@NamedQuery(name = "findByState", query = "select v from VisitSubject v where v.state = ?")
})
public class VisitSubject extends MyAbstractEntity {

	private static final long serialVersionUID = 477080498682441619L;

	private String subject;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String content;
	
	// 状态
	private boolean state;
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean getIsState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(subject).append(content).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof VisitSubject))
			return false;
		VisitSubject castOther = (VisitSubject) other;
		return new EqualsBuilder().append(this.subject, castOther.getSubject()).append(this.content, castOther.getContent()).isEquals();
	}

	public String toString() {
		return subject + ":" + content;
	}
	
	/**
	 * 查找可用的主题
	 * @return
	 */
	public static List<VisitSubject> findStateByActive() {
		return getRepository().findByNamedQuery("findByState", new Object[]{true}, VisitSubject.class);
	}

}


package com.dayatang.product.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "product_exceptions")
public class ProductException extends MyAbstractEntity {

	private static final long serialVersionUID = -2511201517659881832L;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "product_trial")
	private ProductTrial productTrial;
	
	private String ip;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "exception_date")
	private Date exceptionDate;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "exception_text")
	private String exceptionText;
	
	@Column(name = "action_name")
	private String actionName;
	
	// 异常处理情况
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private ExceptionDisposeCategory category;
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getExceptionDate() {
		return exceptionDate;
	}

	public void setExceptionDate(Date exceptionDate) {
		this.exceptionDate = exceptionDate;
	}

	public String getExceptionText() {
		return exceptionText;
	}

	public void setExceptionText(String exceptionText) {
		this.exceptionText = exceptionText;
	}
	
	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	
	public ExceptionDisposeCategory getCategory() {
		return category;
	}

	public void setCategory(ExceptionDisposeCategory category) {
		this.category = category;
	}

	public static List<ProductException> getProductExceptionsByProduct(Product product) {
		return getRepository().find(QuerySettings.create(ProductException.class).eq("product", product));
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(exceptionText).append(exceptionDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof ProductException))
			return false;
		ProductException castOther = (ProductException) other;
		return new EqualsBuilder().append(exceptionText, castOther.exceptionText).append(exceptionDate, castOther.exceptionDate).isEquals();
	}

	public String toString() {
		return exceptionText;
	}

}

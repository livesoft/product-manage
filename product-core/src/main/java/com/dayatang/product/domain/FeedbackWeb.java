package com.dayatang.product.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "feedback_webs")
public class FeedbackWeb extends MyAbstractEntity {

	private static final long serialVersionUID = -6692345780824861126L;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	// 帮助
	private String help;
	
	// 简单易用
	private String easy;
	
	// 漂亮美观
	private String beautiful;
	
	// 软件问题
	private String problem;
	
	// 整体评价
	@Column(name = "whole_evaluate")
	private String wholeEvaluate;
	
	// 建议
	private String suggestion;
	
	@Column(name = "company_name")
	private String companyName;
	
	private String email;
	
	private String phone;
	
	private String address;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "feedback_date")
	private Date feedbackDate;
	
	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public String getEasy() {
		return easy;
	}

	public void setEasy(String easy) {
		this.easy = easy;
	}

	public String getBeautiful() {
		return beautiful;
	}

	public void setBeautiful(String beautiful) {
		this.beautiful = beautiful;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getWholeEvaluate() {
		return wholeEvaluate;
	}

	public void setWholeEvaluate(String wholeEvaluate) {
		this.wholeEvaluate = wholeEvaluate;
	}

	public String getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public Date getFeedbackDate() {
		return feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	
	/**
	 * 查找产品的反馈信息
	 * @param product
	 * @return
	 */
	public static List<FeedbackWeb> getFeedbackWebsByProduct(Product product) {
		return getRepository().find(QuerySettings.create(FeedbackWeb.class).eq("product", product));
	}

	public String toString() {
		return companyName + ":" + email;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(companyName).append(email).append(feedbackDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof FeedbackWeb))
			return false;
		FeedbackWeb castOther = (FeedbackWeb) other;
		return new EqualsBuilder().append(companyName, castOther.companyName).append(email, castOther.email).append(feedbackDate, castOther.feedbackDate).isEquals();
	}

}

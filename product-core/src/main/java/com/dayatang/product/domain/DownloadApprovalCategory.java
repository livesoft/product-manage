package com.dayatang.product.domain;
/**
 * 文件名:DownloadApprovalCategory.java
 * 描述:下载试用流程定义
 * 创建时间:2012-6-4 上午9:47:47
 * 创建者:mluo
 * 版本号:v2.0
 */
public enum DownloadApprovalCategory {
	APPLY,                   // 申请试用
	APPROVAL_PASS,           // 审批通过
	APPROVAL_REJECT          // 审批不通过
}



package com.dayatang.product.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "trial_codes")
public class TrialCode extends MyAbstractEntity {

	private static final long serialVersionUID = -5353866501208845822L;

	@Column(name = "trial_num")
	private String trialNum;
	
	// 是否激活
	@Column(name = "is_activate")
	private boolean isActivate = false;
	
	// 有效期
	private long validity;
	
	// 有效期剩余天数
	@Column(name = "validity_day")
	private long validityDay;
	
	// 是否有效
	@Column(name = "is_effective")
	private boolean isEffective = true;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "register_date")
	private Date registerDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "activate_date")
	private Date activateDate;
	
	public String getTrialNum() {
		return trialNum;
	}

	public void setTrialNum(String trialNum) {
		this.trialNum = trialNum;
	}

	public boolean getIsActivate() {
		return isActivate;
	}

	public void setActivate(boolean isActivate) {
		this.isActivate = isActivate;
	}

	public long getValidity() {
		return validity;
	}

	public void setValidity(long validity) {
		this.validity = validity;
	}
	
	public boolean getIsEffective() {
		return isEffective;
	}

	public void setEffective(boolean isEffective) {
		this.isEffective = isEffective;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	public long getValidityDay() {
		return validityDay;
	}

	public void setValidityDay(long validityDay) {
		this.validityDay = validityDay;
	}

	public Date getActivateDate() {
		return activateDate;
	}

	public void setActivateDate(Date activateDate) {
		this.activateDate = activateDate;
	}

	public static TrialCode getTrialCodeByTrialCode(String trialCode) {
		return getRepository().getSingleResult(QuerySettings.create(TrialCode.class).eq("trialNum", trialCode));
	}
	
	/**
	 * 获取没有激活且在有效期内的试用号
	 * @return
	 */
	public static List<TrialCode> getTrialCodeToPeriod() {
		return getRepository().find(QuerySettings.create(TrialCode.class).eq("isActivate", false).eq("isEffective", true));
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(trialNum).append(validityDay).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof TrialCode))
			return false;
		TrialCode castOther = (TrialCode) other;
		return new EqualsBuilder().append(trialNum, castOther.trialNum).append(validityDay, castOther.validityDay).isEquals();
	}

	public String toString() {
		return trialNum;
	}

}

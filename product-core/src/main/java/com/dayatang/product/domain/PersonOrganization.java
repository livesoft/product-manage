package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:PersonOrganization.java
 * 描述:人员机构关联表
 * 创建时间:2012-5-30 下午12:05:17
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "person_organizations")
@NamedQueries({
	@NamedQuery(name = "findByPerson", query = "select p from PersonOrganization p where p.person.id = ?"),
	@NamedQuery(name = "findByOrganization", query = "select p from PersonOrganization p where p.organization.id = ?")
})
public class PersonOrganization extends MyAbstractEntity {

	private static final long serialVersionUID = -1960775238219086310L;

	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person person;
	
	@ManyToOne
	@JoinColumn(name = "organization_id")
	private Organization organization;
	
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(person).append(organization).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof PersonOrganization))
			return false;
		PersonOrganization castOther = (PersonOrganization) other;
		return new EqualsBuilder().append(person, castOther.person).append(organization, castOther.organization).isEquals();

	}

	public String toString() {
		return person.toString() + ":" + organization.toString();
	}

	public static List<PersonOrganization> findByPerson(long personId) {
		return getRepository().findByNamedQuery("findByPerson", new Object[]{personId}, PersonOrganization.class);
	}
	
	
	
}



package com.dayatang.product.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "html_mails")
public class HtmlMail extends MyAbstractEntity {

	private static final long serialVersionUID = 8904168907959024440L;

	private String subject;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	@ManyToOne
	@JoinColumn(name = "email_info_id")
	private EmailInfo emailInfo;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "html_msg")
	private String htmlMsg;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "text_msg")
	private String textMsg;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinTable(name = "mail_doc_attachments", 
		joinColumns = @JoinColumn(name = "mail_id"),
		inverseJoinColumns = @JoinColumn(name = "attachment_id"))
	private Set<Attachment> attachments = new HashSet<Attachment>();
	
	public String getHtmlMsg() {
		return htmlMsg;
	}

	public void setHtmlMsg(String htmlMsg) {
		this.htmlMsg = htmlMsg;
	}

	public String getTextMsg() {
		return textMsg;
	}

	public void setTextMsg(String textMsg) {
		this.textMsg = textMsg;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	public static long getLashHtmlMail() {
		return findAll(HtmlMail.class).get(0).getId();
	}
	
	public boolean equals(final Object other) {
		if (this == other)
			return true;
		if (!(other instanceof HtmlMail))
			return false;
		HtmlMail castOther = (HtmlMail) other;
		return new EqualsBuilder().append(htmlMsg, castOther.htmlMsg).append(textMsg, castOther.textMsg).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(htmlMsg).append(textMsg).toHashCode();
	}

	public String toString() {
		return subject + ":" + htmlMsg;
	}

}

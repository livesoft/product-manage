package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:SystemException.java
 * 描述:系统异常记录表
 * 创建时间:2012-7-2 下午2:01:37
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "system_exceptions")
public class SystemException extends MyAbstractEntity {

	private static final long serialVersionUID = 4453097550305316759L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "exception_date")
	private Date exceptionDate;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "exception_text")
	private String exceptionText;
	
	@Column(name = "action_name")
	private String actionName;
	
	private String name;
	
	public Date getExceptionDate() {
		return exceptionDate;
	}

	public void setExceptionDate(Date exceptionDate) {
		this.exceptionDate = exceptionDate;
	}

	public String getExceptionText() {
		return exceptionText;
	}

	public void setExceptionText(String exceptionText) {
		this.exceptionText = exceptionText;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(exceptionText).append(exceptionDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof SystemException))
			return false;
		SystemException castOther = (SystemException) other;
		return new EqualsBuilder().append(exceptionText, castOther.exceptionText).append(exceptionDate, castOther.exceptionDate).isEquals();
	}

	public String toString() {
		return exceptionText;
	}

}



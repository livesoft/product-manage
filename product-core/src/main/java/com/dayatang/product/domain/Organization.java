package com.dayatang.product.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "organizations")
@NamedQueries({
	@NamedQuery(name = "getChildren", query = "select o from Organization o where o.leftValue > ? and o.rightValue < ? and level = ?"),
	@NamedQuery(name = "getRootOrg", query = "select o from Organization o where o.leftValue = ? and o.level = ?")
})
public class Organization extends MyAbstractEntity {

	private static final long serialVersionUID = -702898523615216232L;

	private String name;
	
	@Column(name = "org_describe")
	private String orgDescribe;
	
	// 负责人
	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person person;
	
	// 引入每个机构的左、右值及等级
	@Column(name = "left_value")
	private Integer leftValue = 100000;
	
	@Column(name = "right_value")
	private Integer rightValue = 100001;
	
	private Integer level = 0;
	
	@Column(name = "organization_type")
	private String organizationType;
	
	public Organization() {
	}
	
	public Organization(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrgDescribe() {
		return orgDescribe;
	}

	public void setOrgDescribe(String orgDescribe) {
		this.orgDescribe = orgDescribe;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	public Integer getLeftValue() {
		return leftValue;
	}

	public void setLeftValue(Integer leftValue) {
		this.leftValue = leftValue;
	}

	public Integer getRightValue() {
		return rightValue;
	}

	public void setRightValue(Integer rightValue) {
		this.rightValue = rightValue;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(name).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Organization))
			return false;
		Organization castOther = (Organization) other;
		return new EqualsBuilder().append(name, castOther.name).isEquals();
	}

	public String toString() {
		return name;
	}

	public Set<Organization> getChildren() {
		return new HashSet<Organization>(getRepository().findByNamedQuery("getChildren", new Object[]{leftValue, rightValue, level + 1}, Organization.class));
	}

	public static Organization getRootOrg() {
		// 找出原因
		// getRepository().getSingleResult("getRootOrg", new Object[]{1,0}, Organization.class)
		return getRepository().getSingleResult(QuerySettings.create(Organization.class).eq("level", 0).eq("leftValue", 1));
	}

}

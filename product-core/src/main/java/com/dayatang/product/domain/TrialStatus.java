package com.dayatang.product.domain;

public enum TrialStatus {
	NEW,             // 新增试用号
	DATED,           // 试用号过期
	ACTIVATE,        // 试用号已激活
	USEING,          // 使用中
	OVERTIME         // 试用结束
}

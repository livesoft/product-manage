package com.dayatang.product.domain;

public enum InfoCategory {
	REGISTERINFO,         //注册客户信息
	CUSTOMERINFO,         //公司客户信息
	DOWNLOADINFO          //下载客户信息
}

package com.dayatang.product.domain;

import java.util.List;

import com.dayatang.domain.AbstractEntity;
import com.dayatang.domain.Entity;
import com.dayatang.domain.QuerySettings;
import com.dayatang.product.Page;

public abstract class MyAbstractEntity extends AbstractEntity {

	private static final long serialVersionUID = 3091199674592446378L;

	public static <T extends Entity> List<T> findAll(Class<T> clazz, Page page) {
		if (page == null) {
			return getRepository().find(QuerySettings.create(clazz).desc("id"));
		}
		page.setDataTotal(findAll(clazz).size());
		return getRepository().find(QuerySettings.create(clazz).desc("id").setFirstResult(page.getFirstResult()).setMaxResults(page.getPageSize()));
	}
	
	public static <T extends Entity> List<T> findAll(Class<T> clazz) {
		return findAll(clazz,null);
	}
	
	public static <T extends Entity> List<T> findByQuerySettings(QuerySettings<T> querySettings) {
		return getRepository().find(querySettings);
	}
	
}

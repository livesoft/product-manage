package com.dayatang.product.domain;

public enum DictionaryCategory {
	PROFESSION_TYPE,			//行业类型
	ORGANIZATION_TYPE           //组织类型
}
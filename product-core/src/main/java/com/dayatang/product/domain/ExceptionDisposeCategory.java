package com.dayatang.product.domain;

public enum ExceptionDisposeCategory {
	NODISPOSE,         // 未处理
	DISPOSEING,        // 处理中
	DISPOSEFAILURE,    // 处理失败   
	DISPOSESUCCESS     // 处理成功
}

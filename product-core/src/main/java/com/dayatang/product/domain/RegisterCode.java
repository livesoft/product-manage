package com.dayatang.product.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "register_codes")
public class RegisterCode extends MyAbstractEntity {

	private static final long serialVersionUID = -433250188782075400L;

	@Column(name = "register_code")
	private String registerCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "register_date")
	private Date registerDate;
	
	// 是否超过试用期
	@Column(name = "is_out_date")
	private boolean isOutDate = false;
	
	@Column(name = "trial_day")
	private long trialDay;
	
	// 是否激活
	@Column(name = "is_activate")
	private boolean isActivate = false;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "activate_date")
	private Date activateDate;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinTable(name = "register_code_count", 
		joinColumns = @JoinColumn(name = "code_id"),
		inverseJoinColumns = @JoinColumn(name = "count_id"))
	private Set<Count> counts = new HashSet<Count>();

	public String getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public boolean getIsOutDate() {
		return isOutDate;
	}

	public void setOutDate(boolean isOutDate) {
		this.isOutDate = isOutDate;
	}

	public long getTrialDay() {
		return trialDay;
	}

	public void setTrialDay(long trialDay) {
		this.trialDay = trialDay;
	}
	
	public boolean getIsActivate() {
		return isActivate;
	}

	public void setActivate(boolean isActivate) {
		this.isActivate = isActivate;
	}

	public Set<Count> getCounts() {
		return new HashSet<Count>(counts);
	}

	public void setCounts(Set<Count> counts) {
		this.counts = counts;
	}

	public void addCount(Count count) {
		counts.add(count);
	}
	
	public Date getActivateDate() {
		return activateDate;
	}

	public void setActivateDate(Date activateDate) {
		this.activateDate = activateDate;
	}

	public static RegisterCode getRegisterCodeByCode(String registerCode) {
		return getRepository().getSingleResult(QuerySettings.create(RegisterCode.class).eq("registerCode", registerCode));
	}
	
	public static void checkCode(RegisterCode registerCode) {
		registerCode.setActivate(true);
		getRepository().save(registerCode);
	}
	
	/**
	 * 获取激活的且在试用期内的注册码
	 * @return
	 */
	public static List<RegisterCode> getRegisterCodeToTrial() {
		return getRepository().find(QuerySettings.create(RegisterCode.class).eq("isActivate", true).eq("isOutDate", false));
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(registerCode).append(registerCode).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof RegisterCode))
			return false;
		RegisterCode castOther = (RegisterCode) other;
		return new EqualsBuilder().append(registerCode, castOther.registerCode).append(activateDate, castOther.activateDate).isEquals();
	}

	public String toString() {
		return registerCode;
	}

}

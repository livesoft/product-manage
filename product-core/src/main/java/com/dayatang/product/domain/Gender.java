package com.dayatang.product.domain;
/**
 * 文件名:Gender.java
 * 描述:人员性别枚举
 * 创建时间:2012-5-28 下午9:47:52
 * 创建者:mluo
 * 版本号:v2.0
 */
public enum Gender {
	MALE,
	FEMALE
}

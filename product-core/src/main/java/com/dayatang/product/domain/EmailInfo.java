package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "email_infos")
public class EmailInfo extends MyAbstractEntity {

	private static final long serialVersionUID = 7400061010416858393L;

	private String username;
	
	private String password;
	
	@Column(name = "smtp_name")
	private String smtpName;
	
	@Column(name = "smtp_port")
	private int smtpPort;
	
	@Column(name = "is_usable")
	private boolean isUsable = false;
	
	private String name;
	
	private String remark;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSmtpName() {
		return smtpName;
	}

	public void setSmtpName(String smtpName) {
		this.smtpName = smtpName;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public boolean getIsUsable() {
		return isUsable;
	}

	public void setUsable(boolean isUsable) {
		this.isUsable = isUsable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 查找可用的邮箱 
	 * @return
	 */
	public static List<EmailInfo> getUsableEmialInfo() {
		return getRepository().find(QuerySettings.create(EmailInfo.class).eq("isUsable", true));
	}
	
	public static EmailInfo getDefault() {
		return getRepository().getSingleResult(QuerySettings.create(EmailInfo.class).eq("remark", "DefaultEmail"));
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(username).append(password).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof EmailInfo))
			return false;
		EmailInfo castOther = (EmailInfo) other;
		return new EqualsBuilder().append(username, castOther.username).append(password, castOther.password).isEquals();
	}

	public String toString() {
		return username + ":" + password;
	}

}

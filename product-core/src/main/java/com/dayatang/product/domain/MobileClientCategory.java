package com.dayatang.product.domain;

public enum MobileClientCategory {
	ANDROID,          //android客户端
	IPHONE            //iphone客户端
}

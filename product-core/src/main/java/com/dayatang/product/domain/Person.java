package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@Entity
@Table(name = "persons")
public class Person extends MyAbstractEntity {

	private static final long serialVersionUID = 4559655948747318152L;

	private String name;
	
	@Enumerated
	private Gender gender;

	private String mobile;
	
	private String email;
	
	private String qq;
	
	private String username;

	// 入职时间
	@Temporal(TemporalType.DATE)
	@Column(name = "employ_date")
	private Date employDate;
	
	public Person() {
	}
	
	public Person(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getEmployDate() {
		return employDate;
	}

	public void setEmployDate(Date employDate) {
		this.employDate = employDate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(name).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Person))
			return false;
		Person castOther = (Person) other;
		return new EqualsBuilder().append(name, castOther.name).append(qq, castOther.qq).isEquals();
	}

	public String toString() {
		return name + ":" + qq;
	}

}

package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:VisitResult.java
 * 描述:回访结果
 * 创建时间:2012-6-26 下午2:27:41
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "visit_results")
public class VisitResult extends MyAbstractEntity {

	private static final long serialVersionUID = 2079048088572109133L;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String result;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "visit_date")
	private Date visitDate;

	@Column(name = "report_person")
	private String reportPerson;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public String getReportPerson() {
		return reportPerson;
	}

	public void setReportPerson(String reportPerson) {
		this.reportPerson = reportPerson;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(result).append(visitDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof VisitResult)) {
			return false;
		}
		VisitResult castOther = (VisitResult) other;
		return new EqualsBuilder().append(this.result, castOther.getResult()).append(this.visitDate, castOther.getVisitDate()).isEquals();
	}

	public String toString() {
		return result;
	}

}



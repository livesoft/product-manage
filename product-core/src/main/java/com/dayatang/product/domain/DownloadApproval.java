package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.dayatang.domain.QuerySettings;

/**
 * 文件名:DownloadApproval.java
 * 描述:下载试用审批表
 * 创建时间:2012-6-3 下午10:36:51
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "download_approvals")
public class DownloadApproval extends MyAbstractEntity {

	private static final long serialVersionUID = -2541024210566851641L;

	@ManyToOne
	@JoinColumn(name = "download_info_id")
	private DownloadInfo downloadInfo;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private DownloadApprovalCategory category;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "reason_text")
	private String reasonText;
	
	public DownloadInfo getDownloadInfo() {
		return downloadInfo;
	}

	public void setDownloadInfo(DownloadInfo downloadInfo) {
		this.downloadInfo = downloadInfo;
	}

	public DownloadApprovalCategory getCategory() {
		return category;
	}

	public void setCategory(DownloadApprovalCategory category) {
		this.category = category;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String getReasonText() {
		return reasonText;
	}

	public void setReasonText(String reasonText) {
		this.reasonText = reasonText;
	}

	public int hashCode() {
		return 0;
	}

	public boolean equals(Object other) {
		return false;
	}

	public String toString() {
		return null;
	}
	
	/**
	 * 根据审批状态查找
	 * @param category 审批进展
	 * @return
	 */
	public static List<DownloadApproval> findByCategory(DownloadApprovalCategory category) {
		return getRepository().find(QuerySettings.create(DownloadApproval.class).eq("category", category));
	}

}

package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "attachments")
public class Attachment extends MyAbstractEntity implements Comparable<Attachment> {

	private static final long serialVersionUID = 8032100684024018650L;

	//文件名
	@Column(name = "file_name")
	private String fileName;

	//文件大小
	@Column(name = "file_size")
	private long size;
	
	//描述
	private String description;

	//创建时间
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date created;
	
	//保存路径
	@Column(name="file_path")
	private String path;

	private String url;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other)
			return true;
		if (!(other instanceof Attachment))
			return false;
		Attachment castOther = (Attachment) other;
		return new EqualsBuilder().append(fileName, castOther.fileName).append(created, castOther.created).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(fileName).append(created).toHashCode();
	}

	@Override
	public String toString() {
		return fileName;
	}

	public int compareTo(final Attachment other) {
		return new CompareToBuilder().append(created, other.created).toComparison();
	}

}

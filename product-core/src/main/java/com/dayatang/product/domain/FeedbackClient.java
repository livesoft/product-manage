package com.dayatang.product.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "feedback_clients")
public class FeedbackClient extends MyAbstractEntity {

	private static final long serialVersionUID = 695993714691071917L;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	// 客户端类型,枚举
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private MobileClientCategory category;
	
	//反馈内容
	@Column(name = "feedback_content")
	private String feedbackContent;
	
	@Column(name = "feedback_person")
	private String feedbackPerson;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "feedback_date")
	private Date feedbackDate;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public MobileClientCategory getCategory() {
		return category;
	}

	public void setCategory(MobileClientCategory category) {
		this.category = category;
	}

	public String getFeedbackContent() {
		return feedbackContent;
	}

	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}

	public String getFeedbackPerson() {
		return feedbackPerson;
	}

	public void setFeedbackPerson(String feedbackPerson) {
		this.feedbackPerson = feedbackPerson;
	}

	public Date getFeedbackDate() {
		return feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	
	public static List<FeedbackClient> getFeedbackClientsByProductAndCategory(Product product, MobileClientCategory category) {
		if (product != null && category != null) {
			return getRepository().find(QuerySettings.create(FeedbackClient.class).eq("product", product).eq("category", category));
		} else if (product != null) {
			return getRepository().find(QuerySettings.create(FeedbackClient.class).eq("product", product));
		} else if (category != null) {
			return getRepository().find(QuerySettings.create(FeedbackClient.class).eq("category", category));
		} else {
			return FeedbackClient.findAll(FeedbackClient.class);
		}
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(feedbackContent).append(feedbackDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof FeedbackClient))
			return false;
		FeedbackClient castOther = (FeedbackClient) other;
		return new EqualsBuilder().append(feedbackContent, castOther.feedbackContent).append(feedbackDate, castOther.feedbackDate).isEquals();
	}

	public String toString() {
		return feedbackContent + ":" + feedbackDate;
	}

}

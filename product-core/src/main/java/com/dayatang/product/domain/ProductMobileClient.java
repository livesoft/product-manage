package com.dayatang.product.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "product_mobile_clients")
public class ProductMobileClient extends MyAbstractEntity {

	private static final long serialVersionUID = -7155001486247149229L;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	// 客户端描述
	@Column(name = "client_describe")
	private String clientDescribe;

	// 客户端类型,枚举
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private MobileClientCategory category;
	
	// 版本号
	@Column(name = "client_version")
	private long clientVersion;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinTable(name = "mobile_client_attachments", 
		joinColumns = @JoinColumn(name = "mobile_client_id"),
		inverseJoinColumns = @JoinColumn(name = "attachment_id"))
	private Set<Attachment> attachments = new HashSet<Attachment>();
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getClientDescribe() {
		return clientDescribe;
	}

	public void setClientDescribe(String clientDescribe) {
		this.clientDescribe = clientDescribe;
	}

	public MobileClientCategory getCategory() {
		return category;
	}

	public void setCategory(MobileClientCategory category) {
		this.category = category;
	}

	public long getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(long clientVersion) {
		this.clientVersion = clientVersion;
	}

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}
	
	public static long getLastProductMobileClientId() {
		return findAll(ProductMobileClient.class).get(0).getId();
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(clientDescribe).append(clientVersion).toHashCode();
	}
		
	public boolean equals(Object other) {
		if (!(other instanceof ProductMobileClient))
			return false;
		ProductMobileClient castOther = (ProductMobileClient) other;
		return new EqualsBuilder().append(clientDescribe, castOther.clientDescribe).append(clientVersion, castOther.clientVersion).isEquals();
	}

	public String toString() {
		return clientDescribe + ":" + clientVersion;
	}

}

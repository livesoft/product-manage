package com.dayatang.product.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;


@Entity
@Table(name = "mail_receivers")
public class MailReceiver extends MyAbstractEntity {

	private static final long serialVersionUID = 344374275126499404L;

	@ManyToOne
	@JoinColumn(name = "send_mail_id")
	private SendMail sendMail;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "is_success")
	private boolean isSuccess = false;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String error;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "send_date")
	private Date sendDate;

	public SendMail getSendMail() {
		return sendMail;
	}

	public void setSendMail(SendMail sendMail) {
		this.sendMail = sendMail;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean getIsSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public static List<MailReceiver> getMailReceivers(SendMail sendMail) {
		return getRepository().find(QuerySettings.create(MailReceiver.class).eq("sendMail", sendMail));
	}

	public boolean equals(final Object other) {
		return false;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(sendMail).append(sendDate).toHashCode();
	}

	public String toString() {
		return sendMail.toString();
	}

}

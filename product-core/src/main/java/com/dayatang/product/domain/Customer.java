package com.dayatang.product.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "customers")
public abstract class Customer extends MyAbstractEntity {

	private static final long serialVersionUID = 3658767241061107517L;

	@Column(name = "company_name")
	private String companyName;
	
	private String address;
	
	private String phone;
	
	private String email;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private InfoCategory category;
	
	@Column(name = "customer_describe")
	private String customerDescribe;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public InfoCategory getCategory() {
		return category;
	}

	public void setCategory(InfoCategory category) {
		this.category = category;
	}

	public String getCustomerDescribe() {
		return customerDescribe;
	}

	public void setCustomerDescribe(String customerDescribe) {
		this.customerDescribe = customerDescribe;
	}
	
	public String toString() {
		return companyName + ":" + email;
	}

}

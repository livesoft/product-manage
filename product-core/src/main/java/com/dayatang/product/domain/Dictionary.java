package com.dayatang.product.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
@Table(name = "dictionaries", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"category", "serial_number"}),
		@UniqueConstraint(columnNames = {"category", "text"})})
public class Dictionary extends MyAbstractEntity {

	private static final long serialVersionUID = 6102498801737105639L;

	@NotNull
	@Column(name = "serial_number", nullable = false, unique = true)
	private String serialNumber;
	
	@NotNull
	private String text;
	
	@Column(name = "sort_order")
	private int sortOrder;

	@NotNull
	@Enumerated(EnumType.STRING)
	private DictionaryCategory category;
		
	@Column(length = 256)
	private String remark;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public DictionaryCategory getCategory() {
		return category;
	}

	public void setCategory(DictionaryCategory category) {
		this.category = category;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Dictionary() {
	}
	
	public static List<Dictionary> findByCategory(DictionaryCategory category) {
		return getRepository().find(QuerySettings.create(Dictionary.class).eq("category", category).asc("sortOrder"));
	}

	public static Map<String, String> getMap(DictionaryCategory category) {
		Map<String, String> results = new HashMap<String, String>();
		for (Dictionary dictionary : findByCategory(category)) {
			results.put(dictionary.getSerialNumber(), dictionary.getText());
		}
		return results;
	}
	
	public static Map<String, String> getReverseMap(DictionaryCategory category) {
		Map<String, String> results = new HashMap<String, String>();
		for (Dictionary dictionary : findByCategory(category)) {
			results.put(dictionary.getText(), dictionary.getSerialNumber());
		}
		return results;
	}
	
	public static boolean isTextExist(String text) {
		for(Dictionary dictionary : Dictionary.findAll(Dictionary.class)) {
			if(text.equals(dictionary.getText())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isSerialNumberExist(String serialNum) {
		for(Dictionary dictionary : Dictionary.findAll(Dictionary.class)) {
			if(serialNum.equals(dictionary.getSerialNumber())) {
				return true;
			}
		}
		return false;
	}
	public static boolean isSortOrderExist(int sortOrder,DictionaryCategory category) {
		for(Dictionary dictionary : Dictionary.findByCategory(category)) {
			if(sortOrder == dictionary.getSortOrder()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 根据序列号获取字典文本
	 * @param serialNumber
	 * @return
	 */
	public static String getDictionaryTextBySerialNumBer(String serialNumber){
		Dictionary dictionary = getRepository().getSingleResult(QuerySettings.create(Dictionary.class).eq("serialNumber", serialNumber));
		return dictionary.getText();
	}
	
	@Override
	public String toString() {
		return serialNumber + ":" + text;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Dictionary))
			return false;
		Dictionary castOther = (Dictionary) other;
		return new EqualsBuilder().append(serialNumber, castOther.serialNumber)
				.append(category, castOther.category).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(serialNumber)
				.append(category).toHashCode();
	}
	
}

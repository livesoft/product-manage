package com.dayatang.product.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@Entity
@Table(name = "counts")
public class Count extends MyAbstractEntity {

	private static final long serialVersionUID = -1835488124963994697L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "usr_date")
	private Date useDate;
	
	private String ip;
	
	private String address;

	public Date getUseDate() {
		return useDate;
	}

	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(useDate).append(ip).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Count))
			return false;
		Count castOther = (Count) other;
		return new EqualsBuilder().append(useDate, castOther.useDate).append(ip, castOther.ip).isEquals();
	}

	public String toString() {
		return ip + ":" + address;
	}

}

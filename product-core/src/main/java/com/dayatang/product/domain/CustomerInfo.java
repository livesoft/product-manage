package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.Page;

@Entity
public class CustomerInfo extends Customer {

	private static final long serialVersionUID = 4922810486810240825L;
	
	@Column(name = "profession_type")
	private String professionType;
	
	@Column(name = "company_url")
	private String companyUrl;

	public String getProfessionType() {
		return professionType;
	}

	public void setProfessionType(String professionType) {
		this.professionType = professionType;
	}
	
	public String getCompanyUrl() {
		return companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	/**
	 * 根据行业类型查找客户
	 * @param professionType
	 * @return
	 */
	public static List<CustomerInfo> findByProfessionType(String professionType, Page page) {
		if (page == null) {
			return getRepository().find(QuerySettings.create(CustomerInfo.class).eq("professionType", professionType));
		}
		page.setDataTotal(findByProfessionType(professionType).size());
		page.setUrl("customer-manage.action?professionType=" + professionType + "&");
		return getRepository().find(QuerySettings.create(CustomerInfo.class).eq("professionType", professionType).setFirstResult(page.getFirstResult()).setMaxResults(page.getPageSize()));
	}
	
	public static List<CustomerInfo> findByProfessionType(String professionType) {
		return findByProfessionType(professionType,null);
	}
	
	public static List<CustomerInfo> findByDictionary(Dictionary dictionary) {
		return getRepository().find(QuerySettings.create(CustomerInfo.class).eq("professionType", dictionary.getSerialNumber()));
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(professionType).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof CustomerInfo))
			return false;
		CustomerInfo castOther = (CustomerInfo) other;
		return new EqualsBuilder().append(professionType, castOther.professionType).isEquals();
	}

}

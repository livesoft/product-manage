package com.dayatang.product.domain;

public enum FeedbackCategory {
	GOOD,  // 好
	SOSO,  // 一般
	BAD    // 糟糕
}

package com.dayatang.product.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.dayatang.domain.QuerySettings;

@Entity
public class RegisterInfo extends Customer {

	private static final long serialVersionUID = 6963248352312190554L;

	private String contact;
	
	private String mobile;
	
	private long importance = 1;
	
	// 注册码是否成功发送到客户邮箱
	@Column(name = "is_send")
	private boolean isSend;
	
	// 发送失败原因
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String error;
	
	@OneToOne
	@JoinColumn(name = "product_trial")
	private ProductTrial productTrial;
	
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}
	
	public long getImportance() {
		return importance;
	}

	public void setImportance(long importance) {
		this.importance = importance;
	}

	public boolean getIsSend() {
		return isSend;
	}

	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public static RegisterInfo getRegisterByTrial(ProductTrial productTrial) {
		return getRepository().getSingleResult(QuerySettings.create(RegisterInfo.class).eq("productTrial", productTrial));
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(contact).append(mobile).toHashCode();
	}

	public boolean equals(Object other) {
		if (!(other instanceof RegisterInfo))
			return false;
		RegisterInfo castOther = (RegisterInfo) other;
		return new EqualsBuilder().append(contact, castOther.contact).append(mobile, castOther.mobile).isEquals();
	}


}

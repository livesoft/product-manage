package com.dayatang.product.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:CustomerVisit.java
 * 描述:客户回访
 * 创建时间:2012-6-26 下午3:41:06
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "customer_visits")
public class CustomerVisit extends MyAbstractEntity {

	private static final long serialVersionUID = -6886169737067918607L;

	private String title;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "customerVisit", fetch=FetchType.EAGER)
	private Set<Visit> visits = new HashSet<Visit>();
	
	@Temporal(TemporalType.DATE)
	@Column(name = "create_date")
	private Date createDate;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinTable(name = "customer_visit_attachments", 
		joinColumns = @JoinColumn(name = "customer_visit_id"),
		inverseJoinColumns = @JoinColumn(name = "attachment_id"))
	private Set<Attachment> attachments = new HashSet<Attachment>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Visit> getVisits() {
		return visits;
	}

	public void setVisits(Set<Visit> visits) {
		this.visits = visits;
	}
	
	public void addVisit(Visit visit) {
		visit.setCustomerVisit(this);
		visits.add(visit);
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}
	
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(title).append(createDate).toHashCode();
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerVisit)) {
			return false;
		}
		CustomerVisit castOther = (CustomerVisit) other;
		return new EqualsBuilder().append(this.title, castOther.getTitle()).append(this.createDate, castOther.getCreateDate()).isEquals();
	}

	public String toString() {
		return title;
	}
	
	// 获取客户回访中不同的主题
	public Set<VisitSubject> getDifferentSubject() {
		Set<VisitSubject> visitSubjects = new HashSet<VisitSubject>();
		for (Visit visit : getVisits()) {
			visitSubjects.add(visit.getVisitSubject());
		}
		return visitSubjects;
	}

}



package com.dayatang.product.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 文件名:Visit.java
 * 描述:回访记录
 * 创建时间:2012-6-26 下午3:44:14
 * 创建者:mluo
 * 版本号:v2.0
 */
@Entity
@Table(name = "visits")
@NamedQueries({
	@NamedQuery(name = "findByProductTrialAlreadyVisit", query = "select v from Visit v where v.visitResult is not null and v.productTrial = ? "),
	@NamedQuery(name = "findByProductTrialNotVisit", query = "select v from Visit v where v.productTrial = ? and v.visitResult is null"),
	@NamedQuery(name = "findByProductTrialAndVisitSubject", query = "select v from Visit v where v.productTrial.id = ? and v.visitSubject.id = ?")
})
public class Visit extends MyAbstractEntity {

	private static final long serialVersionUID = 7585862939740133636L;

	@ManyToOne
	@JoinColumn(name = "visit_subject_id")
	private VisitSubject visitSubject;
	
	@ManyToOne
	@JoinColumn(name = "visit_result_id")
	private VisitResult visitResult;
	
	@ManyToOne
	@JoinColumn(name = "product_trial_id")
	private ProductTrial productTrial;
	
	@ManyToOne
	@JoinColumn(name = "customer_visit_id")
	private CustomerVisit customerVisit;
	
	public VisitSubject getVisitSubject() {
		return visitSubject;
	}

	public void setVisitSubject(VisitSubject visitSubject) {
		this.visitSubject = visitSubject;
	}

	public VisitResult getVisitResult() {
		return visitResult;
	}

	public void setVisitResult(VisitResult visitResult) {
		this.visitResult = visitResult;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}

	public CustomerVisit getCustomerVisit() {
		return customerVisit;
	}

	public void setCustomerVisit(CustomerVisit customerVisit) {
		this.customerVisit = customerVisit;
	}

	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(productTrial).append(visitSubject).toHashCode();
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Visit)) {
			return false;
		}
		Visit castOther = (Visit) other;
		return new EqualsBuilder().append(this.productTrial, castOther.getProductTrial()).append(this.visitSubject, castOther.getVisitSubject()).isEquals();
	}

	public String toString() {
		return productTrial.toString() + ":" + visitSubject.toString();
	}
	
	/**
	 * 查找已回访客户的记录
	 * @param productTrial 试用客户
	 * @return
	 */
	public static List<Visit> findByProductTrialAlreadyVisit(ProductTrial productTrial) {
		return getRepository().findByNamedQuery("findByProductTrialAlreadyVisit", new Object[]{productTrial}, Visit.class);
	}
	
	/**
	 * 查找未回访客户的记录
	 * @param productTrial 试用客户
	 * @return
	 */
	public static List<Visit> findByProductTrialNotVisit(ProductTrial productTrial) {
		return getRepository().findByNamedQuery("findByProductTrialNotVisit", new Object[]{productTrial}, Visit.class);
	}
	
	
	/**
	 * 查找客户对应的回访
	 * @param subjectId 主题
	 * @param trialId 使用客户
	 * @return
	 */
	public static Visit findByProductTrialAndVisitSubject(long subjectId, long trialId) {
		return getRepository().findByNamedQuery("findByProductTrialAndVisitSubject", new Object[]{trialId,subjectId}, Visit.class).get(0);
	}
	
	/**
	 * 查找上次回访的记录
	 * @param visit
	 * @return
	 */
	public static Visit findLastVisit(Visit visit) {
		List<Visit> visits = findByProductTrialAlreadyVisit(visit.getProductTrial());
		return visits.size() > 0 ? visits.get(visits.size() - 1) : null;
	}

}



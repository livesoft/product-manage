package com.dayatang.product;

/**
 * 文件名:Page.java 
 * 描述:分页对象 创建时间:2012-6-21 上午10:28:44 
 * 创建者:mluo 
 * 版本号:v2.0
 */
public class Page {

	private int total = 1;             // 总页数
	private int cpage = 1;         // 当前页
	private int pageSize = 20;     // 显示数量
	private String url = "index.action";            // 请求地址

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCpage() {
		return cpage;
	}

	public void setCpage(int cpage) {
		this.cpage = cpage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setDataTotal(int dataSize) {
		if (dataSize > 0) {
			total = dataSize/getPageSize() + 1;
		} else {
			total = 0;
			cpage = 0;
		}
	}
	
	public int getFirstResult() {
		return (getCpage()-1)*getPageSize();
	}

}

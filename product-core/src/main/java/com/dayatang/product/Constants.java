package com.dayatang.product;

/**
 * 项目常量
 */
public interface Constants {
	
	public static final String CHARSET = "charset";
	
	public static final String UPLOAD_DIR = "upload.dir";
	
}

package com.dayatang.product.application;


import java.util.List;
import java.util.Set;

import com.dayatang.domain.AbstractEntity;
import com.dayatang.product.domain.Attachment;
import com.dayatang.product.domain.Customer;
import com.dayatang.product.domain.CustomerVisit;
import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.domain.DownloadInfo;
import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.MyAbstractEntity;
import com.dayatang.product.domain.Organization;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductMobileClient;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.domain.RegisterCode;
import com.dayatang.product.domain.SendMail;
import com.dayatang.product.domain.TrialCode;
import com.dayatang.product.domain.VisitResult;

public interface ProductApplication {
	
	<T extends MyAbstractEntity> T saveEntity(T entity);
	
	void removeEntity(AbstractEntity entity);

	/**
	 * 新增试用号
	 * @param productTrial 试用实体类
	 * @param trialPeriod 试用期天数
	 */
	void createProductTrial(ProductTrial productTrial, long trialPeriod);

	/**
	 * 删除试用号 
	 * @param productTrial 试用实体类
	 */
	void removeProductTiral(ProductTrial productTrial);
	
	/**
	 * 获取注册码
	 * @param productTrial 试用实体类
	 * @param register 注册信息
	 */
	void obtainRegisterCode(TrialCode trialCode, RegisterInfo register);
	
	/**
	 * 激活注册码
	 * @param registerCode 注册码
	 */
	void activeRegisterCode(RegisterCode registerCode);
	
	/**
	 * 更新有效期
	 */
	void updatePeriodDay();
	
	/**
	 * 更新试用期
	 */
	void updateTrialDay();
	
	/**
	 * 保存发送邮件信息
	 * @param htmlMail 邮件
	 * @param customers 收件人
	 */
	void saveSendMailInfo(HtmlMail htmlMail, List<Customer> customers);
	
	/**
	 * 删除HTML邮件
	 * @param htmlMail HTML邮件
	 */
	void removeHtmlMail(HtmlMail htmlMail);
	
	/**
	 * 删除发送邮件信息
	 * @param sendMail 发送邮件信息
	 */
	void removeSendMail(SendMail sendMail);
	
	/**
	 * 导入客户信息
	 * @param objects 读取的客户信息
	 */
	void reportCustomerInfo(List<Object[]> objects);
	
	/**
	 * 修改试用期天数
	 * @param productTrial 试用实体
	 * @param trialDay 试用期天数
	 */
	void modifyTrialDay(ProductTrial productTrial, long trialDay);
	
	/**
	 * 批量生成试用号
	 * @param product 产品
	 * @param customers 目标客户
	 * @param validity 有效期
	 * @param trialPeriod 试用期天数
	 */
	void batchCreateProductTrial(Product product, long[] customers, long validity, long trialPeriod);
	
	/**
	 * 随机生成试用号
	 * @param mark 产品标识
	 * @return 试用号
	 */
	String createTrailCode(String mark);
	
	/**
	 * MD5加密
	 * @param src 字符串字节
	 * @return 加密后的字符串
	 */
	String getMD5(byte[] src);
	
	/**
	 * 更新移动客户端
	 * @param productMobileClient 移动客户实体类
	 * @param attachments 附件列表
	 */
	void updateMobileClient(ProductMobileClient productMobileClient, Set<Attachment> attachments);

	/**
	 * 组织机构添加子机构
	 * @param org 子机构
	 * @param parent 父机构
	 */
	void createLowerOrganization(Organization org, Organization parent);
	
	/**
	 * 申请试用
	 * @param downloadInfo 申请信息
	 * @param product 试用产品
	 */
	void applyTrial(DownloadInfo downloadInfo, Product product);
	
	/**
	 * 审批通过,自动走产品试用流程
	 * @param approval 审批
	 * @param trialPeriod 试用期天数
	 * @param validity 有效期天数
	 * @return 注册客户
	 */
	RegisterInfo approvalPass(DownloadApproval approval, long trialPeriod, long validity);
	
	/**
	 * 生成客户回访
	 * @param title 标题
	 * @param trialIds 回访客户列表
	 * @param subjectIds 主题列表
	 * @return 客户回访对象
	 */
	CustomerVisit createCustomerVisit(String title, long[] trialIds, long[] subjectIds);
	
	/**
	 * 记录回访结果
	 * @param visitResult 回访结果
	 * @param subjectId 回访主题
	 * @param trialId 回访客户
	 */
	void recordVisitResult(VisitResult visitResult, long subjectId, long trialId);
	
	/**
	 * 保存回访帮助文档
	 * @param customerVisit
	 * @param attachment
	 */
	void saveVisitHelpAttachment(CustomerVisit customerVisit, Attachment attachment);

	/**
	 * 将试用激活的试用号变成可注册状态
	 */
	void checkActivateTrail();
}

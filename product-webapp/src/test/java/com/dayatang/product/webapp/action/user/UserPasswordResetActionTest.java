package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.stub;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserPasswordResetActionTest.java
 * 描述:用户密码重置
 * 创建时间:2012-5-29 下午5:42:06
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserPasswordResetActionTest {
	
	private UserPasswordResetAction action = new UserPasswordResetAction();
	
	private PersonUser personUser;
	private String result;
	
	@Mock
	private EntityRepository repository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		personUser = new PersonUser();
	}
	
	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		personUser.setId(2L);
		stub(PersonUser.get(PersonUser.class, 2L)).toReturn(personUser);
		action.setPersonUser(personUser);
		result = action.execute();
		assertEquals(personUser, action.getPersonUser());
		assertEquals(ActionSupport.SUCCESS, result);
	}

	
}



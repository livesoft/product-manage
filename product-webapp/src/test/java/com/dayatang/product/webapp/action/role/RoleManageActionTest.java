package com.dayatang.product.webapp.action.role;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.Role;
import com.opensymphony.xwork2.ActionSupport;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * 文件名:RoleManageActionTest.java
 * 描述:角色管理单元测试类
 * 创建时间:2012-5-24 下午5:38:22
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleManageActionTest {
	
	private RoleManageAction action = new RoleManageAction();
	
	private List<Role> roles;
	
	@Mock
	private EntityRepository repository;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Role.setRepository(repository);
		roles = new ArrayList<Role>();
		roles.add(new Role("超级管理员"));
	}

	@After
	public void  tearDown() throws Exception {
		Role.setRepository(null);
	}
	
	@Test
	public void execute() throws Exception {
		stub(Role.findAll(Role.class)).toReturn(roles);
		String result = action.execute();
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(roles, action.getRoles());
		action.setRoleName("test");
		stub(Role.findByQuerySettings(QuerySettings.create(Role.class).containsText("name", "test"))).toReturn(roles);
		result = action.execute();
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(roles, action.getRoles());
	}
	
}



package com.dayatang.product.webapp.action.organization;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.Organization;
import com.opensymphony.xwork2.ActionSupport;
import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

/**
 * 文件名:OrganizationEditActionTest.java
 * 描述:组织机构编辑单元测试类
 * 创建时间:2012-5-23 下午4:37:40
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationEditActionTest {
	
	private OrganizationEditAction action = new OrganizationEditAction();
	
	private Organization organization;
	
	@Mock
	private EntityRepository repository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Organization.setRepository(repository);
		organization = new Organization();
	}
	
	@After
	public void  tearDown() throws Exception {
		Organization.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		String result = action.execute();
		assertNull(action.getOrganization());
		assertEquals(ActionSupport.SUCCESS, result);
		organization.setId(2L);
		stub(Organization.get(Organization.class, 2L)).toReturn(organization);
		action.setOrganization(organization);
		result = action.execute();
		assertEquals(organization, action.getOrganization());
	}
}



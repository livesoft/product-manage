package com.dayatang.product.webapp.action.organization;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.product.domain.Organization;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:OrganizationRemoveActionTest.java
 * 描述:删除组织机构单元测试类
 * 创建时间:2012-5-24 下午2:19:19
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationRemoveActionTest {
	
	private OrganizationRemoveAction action = new OrganizationRemoveAction();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private ProductApplication application;
	
	private Organization organization;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Organization.setRepository(repository);
		action.setProductApplication(application);
		organization = new Organization();
	}

	@After
	public void  tearDown() throws Exception {
		Organization.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		organization.setId(1L);
		action.setOrganization(organization);
		stub(Organization.get(Organization.class, 1L)).toReturn(organization);
		String result = action.execute();
		verify(application).removeEntity(organization);
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(0, action.getResult());
	}
}



package com.dayatang.product.webapp.action.organization;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.Organization;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:OrganizationManageActionTest.java
 * 描述:组织机构加载单元测试类
 * 创建时间:2012-5-22 上午9:30:16
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationManageActionTest {
	
	private OrganizationManageAction action = new OrganizationManageAction();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private Organization root;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Organization.setRepository(repository);
	}
	
	@After
	public void  tearDown() throws Exception {
		Organization.setRepository(null);
	}
	
	@Test
	public void execute() throws Exception {
		stub(Organization.getRootOrg()).toReturn(root);
		String result = action.execute();
		assertEquals(ActionSupport.SUCCESS,result);
	}

	
	
}



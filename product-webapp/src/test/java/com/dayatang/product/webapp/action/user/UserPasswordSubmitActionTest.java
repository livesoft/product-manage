package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.SecurityApplication;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserPasswordSubmitActionTest.java
 * 描述:保存重置密码单元测试类
 * 创建时间:2012-5-29 下午5:47:41
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserPasswordSubmitActionTest {

	private UserPasswordSubmitAction action = new UserPasswordSubmitAction();
	
	@Mock
	private EntityRepository repository;

	@Mock
	private SecurityApplication application;

	private PersonUser personUser;
	private String newPassword = "654321";
	private String result;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		InitData();
	}

	public void InitData() {
		action.setSecurityApplication(application);
		personUser = new PersonUser();
		personUser.setId(1L);
		action.setPersonUser(personUser);
		action.setNewPassword(newPassword);
	}
	
	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}
	
	@Test
	public void execute() throws Exception {
		stub(PersonUser.get(PersonUser.class, 1L)).toReturn(personUser);
		result = action.execute();
		verify(application).resetPassword(personUser, newPassword);
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(1, action.getResult());
	}
	
}



package com.dayatang.product.webapp.action.organization;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.product.domain.Organization;
import com.opensymphony.xwork2.ActionSupport;

import static org.junit.Assert.*;
/**
 * 文件名:OrganizationSubmitActionTest.java
 * 描述:新增组织机构单元测试类
 * 创建时间:2012-5-24 上午9:57:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationSubmitActionTest {

	private OrganizationSubmitAction action = new OrganizationSubmitAction();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private ProductApplication application;
	
	private Organization organization;
	private Organization parent;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Organization.setRepository(repository);
		InitOrganization();
	}
	
	private void InitOrganization() {
		organization = new Organization();
		organization.setName("childRen");
		parent = new Organization();
		action.setParent(parent);
		action.setOrganization(organization);
		action.setProductApplication(application);
	}
	
	@After
	public void  tearDown() throws Exception {
		Organization.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		parent.setId(0L);
		String result = action.execute();
		verify(application).saveEntity(organization);
		assertEquals(ActionSupport.SUCCESS, result);
		parent.setId(1L);
		action.setParent(parent);
		stub(Organization.get(Organization.class, 1L)).toReturn(parent);
		result = action.execute();
		verify(application).createLowerOrganization(organization, parent);
	}
	
}



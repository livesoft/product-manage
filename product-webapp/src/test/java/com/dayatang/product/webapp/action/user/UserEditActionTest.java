package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.stub;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserEditActionTest.java
 * 描述:用户编辑单元测试类
 * 创建时间:2012-5-28 下午4:52:12
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserEditActionTest {
	
	private UserEditAction action = new UserEditAction();
	
	private PersonUser personUser;
	
	@Mock
	private EntityRepository repository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		personUser = new PersonUser();
	}
	
	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		String result = action.execute();
		assertNull(action.getPersonUser());
		assertEquals(ActionSupport.SUCCESS, result);
		personUser.setId(2L);
		stub(PersonUser.get(PersonUser.class, 2L)).toReturn(personUser);
		action.setPersonUser(personUser);
		result = action.execute();
		assertEquals(personUser, action.getPersonUser());

	}
}



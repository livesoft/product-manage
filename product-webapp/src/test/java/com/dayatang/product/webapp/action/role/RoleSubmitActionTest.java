package com.dayatang.product.webapp.action.role;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.product.domain.Role;
import com.opensymphony.xwork2.ActionSupport;
import static org.mockito.Mockito.*;

/**
 * 文件名:RoleSubmitActionTest.java
 * 描述:添加角色单元测试类
 * 创建时间:2012-5-24 下午11:19:26
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleSubmitActionTest {
	
	private RoleSubmitAction action = new RoleSubmitAction();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private ProductApplication application;

	private Role role;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Role.setRepository(repository);
		InitRole();
	}

	private void InitRole() {
		role = new Role();
		role.setName("test");
		action.setProductApplication(application);
	}
	
	@After
	public void  tearDown() throws Exception {
		Role.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		String result = action.execute();
		assertEquals(ActionSupport.SUCCESS, result);
		action.setRole(role);
		result = action.execute();
		verify(application).saveEntity(role);
		assertEquals(ActionSupport.SUCCESS, result);
	}
}

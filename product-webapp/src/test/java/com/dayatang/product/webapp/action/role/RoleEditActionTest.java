package com.dayatang.product.webapp.action.role;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.stub;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.Role;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:RoleEditActionTest.java
 * 描述:角色编辑单元测试类
 * 创建时间:2012-5-24 下午10:24:01
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleEditActionTest {
	
	private RoleEditAction action = new RoleEditAction();
	
	private Role role;
	
	@Mock
	private EntityRepository repository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Role.setRepository(repository);
		role = new Role();
	}
	
	@After
	public void  tearDown() throws Exception {
		Role.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		String result = action.execute();
		assertNull(action.getRole());
		assertEquals(ActionSupport.SUCCESS, result);
		role.setId(2L);
		stub(Role.get(Role.class, 2L)).toReturn(role);
		action.setRole(role);
		result = action.execute();
		assertEquals(role, action.getRole());
	}

}

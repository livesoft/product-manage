package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserRemoveActionTest.java
 * 描述:删除用户单元测试类
 * 创建时间:2012-5-29 下午3:03:21
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserRemoveActionTest {
	
	private UserRemoveAction action = new UserRemoveAction();
	
	@Mock
	private EntityRepository repository;
	
	@Mock
	private ProductApplication application;
	
	private PersonUser personUser;
	private String result;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		action.setProductApplication(application);
		personUser = new PersonUser();
		personUser.setId(1L);
	}

	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		action.setPersonUser(personUser);
		stub(PersonUser.get(PersonUser.class, 1L)).toReturn(personUser);
		result = action.execute();
		verify(application).removeEntity(personUser);
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(0, action.getResult());
	}
	
}



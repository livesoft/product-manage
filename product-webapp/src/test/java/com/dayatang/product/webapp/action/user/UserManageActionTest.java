package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.stub;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserManageActionTest.java
 * 描述:用户管理单元测试类
 * 创建时间:2012-5-28 上午11:10:26
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserManageActionTest {
	
	private UserManageAction action = new UserManageAction();
	
	private List<PersonUser> personUsers;
	
	@Mock
	private EntityRepository repository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		personUsers = new ArrayList<PersonUser>();
		personUsers.add(new PersonUser());
	}

	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}

	@Test
	public void execute() throws Exception {
		stub(PersonUser.findAll(PersonUser.class)).toReturn(personUsers);
		String result = action.execute();
		assertEquals(ActionSupport.SUCCESS, result);
		assertEquals(personUsers, action.getPersonUsers());
	}
}



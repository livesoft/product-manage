package com.dayatang.product.webapp.action.role;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.Role;
import com.opensymphony.xwork2.ActionSupport;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

/**
 * 文件名:RoleNameExistActionTest.java
 * 描述:检查角色名称是否存在单元测试类
 * 创建时间:2012-5-28 上午9:42:26
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleNameExistActionTest {
	
	private RoleNameExistAction action = new RoleNameExistAction();
	
	@Mock
	private EntityRepository repository;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Role.setRepository(repository);
	}

	@After
	public void  tearDown() throws Exception {
		Role.setRepository(null);
	}
	
	@Test
	public void execute() throws Exception {
		String result = action.execute();
		assertEquals(0, action.getResult());
		assertEquals(ActionSupport.SUCCESS, result);
		action.setRoleName("test");
		stub(Role.findByName(action.getRoleName())).toReturn(new Role("test"));
		action.execute();
		assertEquals(1, action.getResult());
	}
	
}



package com.dayatang.product.webapp.action;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.Action;

/**
 * 文件名:LoginActionTest.java
 * 描述:登录单元测类
 * 创建时间:2012-6-11 下午9:54:45
 * 创建者:mluo
 * 版本号:v2.0
 */
public class LoginActionTest {
	
	private LoginAction action = new LoginAction();
	
	@Mock
	private EntityRepository repository;
	
	private String username = "test";
	private String password = "123456";
	private String code = "1234";
	private String result;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
//		action.setUsername(username);
//		action.setPassword(password);
//		action.setCode(code);
	}
	
	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}
	
	@Test
	public void execute() throws Exception {
		result = action.execute();
		assertEquals(Action.SUCCESS, result);
	}
}

package com.dayatang.product.webapp.action.product;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.domain.Product;
import com.opensymphony.xwork2.ActionSupport;

import static org.mockito.Mockito.stub;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class ProductManageActionTest {
	
	private ProductManageAction action = new ProductManageAction();
	private EntityRepository repository = mock(EntityRepository.class);
	
	@Before
	public void setUp() throws Exception {
		Product.setRepository(repository);
	}
	
	@After
	public void  tearDown() throws Exception {
		Product.setRepository(null);
	}
	
	@Test
	public void testExecute() throws Exception {
		assertEquals(ActionSupport.SUCCESS, action.execute());
	}
	
	@Test
	public void testFindProducts() {
		List<Product> products = new ArrayList<Product>();
		stub(Product.findAll(Product.class)).toReturn(products);
		assertEquals(products, action.getProducts());
	}
	
}

package com.dayatang.product.webapp.action.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dayatang.domain.EntityRepository;
import com.dayatang.product.application.SecurityApplication;
import com.dayatang.product.domain.Person;
import com.dayatang.product.domain.PersonUser;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:UserSubmitActionTest.java
 * 描述:新增用户单元测试
 * 创建时间:2012-5-28 下午11:16:36
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserSubmitActionTest {

	private UserSubmitAction action = new UserSubmitAction();
	
	@Mock
	private EntityRepository repository;

	@Mock
	private SecurityApplication application;
	
	private PersonUser personUser;
	private Person person;
	private String result;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PersonUser.setRepository(repository);
		InitPersonUser();
	}

	private void InitPersonUser() {
		personUser = new PersonUser();
		personUser.setUsername("test");
		personUser.setPerson(InitPerson());
		action.setSecurityApplication(application);
		action.setPersonUser(personUser);
	}

	@After
	public void  tearDown() throws Exception {
		PersonUser.setRepository(null);
	}

	@Ignore
	@Test
	public void execute() throws Exception {
		result = action.execute();
		verify(application).createPersonUser(personUser,null,null);
		assertEquals(ActionSupport.SUCCESS, result);
	}
	
	public Person InitPerson() {
		person = new Person();
		person.setName("test");
		return person;
	}
}

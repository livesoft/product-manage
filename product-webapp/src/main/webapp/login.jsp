<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户登录</title>
<link href="<%=request.getContextPath() %>/style/login.css" rel="stylesheet" type="text/css" />
<script src="<%=request.getContextPath() %>/page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	function changeValidateCode(obj) {
		var currentTime = new Date().getTime();
		obj.src = "<%=request.getContextPath() %>/common/random-verify.action?sessionKey=LOGIN&d=" + currentTime;
	}
	
	function checkCode(code,sessionKey)
	{
		$.ajax({
			 type: "POST",	   	   
			 async: false,
			 data: {"code" : code, "sessionKey" : sessionKey},
			 url: "<%=request.getContextPath() %>/common/check-verify-code.action",
			 success: function(data) {
				 if (data.result == 0) {
					$("#error").text("*验证码错误");
				 }
			 }
		});
	}
	
	function resetInfo() {
		$("#username").val("");
		$("#password").val("");
		$("#code").val("");
	}
	
	function login() {
		var username = $("#username").val();
		var password = $("#password").val();
		var code = $("#code").val();
		if (username == "") {
			$("#error").text("*请输入账号");
		} else if (password == "") {
			$("#error").text("*请输入密码");
		} else if (code == "") {
			$("#error").text("*请输入验证码");
		} else {
			checkCode(code,"LOGIN");
		}
		
		if ($("#error").text() == "") {
			$.ajax({
				 type: "POST",	   	   
				 async: false,
				 data: {"code" : code, "username" : username, "password" : password},
				 url: "<%=request.getContextPath() %>/login.action",
				 success: function(data) {
					 if (data.result == "failure") {
						 $("#error").text("*登录失败");
					 } else if (data.result == "usernameNoExists") {
					 	 $("#error").text("*账号不存在");
					 } else if (data.result == "passwordError") {
						 $("#error").text("*密码错误");
					 } else if (data.result == "userDisable") {
						 $("#error").text("*账号被禁用");
					 } else if (data.result == "noRole") {
						 $("#error").text("*账号未分配角色");
					 } else if (data.result == "noPermission") {
						 $("#error").text("*账号角色未分配权限");
					 } else {
						 document.location.href = "<%=request.getContextPath() %>/index.action";
					 }
				 }
			});
		}
		
	}
	
	function cleanInfo() {
		$("#error").text("");
	}
	
	function keyEnter(e) { 
        if (e.keyCode == 13) { 
        	login();
        }   
	} 
	document.onkeydown = keyEnter; 
	
</script>
<script language="JavaScript"> 
	if (window != top) 
		top.location.href = location.href;
</script>
</head>
<body>

	<div id="login">
		<div id="top">
			<div id="top_left">
				<img src="<%=request.getContextPath() %>/images/login_03.gif" />
			</div>
			<div id="top_center"></div>
		</div>
		<div id="center">
			<div id="center_left"></div>
			<div id="center_middle">
				<div id="user">
					账号:<input type="text" name="username" id="username" size="15" onfocus="cleanInfo();"/>
				</div>
				<div id="passwordDiv">
					密码:<input type="password" name="password" id="password" size="15" onfocus="cleanInfo();"/>
				</div>
				<div id="passwordDiv">
					验证码:<input type="text" id="code" name="code" size="5" onfocus="cleanInfo();"/>&nbsp;<a href="#"><img align="top" src="<%=request.getContextPath() %>/common/random-verify.action?sessionKey=LOGIN" onclick="changeValidateCode(this)" /></a>
				</div>
				<div align="right" id="error" style="color: red;"></div>
				<div id="btn">
					<a href="javascript:login();">登录</a><a href="javascript:resetInfo();">重置</a>
				</div>
			</div>
			<div id="center_right"></div>
		</div>
		<div id="down">
			<div id="down_left">
				<div id="inf">
					<span class="inf_text">版本信息</span> <span class="copyright" >产品信息管理系统 2012 v2.0</span>
				</div>
			</div>
			<div id="down_center"></div>
		</div>
	</div>
	
</body> 
</html>
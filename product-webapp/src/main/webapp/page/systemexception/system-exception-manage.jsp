<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<%@ taglib prefix="pm" uri="/authority-tags"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function removeException(id)
	{
		$.post("system-exception-remove.action",{"exceptionId":id},
				function(data) {
					promptRedirect("删除成功");
			}, "json");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="4" align="center" bgcolor="#EEEEEE"class="tablestyle_title">异常列表列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="14%" align="center" bgcolor="#EEEEEE">操作人</td>
					                    <td width="60%" align="center" bgcolor="#EEEEEE">异常类</td>
					                    <td width="18%" align="center" bgcolor="#EEEEEE">异常时间</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="systemExceptions.size() > 0">
						                  <s:iterator value="systemExceptions">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="actionName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="exceptionDate"/></div></td>
							                  	<td bgcolor="#FFFFFF"><div align="center">
							                  		<pm:authority name="systemSetting:exception:view">
							                  			<s:a href="system-exception-detail.action?systemException.id=%{id}"><img src="../page/images/system/details.gif" title="详情"/></s:a>
							                  		</pm:authority>
							                  		<pm:authority name="systemSetting:exception:remove">
							                  			<s:a href="javascript:removeException(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
							                  		</pm:authority>
							                  		</div>
							                  	</td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="4">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
</head>
<body>
		<s:include value="../right_top.jsp" />
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">异常信息页面</th>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>异常信息</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">操作人:</td>
												<td align="left"><s:property value="systemException.name"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">异常类:</td>
												<td align="left"><s:property value="systemException.actionName"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">异常详情:</td>
												<td align="left"><s:property value="systemException.exceptionText" escape="false"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">异常时间:</td>
												<td align="left"><s:property value="systemException.exceptionDate"/></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>
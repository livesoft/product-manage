<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/authority-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	function removeTrial(id)
	{
		$.post("trial-remove.action",
				{"productTrial.id" : id},
				function(data){
					if (data.result == 1)
					{
						alert("该试用号已被引用");
					}
					else
					{
						document.location.href = 'trial-manage.action';
					}
				},"json"
		);
	}
	
	function editTrialDay(id)
	{
		var url = "trial-day-edit.action?productTrial.id="+id;
		popupNewWin(url,"380px","200px");
	}

	
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										<input name="textfield" type="text" size="12" />
										<input name="submit" type="button" class="query-button" value="查 询" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="97%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">试用号列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="20%" align="center" bgcolor="#EEEEEE">客户名称</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">产品名称</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">试用号</td>
					                    <td width="20%" align="center" bgcolor="#EEEEEE">注册码</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">试用期</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">是否激活</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">剩余天数</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">是否过期</td>
					                    <td width="10" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="productTrials.size() > 0">
						                  <s:iterator value="productTrials">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:a href="../product/product-details.action?productTrial.id=%{id}"><s:property value="customerInfo.companyName"/></s:a></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="trialCode.trialNum"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="registerCode.registerCode"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="trialPeriod"/>天</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:if test="registerCode.isActivate">是</s:if><s:else>否</s:else></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="registerCode.trialDay"/>天</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:if test="trialCode.isEffective">否</s:if><s:else>是</s:else></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:if test="!registerCode.isActivate && trialCode.isEffective">
							                    		<pm:authority name="trialNumMgmt:list:edit">
							                    			<s:a href="javascript:editTrialDay(%{id})" id="editTrialDay%{id}">修改试用期</s:a>
							                    		</pm:authority>
							                    	</s:if>
							                    	<s:if test="!trialCode.isEffective">
							                    		<s:a href="trial-validity-activate.action?trialCode.id=%{trialCode.id}">激活有效期</s:a>
							                    	</s:if>
							                    	<pm:authority name="trialNumMgmt:list:remove">
							                    		<s:a href="javascript:removeTrial(%{id})">删除</s:a>
							                    	</pm:authority>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="9">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">5</span> 页 | 第 <span class="right-text09">1</span> 页</td>
                <td width="49%" align="right">[<a href="#" class="right-font08">首页</a> | <a href="#" class="right-font08">上一页</a> | <a href="#" class="right-font08">下一页</a> | <a href="#" class="right-font08">末页</a>] 转至：</td>
                <td width="1%"><table width="20" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="1%"><input name="textfield3" type="text" class="right-textfield03" size="1" /></td>
                      <td width="87%"><input name="Submit23222" type="submit" class="page-go" value=" " />
                      </td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>

</body>
</html>
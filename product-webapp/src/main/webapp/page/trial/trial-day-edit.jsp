<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	
	$(function(){
		
		$("#trialSub").submit(function(){
			var trialDay = $("#trialDay").val();
			if (trialDay == "")
			{
				$("#alertInfo").text("*请填写试用期");
				return false;
			}
			else
			{
				$.ajax({
					url: "trial-day-submit.action",
					cache: false,
					async: false,
					data: $("#trialSub").serialize(),
					success:function(){
						parent.closeWin("修改成功");
					}
				});
			}
			return true;

		});
		
	});
	
</script>
</head>
<body class="ContentBody">

	<s:form action="trial-day-submit.action" id="trialSub">
		<s:hidden name="productTrial.id" value="%{productTrial.id}" />
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>修改试用期</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right">试用期天数:</td>
												<td colspan="3"><input name="trialDay" id="trialDay" onkeyup="this.value=this.value.replace(/\D/g,'')" class="text" value="<s:property value="productTrial.trialPeriod"/>" size="10" onfocus="cleanInfo()" />天</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
	
</body>
</html>
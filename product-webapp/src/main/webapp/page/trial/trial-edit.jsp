<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		$("#trialButton").click(function(){
			var productId = $("#productId").val();
			var trialCode = $("#trialCode").val();
			if (productId != "" && trialCode == "") {
				$.post("trial-random.action",
						{"product.id" : productId},
						function(data){
							$("#trialCode").val(data.trialCode);
							$("#alertInfo").text("");
						},"json"
				);
			} else if (trialCode != "") {
				$("#alertInfo").text("*产品试用号已生成");
			} else {
				$("#alertInfo").text("*请选择产品");
			}
		});
	});
	
	function cleanInfo() {
		$("#alertInfo").text("");
	}
	
	function formSubmit() 
	{
		var productId = $("#productId").val();
		var trialCode = $("#trialCode").val();
		var customer = $("#testForm_productTrial_customerInfo_id").val();
		var validity = $("#validity").val();
		if (productId == "")
		{
			$("#alertInfo").text("请选择产品");
			return false;
		}
		if (customer == null)
		{
			$("#alertInfo").text("请选择客户");
			return false;
		}
		if (trialCode == "")
		{
			$("#alertInfo").text("请生成试用号");
			return false;
		}
		if (validity == "")
		{
			$("#alertInfo").text("请输入有效期");
			return false;
		}
		return true;
	}
	
</script>
<style type="text/css">
   .nobr br{display:none}   
</style>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="trial-submit.action" theme="simple" id="testForm" onsubmit="return formSubmit();">
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">新增试用号页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<pm:authority name="trialNumMgmt:add:add">
										<input type="submit" value="保存" class="button" />
									</pm:authority>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
									<pm:authority name="trialNumMgmt:add:batch">
										<input type="button" value="批量生产试用号" class="button" onclick="document.location.href='trial-batch.action'" />
									</pm:authority>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加试用号</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">选择产品:</td>
												<td width="41%"><s:select list="productMaps" headerKey="" headerValue="请选择产品" listKey="key" listValue="value" id="productId" name="productTrial.product.id" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">选择客户:</td>
												<td width="41%">
													<div class="nobr"><s:doubleselect theme="simple" list="dictionarys" headerKey="" headerValue="请选择行业" listKey="serialNumber" listValue="text" doubleList="customerMaps.get(top)" doubleListKey="id" doubleListValue="companyName" doubleName="productTrial.customerInfo.id" onfocus="cleanInfo();"/><span class="red">*</span></div>
												</td>
											</tr>
											<tr>
												<td nowrap align="right">有效期天数:</td>
												<td><input name="productTrial.trialCode.validity" id="validity" value="50" class="text" size="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/>天<span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">试用期天数:</td>
												<td><input name="trialPeriod" class="text" size="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/>天<span class="red">(不填默认为产品的试用天数)</span></td>
											</tr>
											<tr>
												<td nowrap align="right">试用号:</td>
												<td><input name="productTrial.trialCode.trialNum" id="trialCode" class="text" size="30" readonly="readonly" onfocus="cleanInfo();"/><span class="red">*<input type="button" value="点击生成试用号" class="button" id="trialButton" /></span></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<pm:authority name="trialNumMgmt:add:add">
							<input type="submit" value="保存" class="button" />
						</pm:authority>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
						<pm:authority name="trialNumMgmt:add:batch">
							<input type="button" value="批量生产试用号" class="button" onclick="document.location.href='trial-batch.action'" />
						</pm:authority>
					</td>
				</tr>
			</table>
		</div>
	</s:form>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function cleanInfo() {
		$("#alertInfo").text("");
	}
	
	function customerSel()
	{
		$("#alertInfo").text("");
		var cust = $("#cust").val();
		
		if (cust == 0) 
		{
			$("#optDiv").css("display","none");
		}
		else
		{
			$("select[name=customers] option").each(function(){
				$(this).remove();
			});
			$.post("trial-customer-info.action",
					{"professionType" : cust},
					function(data){
						$.each(data.customerInfos,function(i, item){
							$("<option></option>").attr("value",item.id).html(item.companyName).appendTo($("select[name=customers]"));
						});
					},"json"
			);
			$("#optDiv").css("display","block");
		}
	}

	$(function(){
		
		$("#trialForm").submit(function(){
			
			var productId = $("#productId").val();
			var cust = $("#cust").val();
			var validity = $("#validity").val();
			
			var len = 0;
			$("select[name=customers] option").each(function(){
				len = len + 1;
			});
			
			if (productId == "")
			{
				$("#alertInfo").text("*请选择产品");
				return false;
			}
			if (cust == 0)
			{
				$("#alertInfo").text("*请选择客户");
				return false;
			}
			if (validity == "")
			{
				$("#alertInfo").text("*请填写有效期");
				return false;
			}
			if (len == 0)
			{
				$("#alertInfo").text("*请选择客户");
				return false;
			}
			$("select[name=customers] option").each(function(){
				$(this).attr("selected",true);
			});
			return true;
			
		});
		
	});
	
	
</script>
<style type="text/css">
   .nobr br{display:none}   
</style>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="trial-batch-submit.action" theme="simple" id="trialForm">
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">批量生成试用号页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>批量生成试用号</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">选择产品:</td>
												<td width="41%"><s:select list="products" headerKey="" headerValue="请选择产品" listKey="id" listValue="name" id="productId" name="product.id" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">选择客户:</td>
												<td width="41%">
													<div class="nobr"><s:select id="cust" list="dictionarys" headerKey="0" headerValue="请选择行业" listKey="serialNumber" listValue="text" onchange="customerSel();"/><span class="red">*</span></div>
												</td>
											</tr>
											<tr>
												<td nowrap align="right"></td>
												<td>
													<div id="optDiv" style="display: none;">
													<s:optiontransferselect
												        name="customers"
												        leftTitle="生成"
												        rightTitle="不生成"
												        list="{'0'}"
												        cssStyle="width:300px;height:300px;"
												        doubleCssStyle="width:300px;height:300px;"
												        doubleName="rightList"
												        doubleList=""
												        addToLeftLabel="向左移动"
												        addToRightLabel="向右移动"
												        addAllToLeftLabel="全部左移"
												        addAllToRightLabel="全部右移"
												        allowUpDownOnLeft="false"
												        allowUpDownOnRight="false"
												        allowSelectAll="false"
												        />
													<span class="red">*</span>
													</div>
												</td>
											</tr>
											<tr>
												<td nowrap align="right">有效期天数:</td>
												<td><input name="validity" id="validity" value="50" class="text" size="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/>天<span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">试用期天数:</td>
												<td><input name="trialPeriod" class="text" size="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/>天<span class="red">(不填默认为产品的试用天数)</span></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
	
</body>
</html>
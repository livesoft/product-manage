<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function checkFor()
	{
		var status = $("#status").val();
		var productId = $("#product").val();
		if (status != "" && productId != "")
		{
			document.location.href = "trial-report.action?product.id=" + productId + "&status=" + status;
		} 
		else if (productId != "")
		{
			document.location.href = "trial-report.action?product.id=" + productId;
		}
		else if (status != "")
		{
			document.location.href = "trial-report.action?status=" + status;
		} 
		else
		{
			document.location.href = "trial-report.action";
		}
	}
	
	function getDel(id){
		$("#"+id).remove();    
	}

</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="trial-code-export-excel.action" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										&nbsp;状态:
										<s:select list="statusMap" listKey="key" emptyOption="true" listValue="value" theme="simple" id="status" />
										&nbsp;产品:
										<s:select list="products" listKey="id" listValue="name" emptyOption="true" theme="simple" id="product" />
										<input name="submit" type="button" class="query-button" value="查 询" onclick="checkFor();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<pm:authority name="trialNumMgmt:export:excel">
								<input type="submit" value="导出Excel" class="button" />
							</pm:authority>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="10" align="center" bgcolor="#EEEEEE" class="tablestyle_title">导出试用号</td>
                    				  </tr>
					                  <tr>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">客户名称</td>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">产品名称</td>
						                <td width="30%" align="center" bgcolor="#EEEEEE">试用号</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="productTrials.size() > 0">
						                  <s:iterator value="productTrials">
											  <tr id="<s:property value="id"/>">
											  	<s:hidden name="trialIds" value="%{id}"/>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="customerInfo.companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="trialCode.trialNum"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:a href="javascript:getDel(%{id});">删除</s:a></div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="10">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
        	<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<input type="submit" value="导出Excel" class="button" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
      </table>
	</td>
  </tr>
</table>
</s:form>
	
</body>
</html>
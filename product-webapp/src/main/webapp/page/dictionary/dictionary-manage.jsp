<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>

<script type="text/javascript">
	
function checkClick() 
{
	var category = $("#category").val();
	if (category != "")
	{
		document.location.href = "dictionary-manage.action?category=" + category;
	}
	else
	{
		document.location.href = 'dictionary-manage.action';
	}
	
}
	
</script>

</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										类型:<s:select id="category" list="categories" listKey="key" listValue="value" emptyOption="true" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
										<pm:authority name="systemSetting:dictionary:add">
											<input type="button" class="button" value="新 增" onclick="document.location.href='dictionary-edit.action'"/>
										</pm:authority>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="5" align="center" bgcolor="#EEEEEE"class="tablestyle_title">字典列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="11%" align="center" bgcolor="#EEEEEE">编号</td>
					                    <td width="27%" align="center" bgcolor="#EEEEEE">文本</td>
					                    <td width="27%" align="center" bgcolor="#EEEEEE">排序</td>
					                    <td width="27%" align="center" bgcolor="#EEEEEE">备注</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="dictionaries.size() > 0">
						                  <s:iterator value="dictionaries">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="serialNumber"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="text"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="sortOrder"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="remark"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="systemSetting:dictionary:add">
							                    		<s:a href="dictionary-edit.action?dictionary.id=%{id}"><img src="../page/images/system/edit.gif" title="编辑"/></s:a>
							                    	</pm:authority>
							                    	<pm:authority name="systemSetting:dictionary:remove">
							                    		<s:a href="dictionary-remove.action?dictionary.id=%{id}"><img src="../page/images/system/remove.png" title="删除"/></s:a>
							                    	</pm:authority>
							                    </div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="5">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
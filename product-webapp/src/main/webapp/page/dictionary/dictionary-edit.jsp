<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		
		$("#serialNum").blur(function(){
			var serialNumber = $("#serialNum").val();
			var category = $("#category").val();
			$.post("serialNumber-exist.action",{"serialNumber":serialNumber,"category":category},
					function(data) {
						if (data.result == 1) {
							$("#serialNum").val("");
							$("#alertInfo").text("*该编号已存在");
						}
					}, "json");
		});
		
		$("#sortOrder").blur(function(){
			var sortOrder = $("#sortOrder").val();
			var category = $("#category").val();
			$.post("sortOrder-exist.action",{"sortOrder":sortOrder,"category":category},
					function(data) {
						if (data.result == 1) {
							$("#sortOrder").val("");
							$("#alertInfo").text("*该序号已被占用");
						}
					}, "json");
		
		});
		
		$("#dictionarySub").submit(function(){
			var serialNumber = $("#serialNum").val();
			var sortOrder = $("#sortOrder").val();
			var text = $("#text").val();
			if (serialNumber == "" || sortOrder == "" || text == "")
			{
				$("#alertInfo").text("*请填写完整的信息");
				return false;
			}
			return true;
		});
		
	});
	
	function cleanInfo()
	{
		$("#alertInfo").text("");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="dictionary-submit.action" id="dictionarySub" >
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">新增字典页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button"  />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加字典</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">类型:</td>
												<td width="41%"><s:select list="categories" id="category" listKey="key" listValue="value" name="dictionary.category" theme="simple"/></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">编号:</td>
												<td width="41%"><input name="dictionary.serialNumber" id="serialNum" value="<s:property value="dictionary.serialNumber"/>" class="text" type="text" size="20" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">文本:</td>
												<td><input name="dictionary.text" class="text" size="20" id="text" value="<s:property value="dictionary.text"/>" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">排序:</td>
												<td><input name="dictionary.sortOrder" id="sortOrder" value="<s:property value="dictionary.sortOrder"/>" class="text" size="20" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">备注:</td>
												<td colspan="3"><input name="dictionary.remark" value="<s:property value="dictionary.remark"/>" class="text" size="20" /></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="dictionary.id" />
		<s:hidden name="dictionary.version" />
	</s:form>
</body>
</html>
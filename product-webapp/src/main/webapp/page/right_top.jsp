<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../style/main.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.STYLE3 {
	font-size: 12px;
	color: #000000;
}
a.logout{color:#FFFFFF;text-decoration:none; }
a.logout:hover {color:#CC3300;text-decoration:underline;}
</style>
</head>
<body>
     <div id="right_top">
		<div id="img"><img src="../images/close.gif"/></div>
	    <span class="imgtext">打开/关闭左栏</span>
		 <div id="loginout">
			<div id="loginoutimg"><a href="../logout.action" class="logout"><img src="../images/loginout.gif" /></a></div>
			<span class="logintext"><a href="../logout.action" class="logout">退出系统</a></span>	 
		 </div>			   		
	</div>
	<div id="right_font" class="STYLE3" align="left"><img src="../images/main_14.gif"/>
	<s:if test="navBar == null">您现在所在的位置：个人首页</s:if>
	<s:property value="%{navBar}"/></div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<link type="text/css" href="../page/style/datepicker/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../page/style/datepicker/style.css" rel="stylesheet" />
<script type="text/javascript" src="../page/js/datepicker/ui.core.js"></script>
<script type="text/javascript" src="../page/js/datepicker/ui.datepicker.js"></script>
<script type="text/javascript" src="../page/js/datepicker/ui.datepicker-zh_CN.js"></script>
<link href="../page/style/ztree/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script src="../page/js/ztree/jquery.ztree.core-3.2.js" type="text/javascript"></script>
<script src="../page/js/ztree/jquery.ztree.excheck-3.2.js" type="text/javascript"></script>


<script type="text/javascript">
	
	$(function(){
		
		$("#userSub").submit(function(){
			var username = $("#username").val();
			if (username == "" )
			{
				$("#alertInfo").text("*请输入账号");
				return false;
			}
			
			var password = $("#password").val();
			if (password == "" )
			{
				$("#alertInfo").text("*请输入密码");
				return false;
			}
			else if (password.length < 6) 
			{
				$("#alertInfo").text("*密码长度不能小于6位");
				return false;
			}
			else if (password.length > 20)
			{
				$("#alertInfo").text("*密码长度不能大于20位");
				return false;
			}
			
			var personName = $("#personName").val();
			if (personName == "" )
			{
				$("#alertInfo").text("*请输入员工姓名");
				return false;
			}
			
			var employDate = $("#employDate").val();
			if (employDate == "" )
			{
				$("#alertInfo").text("*请选择入职日期");
				return false;
			}
			
			if ($("#alertInfo").val() != "")
			{
				return false;
			} 
			
			return true;
		});
		
		$("#email").blur(function(){
			var email = $("#email").val();
			if (email != "")
			{
				if (email.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/))
				{
					return true;	
				}
				else
				{
					$("#alertInfo").text("*email格式不正确");
					return false;
				}
			}
			else
			{
				$("#alertInfo").text("*请输入email");
				return false;
			}
		});
		
		datePicker('#employDate','zh_CN');
		
	});
	
	function datePicker(pickerName,locale) {
		$(pickerName).datepicker($.datepicker.regional[locale]);
		$(pickerName).datepicker('option', 'changeMonth', true);//月份可调
		$(pickerName).datepicker('option', 'changeYear', true);//年份可调
	}
	
	function selectOrg()
	{
		$.layer({
			 v_box : 1,
			 v_istitle:false,
			 v_dom : '#orgDiv',	//id
			 v_area : ['250px','300px'],
			 v_btns : 0
		}); 
	}	
	
	function selectRole()
	{
		$.layer({
			 v_box : 1,
			 v_istitle:false,
			 v_dom : '#roleDiv',	//id
			 v_area : ['200px','300px'],
			 v_btns : 0
		}); 
	}
	
</script>
</head>
<body >

	<s:include value="../right_top.jsp" />
	<s:form action="user-submit.action" id="userSub">
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title"><s:if test="personUser != null">编辑用户界面</s:if><s:else>新增用户页面</s:else></th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend><s:if test="personUser != null">编辑用户</s:if><s:else>添加用户</s:else></legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">账号:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.username"/>" id="username" name="personUser.username" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()" /><span class="red">*</span></td>
											</tr>
											<s:if test="personUser == null">
											<tr>
												<td nowrap align="right" width="13%">密码:</td>
												<td width="41%" align="left"><input value="123456" id="password" name="password" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											</s:if>
											<s:else>
												<s:hidden name="password" value="%{personUser.password}" />
											</s:else>
											<tr>
												<td nowrap align="right" width="13%">员工姓名:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.person.name"/>" id="personName" name="personUser.person.name" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()" /><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">性别:</td>
												<td width="41%" align="left"><s:select list="genders" name="personUser.person.gender" listValue="getText(name())" theme="simple"/></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">E-mail:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.person.email"/>" id="email" name="personUser.person.email" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">入职时间:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.person.employDate"/>" name="personUser.person.employDate" id="employDate" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">状态:</td>
												<td width="41%" align="left">
													<s:select list="mapStates" listKey="key" listValue="value" name="personUser.disabled" theme="simple"/>
												</td>
											</tr>
											<s:if test="personUser == null">
												<tr>
													<td nowrap align="right" width="13%">所属部门:</td>
													<td width="41%" align="left">
														<div id="orgList"></div>
														<input type="button" value="请选择" class="button" onclick="selectOrg();"/>
													</td>
												</tr>
												<tr>
													<td nowrap align="right" width="13%">角色:</td>
													<td width="41%" align="left">
														<div id="roleList"></div>
														<input type="button" value="请选择" class="button" onclick="selectRole();"/>
													</td>
												</tr>
											</s:if>
											<tr>
												<td nowrap align="right" width="13%">qq:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.person.qq"/>" name="personUser.person.qq" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">手机号码:</td>
												<td width="41%" align="left"><input value="<s:property value="personUser.person.mobile"/>" name="personUser.person.mobile" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="left" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="personUser.id" />
		<s:hidden name="personUser.version" />
		<s:hidden name="personUser.person.id" />
		<s:hidden name="personUser.person.version" />
		<s:hidden name="personUser.password" />
		
	</s:form>
	
	<div id="orgDiv" style=" display:none;">
		<div align="center"><span style="font-size: 18px;color: #003399;font-weight: bold;">用户所属部门</span></div>
		<input type="submit" value="保存" class="button" onclick="layer_close();"/>
		<div class="zTreeDemoBackground left">
			<ul id="orgTree" class="ztree"></ul>
		</div>
		<input type="button" value="保存" class="button" onclick="layer_close();"/>
	</div>
	
	<div id="roleDiv" style=" display:none;">
		<div align="center"><span style="font-size: 18px;color: #003399;font-weight: bold;">用户角色</span></div>
		<input type="submit" value="保存" class="button" onclick="layer_close();"/>
		<div class="zTreeDemoBackground left">
			<ul id="roleTree" class="ztree"></ul>
		</div>
		<input type="button" value="保存" class="button" onclick="layer_close();"/>
	</div>
	
	<script type="text/javascript">
	
		function zTreeOnCheck(event, treeId, treeNode) {
			if (treeNode.checked) {
				$("#orgList").after('<span id=org' + treeNode.id + '>' + treeNode.name +'&nbsp;</span>');
				$("#orgList").after('<input type=hidden value=' + treeNode.id +' id=ownOrg' + treeNode.id +' name=orgIds>');
			} else {
				$("#org" + treeNode.id).remove();
				$("#ownOrg" + treeNode.id).remove();
			}
		};
	
		var orgSetting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				onCheck: zTreeOnCheck
			}
		};
		
		function roleZTreeOnCheck(event, treeId, treeNode) {
			if (treeNode.checked) {
				$("#roleList").after('<span id=rol' + treeNode.id + '>' + treeNode.name +'&nbsp;</span>');
				$("#roleList").after('<input type=hidden value=' + treeNode.id +' id=ownRole' + treeNode.id +' name=roleIds>');
			} else {
				$("#rol" + treeNode.id).remove();
				$("#ownRole" + treeNode.id).remove();
			}
		};
		
		var roleSetting = {
				check: {
					enable: true
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onCheck: roleZTreeOnCheck
				}
			};
	
		var code;
		
		function setCheck() {
			var type = { "Y" : "", "N" : "" };
			orgSetting.check.chkboxType = type;
			showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
		}
		
		function showCode(str) {
			if (!code) code = $("#code");
			code.empty();
			code.append("<li>"+str+"</li>");
		}
		
		$(document).ready(function(){
			 $.post("user-organization-json-data.action",
					function(data) {
				 		$.fn.zTree.init($("#orgTree"), orgSetting, data.results);
				}, "json"); 
			 $.post("user-role-json-data.action",
						function(data) {
					 		$.fn.zTree.init($("#roleTree"), roleSetting, data.results);
					}, "json"); 
			setCheck();
			$("#py").bind("change", setCheck);
			$("#sy").bind("change", setCheck);
			$("#pn").bind("change", setCheck);
			$("#sn").bind("change", setCheck);
		});
		
</script>
	
	
</body>
</html>
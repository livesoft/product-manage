<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		
		$("#resetPasswordForm").submit(function(){
			var password1 = $("#password1").val();
			var password2 = $("#password2").val();
			
			if (password1 == "")
			{
				$("#alertInfo").text("*请填写密码");
				return false;
			}
			else if (password1.length < 6) 
			{
				$("#alertInfo").text("*密码长度不能小于6位");
				return false;
			}
			else if (password1.length > 20)
			{
				$("#alertInfo").text("*密码长度不能大于20位");
				return false;
			}
			
			if (password2 == "")
			{
				$("#alertInfo").text("*请填写密码");
				return false;
			}
			else if (password2.length < 6) 
			{
				$("#alertInfo").text("*密码长度不能小于6位");
				return false;
			}
			else if (password2.length > 20)
			{
				$("#alertInfo").text("*密码长度不能大于20位");
				return false;
			}
			
			if (password1 != password2)
			{
				$("#alertInfo").text("密码必须一样");
				return false;
			}
			
			$.ajax({
				url: "user-password-submit.action",
				cache: false,
				async: false,
				data: $("#resetPasswordForm").serialize(),
				success:function(){
					parent.closeWin("密码重置成功");
				}
			});
			
			return false;
		});
		
	});
	
</script>

</head>
<body class="ContentBody">

		<s:form id="resetPasswordForm">
			<s:hidden name="personUser.id" value="%{personUser.id}" />
			<div class="MainDiv">
				<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
					<tr>
						<td class="CPanel">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
								<tr>
									<td width="100%">
										<fieldset style="height: 100%;">
											<legend>重置密码</legend>
											<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right">重设密码:</td>
													<td><input name="newPassword" type="password" id="password1" class="text" onfocus="cleanInfo()" /></td>
												</tr>
												<tr>
													<td nowrap align="right">确认密码:</td>
													<td><input id="password2" type="password" class="text" onfocus="cleanInfo()" /></td>
												</tr>
												<tr>
													<td align="right" ></td>
													<td align="center" style="color: red;"><div id="alertInfo"></div></td>
												</tr>
											</table>
											<br />
										</fieldset>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" height="50px">
							<input type="submit" value="保存" class="button" />
						</td>
					</tr>
				</table>
			</div>
		</s:form>
		
</body>
</html>

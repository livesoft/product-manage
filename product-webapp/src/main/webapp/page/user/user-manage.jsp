<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	function removeUser(id) 
	{
		$.layer({
			 v_button:2,
			 v_istitle:false,
			 v_msg:'<span style="font-size: 20px;color: #CC0000;">确定要删除该用户？</span>',
			 yes:function(){
				$.post("user-remove.action",{"personUser.id":id},
					function(data) {
						if (data.result == 1) {
							closeWinRE("该用户已被引用");
						} else {
							closeWin("删除成功");
						}
				}, "json");
			 },
			 no:function(){
				 layer_close(true);
			 },
			 v_btn:['删除','取消']
		});
	}
	
	function resetPassword(id)
	{
		var url = 'user-password-reset.action?personUser.id=' + id;
		popupNewWin(url,"400px","230px");
	}	

	function checkClick()
	{
		var roleName = $("#roleName").val();
		if (roleName != "")
		{
			document.location.href = 'role-manage.action?roleName=' + roleName;
		}
		else
		{
			document.location.href = 'role-manage.action';
		}
	}
	
</script>

</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										用户账号:<input name="textfield" type="text" size="12" id="roleName" />
										<input name="submit" type="button" class="query-button" value="查 询" onclick="checkClick();"/>
										<pm:authority name="systemSetting:user:add">
											<input type="button" class="button" value="添加用户" onclick="document.location.href='user-edit.action'">
										</pm:authority>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="10" align="center" bgcolor="#EEEEEE" class="tablestyle_title">用户列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="3%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">账号</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">员工姓名</td>
					                    <td width="3%" align="center" bgcolor="#EEEEEE">性别</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">邮箱</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">所属部门</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">所在岗位</td>
					                    <td width="14%" align="center" bgcolor="#EEEEEE">拥有角色</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">入职时间</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">管理</td>
					                  </tr>
					                  <s:if test="personUsers.size() > 0">
						                  <s:iterator value="personUsers" id="user">
											  <tr>
												<td bgcolor="#FFFFFF">
													<div align="center">
														<s:if test="disabled">
															<img src="../page/images/effective.png" title="激活"/>
														</s:if>
														<s:else>
															<img src="../page/images/invalid.png" title="禁用"/>
														</s:else>												
													</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="username"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="person.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:text name="%{person.gender}"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="email"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="@com.dayatang.product.webapp.action.user.UserManageAction@getOrganizations(person.id)"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="@com.dayatang.product.webapp.action.user.UserManageAction@getRoles(#user)"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="person.employDate" format="yyyy-MM-dd"/></div></td>
							                    <td bgcolor="#FFFFFF">
							                    	<s:if test="id != 1">
								                    	<div align="center">
								                    		<pm:authority name="systemSetting:user:remove">
								                    			<s:a href="javascript:removeUser(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
								                    		</pm:authority>
								                    		<pm:authority name="systemSetting:user:add">
								                    			<s:a href="user-edit.action?personUser.id=%{id}"><img src="../page/images/system/edit.gif" title="编辑"/></s:a>
								                    		</pm:authority>
								                    		<pm:authority name="systemSetting:user:reset">
								                    			<s:a href="javascript:resetPassword(%{id});"><img src="../page/images/system/password.gif" title="重置密码"/></s:a>
								                    		</pm:authority>
								                    	</div>
							                    	</s:if>
							                    	<s:else>
							                    		<div align="center">
								                    		(无权限管理)
								                    	</div>
							                    	</s:else>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="10">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/ztree/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/ztree/jquery.ztree.core-3.2.js" type="text/javascript"></script>
<script src="../page/js/ztree/jquery.ztree.excheck-3.2.js" type="text/javascript"></script>

<script type="text/javascript">
	
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
	
		var code;
		
		function setCheck() {
			var type = { "Y" : "", "N" : "" };
			setting.check.chkboxType = type;
			showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
		}
		
		function showCode(str) {
			if (!code) code = $("#code");
			code.empty();
			code.append("<li>"+str+"</li>");
		}
		
		$(document).ready(function(){
			 $.post("user-organization-json-data.action",
					function(data) {
				 		$.fn.zTree.init($("#treeDemo"), setting, data.results);
				}, "json"); 
			setCheck();
			$("#py").bind("change", setCheck);
			$("#sy").bind("change", setCheck);
			$("#pn").bind("change", setCheck);
			$("#sn").bind("change", setCheck);
		});
		
		
</script>


</head>
<body>
	
	<div align="center" style="font-size: 18px;color: #003399;font-weight: bold;">用户所属部门</div>
	<div class="zTreeDemoBackground left">
		<ul id="treeDemo" class="ztree"></ul>
	</div>

</body>
</html>

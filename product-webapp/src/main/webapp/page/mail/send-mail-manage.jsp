<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function removeSendMail(id)
	{
		$.post("send-mail-remove.action",
				{"sendMail.id" : id},
				function(data){
					if (data.result == 1)
					{
						alert("该邮件已被引用");
					}
					else
					{
						document.location.href = 'send-mail-manage.action';
					}
		},"json");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="4" align="center" bgcolor="#EEEEEE"class="tablestyle_title">邮件列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">邮件名称</td>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">是否发送完成</td>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">收件人数\成功数\发送中\失败数</td>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="sendMails.size() > 0">
						                  <s:iterator value="sendMails">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="htmlMail.subject"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:if test="isSend">是</s:if><s:else>否</s:else></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:property value="mailReceivers.size()"/>
							                    	<%int i = 0, j = 0, k = 0; %>
							                    	<s:iterator value="mailReceivers">
							                    		<s:if test="isSuccess">
							                    			<%i=i+1; %>
							                    		</s:if>
							                    		<s:elseif test="error == null">
							                    			<%k=k+1; %>
							                    		</s:elseif>
							                    		<s:else>
							                    			<%j=j+1; %>
							                    		</s:else>
							                    	</s:iterator>
							                    	\<%=i %>\<%=k %>\<%=j %>
							                    </div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:if test="!isSend">
							                    		<pm:authority name="mailMgmt:detail:send">
							                    			<s:a href="send-mail-submit.action?sendMailId=%{id}">发送</s:a>
							                    		</pm:authority>
							                    	</s:if>
							                    	<s:a href="send-mail-detail.action?sendMail.id=%{id}">详情</s:a>
							                    	<pm:authority name="mailMgmt:detail:remove">
							                    		<s:a href="javascript:removeSendMail(%{id})">删除</s:a>
							                    	</pm:authority>
							                    	</div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="4">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
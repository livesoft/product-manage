<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
</head>
<body class="ContentBody">
		
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">邮件发送详情页面</th>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>发送详情</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">主题:</td>
												<td align="left"><s:property value="sendMail.htmlMail.subject"/></td>
											</tr>
											<tr height="50px" align="center">
												<td nowrap align="center" width="20%">正文:</td>
												<td width="80%" align="center">
													<s:property value="sendMail.htmlMail.htmlMsg" escape="false"/>
												</td>
											</tr>
											<s:if test="sendMail.htmlMail.attachments.size() > 0">
												<tr height="30px" align="center">
													<td nowrap align="center" width="20%">附件:</td>
													<td align="left">
														<s:iterator value="sendMail.htmlMail.attachments">
															<s:a href="javascript:downloadFile(%{id})"><s:property value="fileName"/></s:a>&nbsp;&nbsp;&nbsp;<s:property value="size"/>KB
														</s:iterator>
													</td>
												</tr>
											</s:if>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">收件人数:</td>
												<td align="left"><s:property value="sendMail.mailReceivers.size()"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">发送情况:</td>
												<td align="left">
													<s:iterator value="sendMail.mailReceivers">
														<s:property value="customer.companyName"/>(<s:property value="customer.email"/>)
														<s:if test="isSuccess">
															&nbsp;&nbsp;发送成功!&nbsp;&nbsp;发送时间:<s:date name="sendDate"/>&nbsp;
														</s:if>
														<s:elseif test="error != null">
															发送失败:<s:property value="error"/>
														</s:elseif>
														<s:else>
															发送中...
														</s:else>
														<br>
													</s:iterator>
												</td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	
</body>
</html>
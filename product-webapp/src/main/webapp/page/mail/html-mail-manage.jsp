<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function removeHtmlMail(id)
	{
		$.post("html-mail-remove.action",
				{"htmlMail.id" : id},
				function(data){
					if (data.result == 1)
					{
						alert("该邮件已被引用");
					}
					else
					{
						document.location.href = 'html-mail-manage.action';
					}
				},"json"
		);
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<pm:authority name="mailMgmt:html:add">
								<input type="button" value="新增HTML邮件" class="button" onclick="document.location.href='html-mail-edit.action'" />
							</pm:authority>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">邮件列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">主题</td>
					                    <td width="40%" align="center" bgcolor="#EEEEEE">正文</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">发送邮箱</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="mails.size() > 0">
						                  <s:iterator value="mails">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="subject"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">........................................</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="emailInfo.username"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    <pm:authority name="mailMgmt:html:view">
							                    	<s:a href="html-mail-detail.action?htmlMail.id=%{id}">查看</s:a>&nbsp;<s:a href="send-mail.action">发送邮件</s:a>&nbsp;
							                    </pm:authority>
							                    <s:if test="textMsg != 'registerCodeEmail'">
							                    	<pm:authority name="mailMgmt:html:remove">
							                    		<s:a href="javascript:removeHtmlMail(%{id})">删除</s:a>
							                    	</pm:authority>
							                    </s:if>
							                    </div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="4">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
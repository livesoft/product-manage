<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function customerSel()
	{
		$("#alertInfo").text("");
		var cust = $("#cust").val();
		
		if (cust == 0) 
		{
			$("#prods").css("display","block");
			$("#optDiv").css("display","none");
		}
		else
		{
			$("select[name=recipients] option").each(function(){
				$(this).remove();
			});
			$.post("get-customer-info.action",
					{"professionType" : cust},
					function(data){
						$.each(data.customerInfos,function(i, item){
							$("<option></option>").attr("value",item.id).html(item.companyName + "(" +item.email + ")").appendTo($("select[name=recipients]"));
						});
					},"json"
			);
			$("#prods").css("display","none");
			$("#optDiv").css("display","block");
		}

	}
	
	function checkForm()
	{
		var mail = $("#mail").val();
		var cust = $("#cust").val();
		var len = 0;
		$("select[name=recipients] option").each(function(){
			len = len + 1;
		});
		if (mail == "")
		{
			$("#alertInfo").text("*请选择邮件");
			return false;
		}
		if (cust == 0)
		{
			$("#alertInfo").text("*请选择行业");
			return false;
		}
		if (len == 0)
		{
			$("#alertInfo").text("*请收件人");
			return false;
		}
		$("select[name=recipients] option").each(function(){
			$(this).attr("selected",true);
		});
		return true;
	}
	
	function btnOper(operate)
	{
		$("#operate").val(operate);
	}
	
</script>
</head>
<body class="ContentBody">

	<s:form action="send-mail-preview.action" theme="simple" onsubmit="return checkForm();">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">发送邮件页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="发送" class="button" onclick="btnOper('save');"/>
									<input type="submit" value="预览" class="button" onclick="btnOper('preview');"/>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>发送邮件</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">邮件:</td>
												<td width="41%"><s:select list="mails" id="mail" name="mailId" emptyOption="true" listKey="id" listValue="subject"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">收件人:</td>
												<td><s:select headerKey="0" id="cust" headerValue="请选择行业" list="dictionarys" listKey="serialNumber" listValue="text" onchange="customerSel()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right"></td>
												<td>
													<div id="optDiv" style="display: none;">
													<s:optiontransferselect
												        name="recipients"
												        leftTitle="发送"
												        rightTitle="不发送"
												        list="{'0'}"
												        cssStyle="width:300px;height:300px;"
												        doubleCssStyle="width:300px;height:300px;"
												        doubleName="rightList"
												        doubleList=""
												        addToLeftLabel="向左移动"
												        addToRightLabel="向右移动"
												        addAllToLeftLabel="全部左移"
												        addAllToRightLabel="全部右移"
												        allowUpDownOnLeft="false"
												        allowUpDownOnRight="false"
												        allowSelectAll="false"
												        />
													<span class="red">*</span>
													</div>
												</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
										
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<s:hidden id="operate" name="operate"/>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="发送" class="button" onclick="btnOper('save');"/>
						<input type="submit" value="预览" class="button" onclick="btnOper('preview');"/>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/tqedit/TQEditor.min.js" type="text/javascript"></script>
<link href="../page/style/uploadify/upload.css" rel="stylesheet" type="text/css" />
<script src="../page/js/uploadify/swfobject.js" type="text/javascript"></script>
<script src="../page/js/uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$("#file_upload").uploadify({
	        'uploader': '../page/swf/uploadify/uploadify.swf',
	        'script': 'html-mail-submit.action',
	        'method' : 'POST',
	        'cancelImg': '../page/images/uploadify/cancel.png',
	        'auto': false,
	        'multi': false,
	        'fileDataName':'files', 
	        'queueID': 'fileQueue',
	        'buttonText':'Select File',
	        'onComplete' : function(event, ID, fileObj, response, data) {
	        	document.location.href = 'html-mail-manage.action';
	         }, 
	        'onError' : function(event, queueID, fileObj,errorObj){
	        	alert(errorObj.type + "Error:" + errorObj.info);
	        }
	    });
	});

	function checkForm()
	{
		var subject = $("#subject").val();
		var msg = $("#e1").val();
		var formEmail = $("#formEmail").val();
		
		if (subject == "")
		{
			$("#alertInfo").text("*请输入主题");
			return false;
		}
		if (msg == "")
		{
			$("#alertInfo").text("*请输入正文");
			return false;
		}
		if (formEmail == "")
		{
			$("#alertInfo").text("*请选择邮箱");
			return false;
		}
		if ($("#fileQueue").text() == "")
		{
			document.location.href = "html-mail-submit.action?htmlMail.subject=" + subject + "&htmlMail.htmlMsg=" + msg + "&htmlMail.emailInfo.id=" + formEmail;			
		}
		else
		{
			$('#file_upload').uploadifySettings('scriptData',{'htmlMail.subject':document.getElementById('subject').value,'htmlMail.htmlMsg':document.getElementById('e1').value,'htmlMail.emailInfo.id':document.getElementById('formEmail').value});
			$('#file_upload').uploadifyUpload();
		}
		
		return true;
	}
		
	function cleanInfo()
	{
		$("#alertInfo").text("");
	}

</script>
</head>
<body>
		<s:include value="../right_top.jsp"/>
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">新增邮件页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" onclick="checkForm();"/>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加邮件</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">主题:</td>
												<td width="41%"><input id="subject" class="text" name="htmlMail.subject" type="text" size="100" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">正文:</td>
												<td>
													<textarea id="e1" rows="20" cols="100" name="htmlMail.htmlMsg" onfocus="cleanInfo()"></textarea>
													<script type="text/javascript">
														new  tqEditor('e1',{toolbar:'full'});
													</script><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">附件:</td>
												<td><div id="fileQueue"></div><s:file name="files" id="file_upload" theme="simple"/></td>
											</tr>
											<tr>
												<td nowrap align="right">发送邮箱:</td>
												<td colspan="3"><s:select list="emailInfos" name="htmlMail.emailInfo.id" emptyOption="true" id="formEmail" listKey="id" listValue="username" theme="simple" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" onclick="checkForm();"/>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>
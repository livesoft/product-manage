<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
</head>
<body class="ContentBody">
		
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">邮件预览页面</th>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="发送" class="button" onclick="document.location.href = 'send-mail-submit.action?sendMailId=<s:property value="mailId"/>'" />
						<input type="button" value="返回" class="button" onclick="document.location.href = 'send-mail-manage.action'" />
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>邮件预览</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">主题:</td>
												<td align="left"><s:property value="htmlMail.subject"/></td>
											</tr>
											<tr height="50px" align="center">
												<td nowrap align="center" width="20%">正文:</td>
												<td width="80%" align="left">
													<s:property value="htmlMail.htmlMsg" escape="false"/>
												</td>
											</tr>
											<s:if test="htmlMail.attachments.size() > 0">
												<tr height="30px" align="center">
													<td nowrap align="center" width="20%">附件:</td>
													<td align="left">
														<s:iterator value="htmlMail.attachments">
															<s:a href="javascript:downloadFile(%{id})"><s:property value="fileName"/></s:a>&nbsp;&nbsp;&nbsp;<s:property value="size"/>KB
														</s:iterator>
													</td>
												</tr>
											</s:if>
											<tr height="50px" align="center">
												<td nowrap align="center" width="20%">收件人:</td>
												<td width="80%" align="left">
													<s:iterator value="customers" status="cus">
														<s:if test="#cus.getIndex()%2 > 0">
															<s:property value="companyName"/>(<s:property value="email"/>)<br/>
														</s:if>
														<s:else>
															<s:property value="companyName"/>(<s:property value="email"/>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														</s:else>
													</s:iterator>
												</td>
											</tr>
											<tr>
												<td colspan="2" align="center" height="50px">
													<input type="submit" value="发送" class="button" />
													<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
												</td>
											</tr>
										</table>
										<br/>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	
</body>
</html>
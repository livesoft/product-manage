<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>

<script type="text/javascript">
	
	$(function(){
		
		$("#organizationSub").submit(function(){
			var name = $("#name").val();
			
			if (name == "" )
			{
				$("#alertInfo").text("*请填写机构名称");
				return false;
			}
			else if ($("#alertInfo").val() != "")
			{
				return false;
			}
			
			$.ajax({
				url: "organization-submit.action",
				cache: false,
				async: false,
				data: $("#organizationSub").serialize(),
				success:function(){
					parent.closeWin("添加成功");
				}
			});
			return false;
		});
		
	});
	
</script>
</head>
<body class="ContentBody">
	
	<s:form action="organization-submit.action" id="organizationSub" >
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加组织机构</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">组织名称:</td>
												<td width="41%"><input name="organization.name" value="<s:property value="organization.name"/>" id="name" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" height="120px">组织描述:</td>
												<td colspan="3"><textarea id="productDescribe" name="organization.orgDescribe" rows="5" cols="50" onfocus="cleanInfo()"><s:property value="organization.orgDescribe"/></textarea></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">类型:</td>
												<td width="41%"><s:select list="orgDictionarys" listKey="key" listValue="value" name="organization.organizationType" theme="simple"/></td>
											</tr>
											<tr>
												<td nowrap align="right">负责人:</td>
												<td><input type="button" value="选择"/></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="parent.id" value="%{parentId}"/>
		<s:hidden name="organization.id" />
		<s:hidden name="organization.version" />
		<s:hidden name="organization.leftValue"/>
		<s:hidden name="organization.rightValue"/>
		<s:hidden name="organization.level"/>
		
	</s:form>
	
</body>
</html>
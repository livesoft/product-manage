<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib prefix="pm" uri="/authority-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>组织机构</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<link href="../page/style/organization/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../page/style/organization/jquery.jOrgChart.css" rel="stylesheet" type="text/css" />
<link href="../page/style/organization/custom.css" rel="stylesheet" type="text/css" />
<link href="../page/style/organization/prettify.css" rel="stylesheet" type="text/css" />
<script src="../page/js/organization/prettify.js" type="text/javascript"></script>
<script src="../page/js/organization/jquery.min.js"></script>
<script src="../page/js/organization/jquery.jOrgChart.js" type="text/javascript"></script>
<script src="../page/js/contextmenu/jquery.contextmenu.r2-min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>

<script type="text/javascript">

	jQuery(document).ready(function() {
	    $("#org").jOrgChart({
			chartElement : '#chart'
		});
		
		/* Extra Code */
		$("#show-list").click(function(e){
			e.preventDefault();
			
			$('#list-html').toggle('fast', function(){
				if($(this).is(':visible')){
					$('#show-list').text('Hide underlying list.');
					$(".topbar").fadeTo('fast',0.9);
				}else{
					$('#show-list').text('Show underlying list.');
					$(".topbar").fadeTo('fast',1);					
				}
			});
		});
		
		$('div.node').contextMenu('myMenu1', {
			bindings: {
				'add': function(t) {
					$.layer({	
						v_box : 2,
						v_title:'添加组织',
					    v_src : 'organization-edit.action?parentId=' + t.id,
					    v_area : ['700px','450px'],
					    v_btns : 0
					});
				 },
				 'edit': function(t) {
					 $.layer({	
							v_box : 2,
							v_title:'编辑组织',
						    v_src : 'organization-edit.action?organization.id=' + t.id,
						    v_area : ['700px','450px'],
						    v_btns : 0
						});
				 },
				 'delete': function(t) {
					 $.layer({
						 v_button:2,
						 v_title:'删除组织',
						 v_msg:'<span style="font-size: 20px;color: #CC0000;">确定要删除该组织？</span>',
						 yes:function(){
							$.post("organization-remove.action",{"organization.id":t.id},
								function(data) {
									if (data.result == 2) {
										closeWinRE("该组织有下属机构");
									} else if (data.result == 1) {
										closeWinRE("该组织已被引用");
									} else {
										closeWin("删除成功");
									}
							}, "json");
							  
						 },
						 no:function(){
							 layer_close(true);
						 },
						 v_btn:['删除','取消']
					  });
				 },
				 'add-user': function(t) {
						alert('Trigger was '+t.id+'\nAction was Delete');
				 }
			}
		});	
	});	
	
	function closeWin(mes)
	{
		layer_close();
		new Toast({context:$('body'),message:mes}).show();
		setTimeout("location.reload()",800);
	}
	
	function closeWinRE(mes)
	{
		layer_close();
		new Toast({context:$('body'),message:mes}).show();
	}
	
</script>
<style type="text/css">
body{
	background 	: url('../page/images/organization/bkgd.png') repeat top left;
	color 		: white;
	font-family : tahoma;
	font-weight : lighter;
	padding-top	: 40px;
}
	
</style>
</head>
<body onLoad="prettyPrint();">
	
	<!-- 右键弹出div层 -->
	<div class="contextMenu" id="myMenu1">
      <ul>
      	<pm:authority name="systemSetting:organization:add">
        	<li id="add"><img src="../page/images/contextmenu/add.png" /> 添加组织</li>
        	<li id="edit"><img src="../page/images/contextmenu/edit.gif" /> 编辑组织</li>
        </pm:authority>
        <pm:authority name="systemSetting:organization:remove">
        	<li id="delete"><img src="../page/images/contextmenu/delete.png" /> 删除组织</li>
        </pm:authority>
        <pm:authority name="systemSetting:organization:adduser">
        	<li id="add-user"><img src="../page/images/contextmenu/add.png" /> 加入员工</li>
      	</pm:authority>
      </ul>
    </div>

	<s:include value="../right_top.jsp" />
	
	<!-- 组织机构图 -->
	<s:property value="%{orgHtml}" escape="false"/>
	<div id="chart" class="orgChart"></div>
	
</body>
</html>
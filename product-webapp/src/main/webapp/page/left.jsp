<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="../js/prototype.lite.js" type="text/javascript"></script>
<script src="../js/moo.fx.js" type="text/javascript"></script>
<script src="../js/moo.fx.pack.js" type="text/javascript"></script>
<link href="../style/left.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" height="280" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF2FB">
  <tr>
    <td width="182" valign="top"><div id="container">
      <s:iterator value="#session.permissions" id="perm">
      	  <s:if test="children.size() > 0 && parent == null">
	      	  <h1 class="type"><a href="javascript:void(0)"><s:property value="description"/></a></h1>
		      <div class="content">
		          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            <tr>
		              <td><img src="../images/menu_topline.gif" width="182" height="5" /></td>
		            </tr>
		          </table>
		          <ul class="MM">
		        	<s:iterator value="#session.permissions" id="permChild">
		          			<s:if test="children.size() > 0 && parent != null && #perm == #permChild.parent">
		          				<li><a href="<s:property value="linkUrl"/>" target="main"><s:property value="description"/></a></li>
		          			</s:if>
		          </s:iterator>
		        </ul>
		      </div>
	      </s:if>
      </s:iterator>
      </div>
      <script type="text/javascript">
		var contents = document.getElementsByClassName('content');
		var toggles = document.getElementsByClassName('type');
	
		var myAccordion = new fx.Accordion(
			toggles, contents, {opacity: true, duration: 400}
		);
		myAccordion.showThisHideOpen(contents[0]);
	  </script>
     </td>
  </tr>
</table>

</body>
</html>
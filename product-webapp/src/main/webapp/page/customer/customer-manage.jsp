<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">

	function checkClick() 
	{
		var type = $("#professionType").val();
		if (type != "") {
			document.location.href="customer-manage.action?professionType=" + type;
		} else{
			document.location.href="customer-manage.action";
		}
	}
	
	function customerRemove(id)
	{
		$.post("customer-remove.action",
				{"customer.id" : id},
				function(data){
					if (data.result == 1)
					{
						alert("该客户已被引用");
					}
					else
					{
						document.location.href = 'customer-manage.action';
					}
				},"json"
		);
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										行业类型:<s:select list="dictionaryMaps" id="professionType" listKey="key" emptyOption="true" listValue="value" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick()"/>
										<pm:authority name="customerInfoMgmt:list:send">
											<input type="button" class="button" value="发送客户邮件" onclick="document.location.href='../mail/send-mail-customer.action'"/>
										</pm:authority>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="8" align="center" bgcolor="#EEEEEE"class="tablestyle_title">客户信息列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="19%" align="center" bgcolor="#EEEEEE">客户名称</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">行业</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">电话</td>
					                    <td width="16%" align="center" bgcolor="#EEEEEE">邮箱</td>
					                    <td width="27%" align="center" bgcolor="#EEEEEE">地址</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">公司网址</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">客户说明</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="customers.size() > 0">
						                  <s:iterator value="customers">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="getProfessionTypeName(professionType)"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="phone"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="email"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="address"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><a href="<s:property value="companyUrl"/>" target="_blank"><s:property value="companyUrl"/></a></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="customerDescribe"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="customerInfoMgmt:list:edit">
							                    		<s:a href="customer-edit.action?customer.id=%{id}"><img src="../page/images/system/edit.gif" title="编辑"/></s:a> 
													</pm:authority>
													<pm:authority name="customerInfoMgmt:list:remove">
							                    		<s:a href="javascript:customerRemove(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
							                    	</pm:authority>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="8">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <s:include value="../page.jsp" />
     </td>
  </tr>
</table>

</body>
</html>
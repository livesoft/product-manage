<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	function checkClick() 
	{
		var productId = $("#productId").val();
		var importance = $("#importance").val();
		if (productId != "" && importance != "")
		{
			document.location.href = "register-info-manage.action?product.id=" + productId + "&importance=" + importance;
		}
		else if (productId != "")
		{
			document.location.href = "register-info-manage.action?product.id=" + productId;
		}
		else if (importance != "")
		{
			document.location.href = "register-info-manage.action?importance=" + importance;
		}
		else
		{
			document.location.href = 'register-info-manage.action';
		}
		
	}
	
	function editRemork(id)
	{
		var url = "register-info-edit.action?type=remork&registerInfo.id="+id;
		popupNewWin(url,"380px","200px");
	}
	
	function editImportance(id)
	{
		var url = "register-info-edit.action?type=importance&registerInfo.id="+id
		popupNewWin(url,"380px","230px");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										产品:<s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple"/>
										关注度:<s:select list="#{1:'一星',2:'二星',3:'三星',4:'四星',5:'五星'}" id="importance" emptyOption="true" listKey="key" listValue="value" theme="simple"></s:select>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">客户注册信息列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="18%" align="center" bgcolor="#EEEEEE">公司名称</td>
					                    <td width="9%" align="center" bgcolor="#EEEEEE">公司电话</td>
					                    <td width="7%" align="center" bgcolor="#EEEEEE">联系人</td>
					                    <td width="7%" align="center" bgcolor="#EEEEEE">手机</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">邮箱</td>
					                    <td width="20%" align="center" bgcolor="#EEEEEE">地址</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">备注</td>
					                    <td width="6%" align="center" bgcolor="#EEEEEE">关注度</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="registerInfos.size() > 0">
						                  <s:iterator value="registerInfos">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:a href="../product/product-details.action?productTrial.id=%{productTrial.id}"><s:property value="companyName"/></s:a></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="phone"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="contact"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="mobile"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="email"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="address"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="customerDescribe"/></div></td>
							                    <td bgcolor="#FFFFFF">
							                    	<div align="center">
							                    		<s:if test="importance == 1">
							                    			<img src="../page/images/select.gif"/>
							                    		</s:if>
							                    		<s:elseif test="importance == 2">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="importance == 3">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="importance == 4">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="importance == 5">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    	</div>
							                    </td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="customerInfoMgmt:register:remark">
							                    		<s:a id="remork%{id}" href="javascript:editRemork(%{id})">添加备注</s:a>&nbsp;
							                    	</pm:authority>
							                    	<pm:authority name="customerInfoMgmt:register:attention">
							                    		<s:a id="importance%{id}" href="javascript:editImportance(%{id})">修改关注度</s:a>
							                    	</pm:authority>
							                    	</div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="9">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>

</body>
</html>
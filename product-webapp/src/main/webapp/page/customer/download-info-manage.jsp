<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	function checkClick() 
	{
		var productId = $("#productId").val();
		var importance = $("#importance").val();
		if (productId != "" && importance != "")
		{
			document.location.href = "register-info-manage.action?product.id=" + productId + "&importance=" + importance;
		}
		else if (productId != "")
		{
			document.location.href = "register-info-manage.action?product.id=" + productId;
		}
		else if (importance != "")
		{
			document.location.href = "register-info-manage.action?importance=" + importance;
		}
		else
		{
			document.location.href = 'register-info-manage.action';
		}
		
	}
	
	function approvalPass(id)
	{
		var url = 'approval-pass.action?approval.id=' + id;
		popupNewWin(url,"380px","250px");
	}
	
	function approvalReject(id)
	{
		var url = 'approval-reject.action?approval.id=' + id;
		popupNewWin(url,"400px","250px");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										产品:<s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">申请试用客户信息列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">公司名称</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">公司电话</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">联系人</td>
					                    <td width="9%" align="center" bgcolor="#EEEEEE">手机</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">邮箱</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">获取渠道</td>
					                  </tr>
					                  <s:if test="approvals.size() > 0">
						                  <s:iterator value="approvals">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.phone"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.contact"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.mobile"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.email"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:text name="%{category}"/>
							                    	<s:if test="category.toString() == 'APPROVAL_REJECT'">
							                    		(<s:property value="reasonText"/>)
							                    	</s:if>
							                    </div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.trench"/></div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="9">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		$("#customerSub").submit(function(){
			var companyName = $("#companyName").val();
			if (companyName == "")
			{
				$("#alertInfo").text("*请填写客户名称");
				return false;
			}
			return true;
		});
	});
	
	function cleanInfo()
	{
		$("#alertInfo").text("");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="customer-submit.action" id="customerSub" >
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">新增客户信息页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<pm:authority name="customerInfoMgmt:add:add">
										<input type="submit" value="保存" class="button" />
									</pm:authority>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加客户信息</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">行业类型:</td>
												<td width="41%"><s:select list="dictionaryMaps" listKey="key" listValue="value" name="customer.professionType" theme="simple"/></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">客户名称:</td>
												<td width="41%"><input name="customer.companyName" id="companyName" value="<s:property value="customer.companyName"/>" class="text" type="text" size="30" onfocus="cleanInfo();"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">电话:</td>
												<td><input name="customer.phone" class="text" value="<s:property value="customer.phone"/>" size="20" /></td>
											</tr>
											<tr>
												<td nowrap align="right">邮箱:</td>
												<td><input name="customer.email" id="email" class="text" value="<s:property value="customer.email"/>" size="20" onfocus="cleanInfo();"/></td>
											</tr>
											<tr>
												<td nowrap align="right">地址:</td>
												<td colspan="3"><input name="customer.address" value="<s:property value="customer.address"/>" class="text" size="40" /></td>
											</tr>
											<tr>
												<td nowrap align="right">客户说明:</td>
												<td colspan="3"><textarea id="textarea" name="customer.customerDescribe" rows="3" cols="40" ><s:property value="customer.customerDescribe"/></textarea></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<pm:authority name="customerInfoMgmt:add:add">
							<input type="submit" value="保存" class="button" />
						</pm:authority>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="customer.id" />
		<s:hidden name="customer.version" />
	</s:form>
	
</body>
</html>
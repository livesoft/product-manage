<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		
		$("#remorkForm").submit(function(){
			var remork = $("#remork").val();
			if (remork == "")
			{
				$("#alertInfo").text("请填写备注");
				return false;
			}
			
			$.ajax({
				url: "register-remork-submit.action",
				cache: false,
				async: false,
				data: $("#remorkForm").serialize(),
				success:function(){
					parent.closeWin("添加成功");
				}
			});
			
			return true;
		});
		
		$("#importanceForm").submit(function(){
			
			var importance = $("#importance").val();
			if (importance == "")
			{
				$("#alertInfo").text("*请选择关注度");
				return false;
			}
			
			$.ajax({
				url: "register-importance-submit.action",
				cache: false,
				async: false,
				data: $("#importanceForm").serialize(),
				success:function(){
					parent.closeWin("修改成功");
				}
			});
			
			return true;
		});
		
	});
	
	function rate(obj,oEvent){ 
		$("#alertInfo").text("");
		//================== 
		// 图片地址设置 
		//================== 
		var imgSrc = '../page/images/noselect.gif'; //没有填色的星星
		var imgSrc_2 = '../page/images/select.gif'; //打分后有颜色的星星
		//--------------------------------------------------------------------------- 
		if(obj.rateFlag) return; 
		var e = oEvent || window.event; 
		var target = e.target || e.srcElement;  
		var imgArray = obj.getElementsByTagName("img"); 
		for(var i=0;i<imgArray.length;i++){ 
		   imgArray[i]._num = i; 
		   imgArray[i].onclick=function(){ 
		    if(obj.rateFlag) return; 
		    obj.rateFlag=true; 
		    var num = this._num+1;
		    alert("已选关注度为:" + num);
		    $("#importance").val(this._num+1);      //this._num+1这个数字写入到数据库中,作为评分的依据
		   }; 
		} 
		if(target.tagName=="IMG"){ 
		   for(var j=0;j<imgArray.length;j++){ 
		    if(j<=target._num){ 
		     imgArray[j].src=imgSrc_2; 
		    } else { 
		     imgArray[j].src=imgSrc; 
		    } 
		   } 
		} else { 
		   for(var k=0;k<imgArray.length;k++){ 
		    imgArray[k].src=imgSrc; 
		   } 
		} 
	} 
	

</script>
<style type="text/css"> 
	.starWrapper{border:1px solid #FFCC00;padding:5px;width:70px;} 
	.starWrapper img{cursor:pointer;} 
</style> 
</head>
<body class="ContentBody">

	<s:if test="type == 'remork'">
		<s:form id="remorkForm">
			<s:hidden name="registerInfo.id" value="%{registerInfo.id}" />
			<div class="MainDiv">
				<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
					<tr>
						<td class="CPanel">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
								<tr>
									<td width="100%">
										<fieldset style="height: 100%;">
											<legend>添加备注</legend>
											<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right">备注:</td>
													<td><input name="remork" id="remork" class="text" value="<s:property value="registerInfo.customerDescribe"/>" size="30" onfocus="cleanInfo()" /></td>
												</tr>
												<tr>
													<td align="right" ></td>
													<td align="center" style="color: red;"><div id="alertInfo"></div></td>
												</tr>
											</table>
											<br />
										</fieldset>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" height="50px">
							<input type="submit" value="保存" class="button" />
						</td>
					</tr>
				</table>
			</div>
		</s:form>
		
	</s:if>
	<s:elseif test="type == 'importance'">
		<s:form id="importanceForm">
			<s:hidden name="registerInfo.id" value="%{registerInfo.id}" />
			<s:hidden name="importance" id="importance"/>
			<div class="MainDiv">
				<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
					<tr>
						<td class="CPanel">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
								<tr>
									<td width="100%">
										<fieldset style="height: 100%;">
											<legend>修改关注度</legend>
											<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right">关注度:</td>
													<td>
														<p class="starWrapper" onmouseover="rate(this,event)"> 
          												<img src="../page/images/noselect.gif" title="很烂" /><img src="../page/images/noselect.gif" title="一般" /><img src="../page/images/noselect.gif" title="还好" /><img src="../page/images/noselect.gif" title="较好" /><img src="../page/images/noselect.gif" title="很好" /> 
													</td>
												</tr>
												<tr>
													<td align="right" ></td>
													<td align="center" style="color: red;"><div id="alertInfo"></div></td>
												</tr>
											</table>
											<br />
										</fieldset>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" height="50px">
							<input type="submit" value="保存" class="button" />
						</td>
					</tr>
				</table>
			</div>
		</s:form>
	</s:elseif>
	
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/page-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
body {
	margin-top: 0px;
	margin-bottom: 0px;
}
</style>
</head>
<body>
	<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
		</tr>
		<tr>
			<td height="33">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
					<tr>
						<td width="50%">共 <span class="right-text09"> <s:property value="page.total"/></span> 页 | 第 <span class="right-text09"><s:property value="page.cpage"/></span> 页
						</td>
						<td width="49%" align="right">
							<pm:pages currentPage="%{page.cpage}" total="%{page.total}" url="%{page.url}" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
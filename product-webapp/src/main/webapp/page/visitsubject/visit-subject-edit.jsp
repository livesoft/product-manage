<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	$(function(){
		
		$("#visitSubjectForm").submit(function(){
			var subject = $("#subject").val();
			var content = $("#content").val();
			if (subject == "" )
			{
				$("#alertInfo").text("*请填写主题");
				return false;
			}
			else if (content == "")
			{
				$("#alertInfo").text("*请填写内容");
				return false;
			}
			else if ($("#alertInfo").val() != "")
			{
				return false;
			}
			
			$.ajax({
				url: "visit-subject-submit.action",
				cache: false,
				async: false,
				data: $("#visitSubjectForm").serialize(),
				success:function(){
					parent.closeWin("添加成功");
				}
			});
			return false;
		});
		
	});
	
</script>
</head>
<body class="ContentBody">
	
	<s:form id="visitSubjectForm">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加回访主题</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">主题:</td>
												<td width="41%"><input id="subject" name="visitSubject.subject" class="text" type="text" size="53" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" height="120px">内容:</td>
												<td colspan="3"><textarea name="visitSubject.content" id="content" rows="8" cols="50" onfocus="cleanInfo()"></textarea><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">状态:</td>
												<td width="41%">
													<s:select list="#{'true':'激活','false':'禁用'}" listKey="key" listValue="value" name="visitSubject.state" theme="simple"/><span class="red">*</span>
												</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="role.id" />
		<s:hidden name="role.version" />
		
	</s:form>
	
</body>
</html>
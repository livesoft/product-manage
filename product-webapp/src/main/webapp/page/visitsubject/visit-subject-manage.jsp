<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib prefix="pm" uri="/authority-tags"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	function removeSubject(id) 
	{
		$.layer({
			 v_button:2,
			 v_istitle:false,
			 v_msg:'<span style="font-size: 20px;color: #CC0000;">确定要删除该主题？</span>',
			 yes:function(){
				$.post("visit-subject-remove.action",{"visitSubject.id":id},
					function(data) {
						if (data.result == 1) {
							closeWinRE("该主题已被引用");
						} else {
							closeWin("删除成功");
						}
				}, "json");
			 },
			 no:function(){
				 layer_close(true);
			 },
			 v_btn:['删除','取消']
		});
	}
	
	function stateSubject(id,state)
	{
		$.post("visit-subject-state.action",{"visitSubject.id":id, "state":state},
				function(data) {
					promptRedirect("操作成功");
			}, "json");
	}
	
	function addSubject() {
		var url = "visit-subject-edit.action";
		popupNewWin(url,"700px","400px");
	}
	
</script>

</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<pm:authority name="productSetting:subject:add">
										<td width="40" align="left"><input type="button" class="button" value="添加回访主题" onclick="addSubject();"></td>
									</pm:authority>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="4" align="center" bgcolor="#EEEEEE" class="tablestyle_title">主题列表</td>
                    				  </tr>
					                  <tr>
					                  	<td width="3%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="37%" align="center" bgcolor="#EEEEEE">主题</td>
					                    <td width="55%" align="center" bgcolor="#EEEEEE">内容</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">管理</td>
					                  </tr>
					                  <s:if test="visitSubjects.size() > 0">
						                  <s:iterator value="visitSubjects">
											  <tr>
											  	<td bgcolor="#FFFFFF">
													<div align="center">
														<s:if test="isState">
															<img src="../page/images/effective.png" title="激活"/>
														</s:if>
														<s:else>
															<img src="../page/images/invalid.png" title="禁用"/>
														</s:else>												
													</div>
												</td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="subject"/></div></td>
												<td bgcolor="#FFFFFF"><s:property value="content" escape="false"/></td>
							                    <td bgcolor="#FFFFFF">
							                    	<div align="center">
							                    		<pm:authority name="productSetting:subject:state">
								                    		<s:if test="isState">
								                    			<s:a href="javascript:stateSubject(%{id},%{isState})"><img src="../page/images/system/lockTask.png" title="禁用主题"/></s:a>
								                    		</s:if>
								                    		<s:else>
								                    			<s:a href="javascript:stateSubject(%{id},%{isState})"><img src="../page/images/system/unlockTask.png" title="激活主题"/></s:a>
								                    		</s:else>
							                    		</pm:authority>
							                    		<pm:authority name="productSetting:subject:remove">
							                    			<s:a href="javascript:removeSubject(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
							                    		</pm:authority>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="4">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
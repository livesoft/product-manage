<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<pm:authority name="productMgmt:mobile:release">
								<input type="button" value="发布客户端" class="button" onclick="document.location.href='remote-client-edit.action'" />
							</pm:authority>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">手机客户端列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">产品名称</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">客户端类型</td>
					                    <td width="40%" align="center" bgcolor="#EEEEEE">发布描述</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">版本号</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">附件</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="productMobileClients.size() > 0">
						                  <s:iterator value="productMobileClients">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="category"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="clientDescribe"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="clientVersion"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    <s:iterator value="attachments">
							                    	<s:a href="javascript:downloadFile(%{id})"><s:property value="fileName"/></s:a>&nbsp;&nbsp;&nbsp;<s:property value="size"/>KB
							                    </s:iterator>
							                    </div></td>
							                    <td bgcolor="#FFFFFF">
							                    	<div align="center">
							                    		<pm:authority name="productMgmt:mobile:update">
							                    			<s:a href="remote-client-update.action?productMobileClient.id=%{id}">更新</s:a>
							                    		</pm:authority>
							                    		<pm:authority name="productMgmt:mobile:remove">
							                    			<s:a href="">删除</s:a>
							                    		</pm:authority>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="7">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<link href="../page/style/uploadify/upload.css" rel="stylesheet" type="text/css" />
<script src="../page/js/uploadify/swfobject.js" type="text/javascript"></script>
<script src="../page/js/uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$("#file_upload").uploadify({
	        'uploader': '../page/swf/uploadify/uploadify.swf',
	        'script': 'remote-client-submit.action',
	        'method' : 'POST',
	        'cancelImg': '../page/images/uploadify/cancel.png',
	        'auto': false,
	        'multi': false,
	        'fileDataName':'files', 
	        'queueID': 'fileQueue',
	        'buttonText':'Select File',
	        'onComplete' : function(event, ID, fileObj, response, data) {
	        	document.location.href = 'remote-client-manage.action';
	         }, 
	        'onError' : function(event, queueID, fileObj,errorObj){
	        	alert(errorObj.type + "Error:" + errorObj.info);
	        }
	    });
	});

	function checkForm()
	{
		var productId = $("#productId").val();
		var category = $("#category").val();
		var describe = $("#describe").val();
		var version = $("#version").val();
		
		if (productId == "")
		{
			$("#alertInfo").text("*请选择产品");
			return false;
		}
		if (category == "")
		{
			$("#alertInfo").text("*请选择客户端类型");
			return false;
		}
		if (version == "")
		{
			$("#alertInfo").text("*请填写版本号");
			return false;
		}
		if ($("#fileQueue").text() == "")
		{
			$("#alertInfo").text("*请选择客户端文件");
			return false;			
		}
		else
		{
			$('#file_upload').uploadifySettings('scriptData',{'productMobileClient.product.id':productId,'productMobileClient.category':category,'productMobileClient.clientDescribe':describe,'productMobileClient.clientVersion':version});
			$('#file_upload').uploadifyUpload();
		}
		
		return true;
	}
		
</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">发布客户端页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="button" value="发布" class="button" onclick="checkForm();"/>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>发布客户端</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">产品:</td>
												<td width="41%"><s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">客户端类型:</td>
												<td width="41%"><s:select list="categories" id="category" emptyOption="true" theme="simple" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">发布描述:</td>
												<td width="41%"><textarea id="describe" rows="5" cols="80" onfocus="cleanInfo()"></textarea></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">版本号:</td>
												<td width="41%"><input id="version" class="text" type="text" size="5" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">附件:</td>
												<td><div id="fileQueue"></div><s:file name="files" id="file_upload" theme="simple"/></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="button" value="发布" class="button" onclick="checkForm();"/>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>
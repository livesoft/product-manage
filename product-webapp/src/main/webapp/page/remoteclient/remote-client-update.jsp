<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<link href="../page/style/uploadify/upload.css" rel="stylesheet" type="text/css" />
<script src="../page/js/uploadify/swfobject.js" type="text/javascript"></script>
<script src="../page/js/uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$("#file_upload").uploadify({
	        'uploader': '../page/swf/uploadify/uploadify.swf',
	        'script': 'remote-client-update-submit.action',
	        'method' : 'POST',
	        'cancelImg': '../page/images/uploadify/cancel.png',
	        'auto': false,
	        'multi': false,
	        'fileDataName':'files', 
	        'queueID': 'fileQueue',
	        'buttonText':'Select File',
	        'onComplete' : function(event, ID, fileObj, response, data) {
	        	document.location.href = 'remote-client-manage.action';
	         }, 
	        'onError' : function(event, queueID, fileObj,errorObj){
	        	alert(errorObj.type + "Error:" + errorObj.info);
	        }
	    });
	});

	function checkForm()
	{
		var describe = $("#describe").val();
		var version = $("#version").val();
		var prcId = $("#prcId").val();
		
		if (version == "")
		{
			$("#alertInfo").text("*请填写版本号");
			return false;
		}
		if ($("#fileQueue").text() == "")
		{
			$("#alertInfo").text("*请选择客户端文件");
			return false;			
		}
		else
		{
			$('#file_upload').uploadifySettings('scriptData',{'productMobileClient.id':prcId,'clientDescribe':describe,'clientVersion':version});
			$('#file_upload').uploadifyUpload();
		}
		
		return true;
	}
		
</script>
</head>
<body>
		<s:include value="../right_top.jsp" />
		<s:hidden id="prcId" value="%{productMobileClient.id}"/>
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">更新客户端页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="button" value="更新" class="button" onclick="checkForm();"/>
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>更新客户端</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">发布描述:</td>
												<td width="41%"><textarea id="describe" rows="5" cols="80" onfocus="cleanInfo()"></textarea></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">版本号:</td>
												<td width="41%"><input id="version" class="text" type="text" size="5" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">附件:</td>
												<td><div id="fileQueue"></div><s:file name="files" id="file_upload" theme="simple"/></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="button" value="更新" class="button" onclick="checkForm();"/>
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
</body>
</html>
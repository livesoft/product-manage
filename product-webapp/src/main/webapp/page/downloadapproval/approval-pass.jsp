<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		$("#approvalForm").submit(function(){
			
			var validity = $("#validity").val();
			if (validity == "")
			{
				$("#alertInfo").text("请输入有效期");
				return false;
			}
			
			$.ajax({
				url: "approval-pass-submit.action",
				cache: false,
				async: false,
				data: $("#approvalForm").serialize(),
				success:function(){
					parent.closeWin("审批通过");
				}
			});
			
			return false;
		});
	});
	
</script>

</head>
<body class="ContentBody">

	<s:form id="approvalForm">
		<s:hidden name="approval.id" value="%{approval.id}" />
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>审批通过</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right">有效期天数:</td>
												<td><input name="validity" id="validity" class="text" size="10" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo();"/>天<span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">试用期天数:</td>
												<td><input name="trialPeriod" type="text" size="10" class="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo()" />天</td>
											</tr>
											<tr>
												<td nowrap align="right"></td>
												<td style="color: red;">不填默认为产品的试用天数:<s:property value="approval.product.trialPeriod"/>天</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
		
</body>
</html>

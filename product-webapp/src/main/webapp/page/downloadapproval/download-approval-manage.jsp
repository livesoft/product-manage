<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	function checkClick() 
	{
		var productId = $("#productId").val();
		var category = $("#category").val();
		
		 if (productId != "" && category != "")
		{
			document.location.href = "download-approval-manage.action?product.id=" + productId + "&category=" + importance;
		}
		else if (productId != "")
		{
			document.location.href = "download-approval-manage.action?product.id=" + productId;
		}
		else if (category != "")
		{
			document.location.href = "download-approval-manage.action?category=" + category;
		}
		else
		{
			document.location.href = 'download-approval-manage.action';
		} 
		
	}
	
	function approvalPass(id)
	{
		var url = 'approval-pass.action?approval.id=' + id;
		popupNewWin(url,"380px","250px");
	}
	
	function approvalReject(id)
	{
		var url = 'approval-reject.action?approval.id=' + id;
		popupNewWin(url,"400px","280px");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										状态:<s:select list="categorys" listKey="key" emptyOption="true" listValue="value" theme="simple" id="category" />
										产品:<s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">申请试用审批列表</td>
                    				  </tr>
					                  <tr>
					                  	<td width="13%" align="center" bgcolor="#EEEEEE">试用产品</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">公司名称</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">公司电话</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">联系人</td>
					                    <td width="9%" align="center" bgcolor="#EEEEEE">手机</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">邮箱</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">申请时间</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">审批</td>
					                  </tr>
					                  <s:if test="approvals.size() > 0">
						                  <s:iterator value="approvals">
											  <tr>
											  	<td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.phone"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.contact"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.mobile"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="downloadInfo.email"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="downloadInfo.applyDate"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:text name="%{category}"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="productMgmt:approval:approval">
							                    		<s:a href="javascript:approvalPass(%{id})">通过</s:a>&nbsp;<s:a href="javascript:approvalReject(%{id})">不通过</s:a>
							                    	</pm:authority>
							                    </div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="9">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>

</body>
</html>
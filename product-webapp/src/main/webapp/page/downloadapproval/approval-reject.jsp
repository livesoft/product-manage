<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		$("#approvalForm").submit(function(){
			
			var textarea = $("#textarea").val();
			if (textarea == "")
			{
				$("#alertInfo").text("请输入原因");
				return false;
			}
			
			$.ajax({
				url: "approval-reject-submit.action",
				cache: false,
				async: false,
				data: $("#approvalForm").serialize(),
				success:function(){
					parent.closeWin("审批不通过");
				}
			});
			
			return false;
		});
	});
	
</script>

</head>
<body class="ContentBody">

	<s:form id="approvalForm">
		<s:hidden name="approval.id" value="%{approval.id}" />
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>审批不通过</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right">原因:</td>
												<td colspan="3"><textarea id="textarea" name="reasonText" rows="3" cols="25" onfocus="cleanInfo()"></textarea></td>
											</tr>
											<tr>
												<td nowrap align="right"></td>
												<td colspan="3"><input type="checkbox" name="sendMail" value="Send" />是否发邮件给客户</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
		
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	$(function(){
		
		$("#roleSub").submit(function(){
			var name = $("#name").val();
			if (name == "" )
			{
				$("#alertInfo").text("*请填写角色名称");
				return false;
			}
			else if ($("#alertInfo").val() != "")
			{
				return false;
			}
			
			$.ajax({
				url: "role-submit.action",
				cache: false,
				async: false,
				data: $("#roleSub").serialize(),
				success:function(){
					parent.closeWin("添加成功");
				}
			});
			return false;
		});
		
		$("#name").blur(function(){
			var name = $("#name").val();
			if (name != "")
			{
				$.post("role-name-exist.action",{"roleName":name},
					function(data) {
						if (data.result == 1) {
							$("#name").val("");
							$("#alertInfo").text("*该角色名称已存在");
						}
				}, "json");				
			}
		});
		
	});
	
</script>
</head>
<body class="ContentBody">
	
	<s:form action="role-submit.action" id="roleSub">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加角色</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">角色名称:</td>
												<td width="41%"><input value="<s:property value="role.name"/>" id="name" name="role.name" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" height="120px">角色描述:</td>
												<td colspan="3"><textarea name="role.roleDescribe" rows="5" cols="50" onfocus="cleanInfo()"><s:property value="role.roleDescribe"/></textarea></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">状态:</td>
												<td width="41%">
													<s:select list="mapStates" listKey="key" listValue="value" name="role.state" theme="simple"/>
												</td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="role.id" />
		<s:hidden name="role.version" />
		
	</s:form>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/ztree/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/ztree/jquery.ztree.core-3.2.js" type="text/javascript"></script>
<script src="../page/js/ztree/jquery.ztree.excheck-3.2.js" type="text/javascript"></script>

<script type="text/javascript">
	
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
	
		var code;
		
		function setCheck() {
			var type = { "Y" : "ps", "N" : "ps" };
			setting.check.chkboxType = type;
			showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
		}
		
		function showCode(str) {
			if (!code) code = $("#code");
			code.empty();
			code.append("<li>"+str+"</li>");
		}
		
		$(document).ready(function() {
			var roleId = $("#roleId").val();
			 $.post("role-permission-json-data.action",{"role.id" : roleId},
					function(data) {
				 		$.fn.zTree.init($("#treeDemo"), setting, data.results);
				}, "json"); 
			setCheck();
			$("#py").bind("change", setCheck);
			$("#sy").bind("change", setCheck);
			$("#pn").bind("change", setCheck);
			$("#sn").bind("change", setCheck);
		});
		
		function saveBtn()
		{
			var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
			var nodes = treeObj.getCheckedNodes(true);
			if (nodes.length > 0) {
				var permissions = "";
				for (var i = 0; i < nodes.length; i++) { 
					permissions += nodes[i].id + ":";
	      		} 
				var roleId = $("#roleId").val();
				$.post("role-accredit-submit.action",{"permissions":permissions, "role.id":roleId},
						function(data) {
							if (data.result == 1) {
								parent.closeWinRE("授权成功");
							} else {
								parent.closeWinRE("授权失败");
							}
					}, "json");
			} else {
				promptInfo("请选择权限");
			}
			
		}
		
</script>


</head>
<body>
	<s:hidden id="roleId" value="%{role.id}" />
	<div align="center" style="font-size: 18px;color: #003399;font-weight: bold;">角色授权</div>
	<input type="submit" value="保存" class="button" onclick="saveBtn();"/>
	<div class="zTreeDemoBackground left">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
	<input type="submit" value="保存" class="button" onclick="saveBtn();"/>
	
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<%@ taglib prefix="pm" uri="/authority-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

	$(function(){
		$("#addRole").click(function(){
			editRole(0);
		});
		
	});

	function editRole(roleId)
	{
		var url;
		if (roleId == 0 ) {
			url = 'role-edit.action';
		} else {
			url = 'role-edit.action?role.id=' + roleId;
		}
		popupNewWin(url,"700px","380px");
	}

	function removeRole(id) 
	{
		$.layer({
			 v_button:2,
			 v_istitle:false,
			 v_msg:'<span style="font-size: 20px;color: #CC0000;">确定要删除该角色？</span>',
			 yes:function(){
				$.post("role-remove.action",{"role.id":id},
					function(data) {
						if (data.result == 1) {
							closeWinRE("该角色已被引用");
						} else {
							closeWin("删除成功");
						}
				}, "json");
			 },
			 no:function(){
				 layer_close(true);
			 },
			 v_btn:['删除','取消']
		});
	}
	
	function checkClick()
	{
		var roleName = $("#roleName").val();
		if (roleName != "")
		{
			document.location.href = 'role-manage.action?roleName=' + roleName;
		}
		else
		{
			document.location.href = 'role-manage.action';
		}
	}
	
	function rolePermission(id)
	{
		var url = 'role-accredit.action?role.id=' + id;
		popupNewWin(url,"250px","400px");
	}
	
</script>

</head>
<body>
	<s:include value="../right_top.jsp" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										角色名称:<input name="textfield" type="text" size="12" id="roleName" />
										<input name="submit" type="button" class="query-button" value="查 询" onclick="checkClick();"/>
										<pm:authority name="systemSetting:role:add">
											<input id="addRole" type="button" class="button" value="添加角色" />
										</pm:authority>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="4" align="center" bgcolor="#EEEEEE" class="tablestyle_title">角色列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">角色名称</td>
					                    <td width="55%" align="center" bgcolor="#EEEEEE">角色描述</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">管理</td>
					                  </tr>
					                  <s:if test="roles.size() > 0">
						                  <s:iterator value="roles">
											  <tr>
												<td bgcolor="#FFFFFF">
													<div align="center">
														<s:if test="isState">
															<img src="../page/images/effective.png" title="激活"/>
														</s:if>
														<s:else>
															<img src="../page/images/invalid.png" title="禁用"/>
														</s:else>												
													</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="roleDescribe"/></div></td>
							                    <td bgcolor="#FFFFFF">
							                    	<s:if test="id != 1">
								                    	<div align="center">
								                    		<pm:authority name="systemSetting:role:remove">
								                    			<s:a href="javascript:removeRole(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
								                    		</pm:authority>
								                    		<pm:authority name="systemSetting:role:add">
								                    			<s:a href="javascript:editRole(%{id})"><img src="../page/images/system/edit.gif" title="编辑"/></s:a>
								                    		</pm:authority>
								                    		<pm:authority name="systemSetting:role:accredit">
								                    			<s:a href="javascript:rolePermission(%{id})"><img src="../page/images/system/grant.gif" title="授权"/></s:a>
								                    		</pm:authority>
								                    	</div>
							                    	</s:if>
							                    	<s:else>
							                    		<div align="center">
								                    		(无权限管理)
								                    	</div>
							                    	</s:else>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="4">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
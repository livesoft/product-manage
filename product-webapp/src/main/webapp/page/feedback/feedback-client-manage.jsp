<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
function checkClick() 
{
	var productId = $("#productId").val();
	var category = $("#category").val();
	if (productId != "" && category != "")
	{
		document.location.href = "feedback-client-manage.action?product.id=" + productId + "&category=" + category;
	}
	else if (productId != "")
	{
		document.location.href = "feedback-client-manage.action?product.id=" + productId;
	}
	else if (category != "")
	{
		document.location.href = "feedback-client-manage.action?category=" + category;
	}
	else
	{
		document.location.href = "feedback-client-manage.action";
	}
	
}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										产品:<s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple"/>
										客户端类型:<s:select list="categories" id="category" emptyOption="true" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">反馈信息列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">反馈人</td>
					                    <td width="60%" align="center" bgcolor="#EEEEEE">反馈内容</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">客户端类型</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">产品名称</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">反馈时间</td>
					                  </tr>
					                  <s:if test="feedbackClients.size() > 0">
						                  <s:iterator value="feedbackClients">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="feedbackPerson"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="feedbackContent"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="category"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="feedbackDate"/></div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="7">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
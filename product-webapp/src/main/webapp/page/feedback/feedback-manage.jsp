<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
function checkClick() 
{
	var productId = $("#productId").val();
	if (productId != "")
	{
		document.location.href = "feedback-manage.action?product.id=" + productId;
	}
	else
	{
		document.location.href = 'feedback-manage.action';
	}
	
}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										产品:<s:select list="products" id="productId" listKey="id" emptyOption="true" listValue="name" theme="simple"/>
										<input type="button" class="query-button" value="查 询" onclick="checkClick();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="8" align="center" bgcolor="#EEEEEE"class="tablestyle_title">反馈信息列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">公司名称</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">是否有帮助</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">是否简单易用</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">是否美观</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">是否经常出现问题</td>
					                   	<td width="8%" align="center" bgcolor="#EEEEEE"> 整体评价</td>
					                    <td width="40%" align="center" bgcolor="#EEEEEE">建议</td>
					                    <td width="5%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="feedbackWebs.size() > 0">
						                  <s:iterator value="feedbackWebs">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:if test="applicability.toString() == 'GOOD'">较适用</s:if>
							                    	<s:else><s:text name="%{applicability}"/></s:else>
							                    </div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:if test="usability.toString() == 'GOOD'">较易用</s:if>
							                    	<s:else><s:text name="%{usability}"/></s:else>
							                    </div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:text name="%{capability}"/>
							                    </div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="assessment"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="feedbackMgmt:pc:view">
							                    		<s:a href="feedback-detail.action?feedbackWeb.id=%{id}">详情</s:a>&nbsp;
							                    	</pm:authority>
							                    	<pm:authority name="feedbackMgmt:pc:remove">
							                    		<s:a href="">删除</s:a>
							                    	</pm:authority>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="8">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
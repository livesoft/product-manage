<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	function testEmailConnect(id)
	{
		$.post("email-connect-test.action",
				{"emailInfo.id" : id},
				function(data){
					alert(data.result);
					document.location.href = 'email-manage.action';
				},"json"
		);
	}
	
	function removeEmail(id)
	{
		$.post("email-remove.action",
				{"emailInfo.id" : id},
				function(data){
					if (data.result == 1)
					{
						promptInfo("该邮箱已被引用");
					}
					else
					{
						promptRedirect("删除成功");
					}
				},"json"
		);
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<pm:authority name="systemSetting:email:add">
								<input type="button" value="新增邮箱" class="button" onclick="document.location.href='email-edit.action'" />
							</pm:authority>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="7" align="center" bgcolor="#EEEEEE"class="tablestyle_title">邮箱列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="17%" align="center" bgcolor="#EEEEEE">用户名</td>
					                    <td width="17%" align="center" bgcolor="#EEEEEE">密码</td>
					                    <td width="16%" align="center" bgcolor="#EEEEEE">smtp服务器</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">smtp端口</td>
					                    <td width="20%" align="center" bgcolor="#EEEEEE">名称</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">是否可用</td>
					                    <td width="8%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="emailInfos.size() > 0">
						                  <s:iterator value="emailInfos">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="username"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="password"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="smtpName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="smtpPort"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:if test="isUsable">是</s:if><s:else>否</s:else></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<s:if test="!isUsable">
							                    		<pm:authority name="systemSetting:email:test">
							                    			<s:a href="javascript:testEmailConnect(%{id});"><img src="../page/images/system/test_email.png" title="测试邮箱"/></s:a>&nbsp;
							                    		</pm:authority>
							                    	</s:if>
							                    	<pm:authority name="systemSetting:email:add">
							                    		<s:a href="email-edit.action?emailInfo.id=%{id}"><img src="../page/images/system/edit.gif" title="编辑"/></s:a>&nbsp;
							                    	</pm:authority>
							                    	<s:if test="remark != 'DefaultEmail'">
								                    	<pm:authority name="systemSetting:email:remove">
								                    		<s:a href="javascript:removeEmail(%{id})"><img src="../page/images/system/remove.png" title="删除"/></s:a>
								                    	</pm:authority>
							                    	</s:if>
							                    	</div>
							                    </td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="7">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
	
</body>
</html>
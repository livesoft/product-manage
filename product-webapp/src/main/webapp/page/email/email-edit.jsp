<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		$("#emailSub").submit(function(){
			var username = $("#username").val();
			var password = $("#password").val();
			var smtpName = $("#smtpName").val();
			var smtpPort = $("#smtpPort").val();
			if (username == "" || password == "" || smtpName == "" || smtpPort == "" )
			{
				$("#alertInfo").text("*请填写完整的信息");
				return false;
			}
			return true;
		});
	});
	
	function cleanInfo()
	{
		$("#alertInfo").text("");
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<s:form action="email-submit.action" id="emailSub">
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">
						<s:if test="emailInfo != null">
							编辑邮箱页面
						</s:if>
						<s:else>
							新增邮箱页面
						</s:else>
					</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>
											<s:if test="emailInfo != null">
												编辑邮箱
											</s:if>
											<s:else>
												添加邮箱
											</s:else>			
										</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">用户名:</td>
												<td width="41%"><input name="emailInfo.username" id="username" value="<s:property value="emailInfo.username"/>" class="text" type="text" size="30" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">密码:</td>
												<td><input name="emailInfo.password" id="password" class="text" value="<s:property value="emailInfo.password"/>" size="20" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">smtp服务器:</td>
												<td><input name="emailInfo.smtpName" id="smtpName" class="text" value="<s:property value="emailInfo.smtpName"/>" size="20" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">smtp端口:</td>
												<td colspan="3"><input name="emailInfo.smtpPort" id="smtpPort" class="text" value="<s:property value="emailInfo.smtpPort"/>" size="10" onfocus="cleanInfo()" onkeyup="this.value=this.value.replace(/\D/g,'')"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">用户名名称:</td>
												<td colspan="3"><input name="emailInfo.name" id="name" class="text" value="<s:property value="emailInfo.name"/>" size="30" onfocus="cleanInfo()" /></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="emailInfo.id" />
		<s:hidden name="emailInfo.version" />
		<s:hidden name="emailInfo.remark"/>
	</s:form>
	
</body>
</html>
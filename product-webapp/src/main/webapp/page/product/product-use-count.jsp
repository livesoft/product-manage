<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										<input name="textfield" type="text" size="12" />
										<span class="newfont06">至</span> 
										<input name="textfield" type="text" size="12" />
										<input name="submit" type="button" class="query-button" value="查 询" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
						</td>
					</tr>
				</table>
			</td>
		</tr>	
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">统计次数列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">使用时间</td>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">IP</td>
					                    <td width="40%" align="center" bgcolor="#EEEEEE">地址</td>
					                  </tr>
					                  <s:if test="registerCode.counts.size() > 0">
						                  <s:iterator value="registerCode.counts">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:date name="useDate" /></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="ip"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="address"/></div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="6">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
			<tr>
				<td>
					<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
						<tr>
							<td height="35">
								<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
      </table>
      
      </td>
  </tr>
</table>
	
</body>
</html>
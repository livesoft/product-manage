<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<link type="text/css" href="../page/style/datepicker/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../page/style/datepicker/style.css" rel="stylesheet" />
<script type="text/javascript" src="../page/js/datepicker/ui.core.js"></script>
<script type="text/javascript" src="../page/js/datepicker/ui.datepicker.js"></script>
<script type="text/javascript" src="../page/js/datepicker/ui.datepicker-zh_CN.js"></script>

<script type="text/javascript">
	
	$(function(){
		
		$("#vrForm").submit(function(){
			var subjectId = $("#subjectId").val();
			var result = $("#result").val();
			var employDate = $("#employDate").val();
			if (subjectId == "" )
			{
				$("#alertInfo").text("*请选择回访主题");
				return false;
			}
			else if (result == "")
			{
				$("#alertInfo").text("*请填写回访结果");
				return false;
			}
			else if (employDate == "")
			{
				$("#alertInfo").text("*请选择回访日期");
				return false;
			}
			
			$.ajax({
				url: "visit-record-submit.action",
				cache: false,
				async: false,
				data: $("#vrForm").serialize(),
				success:function(){
					parent.closeWin("记录成功");
				}
			});
			return false;
		});
		
		datePicker('#employDate','zh_CN');
		
	});
	
	function datePicker(pickerName,locale) {
		$(pickerName).datepicker($.datepicker.regional[locale]);
		$(pickerName).datepicker('option', 'changeMonth', true);//月份可调
		$(pickerName).datepicker('option', 'changeYear', true);//年份可调
	}
	
</script>
</head>
<body class="ContentBody">
	
	<s:form action="visit-record-submit.action" id="vrForm">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>记录回访结果</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">回访主题:</td>
												<td width="41%"><s:select list="visits" listKey="visitSubject.id" listValue="visitSubject.subject" theme="simple" id="subjectId" name="subjectId" /><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" height="120px">回访结果:</td>
												<td colspan="3"><textarea name="result" id="result" rows="8" cols="50" onfocus="cleanInfo()"></textarea><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" width="13%">回访时间:</td>
												<td width="41%"><input name="visitResult.visitDate" id="employDate" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
					</td>
				</tr>
			</table>
		</div>
		<s:hidden name="trialId" value="%{trialId}" />
		
	</s:form>
	
</body>
</html>
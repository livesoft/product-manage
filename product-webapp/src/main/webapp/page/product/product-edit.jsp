<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	$(function(){
		
		$("#productSub").submit(function(){
			var name = $("#name").val();
			var trialPeriod = $("#trialPeriod").val();
			var mark = $("#mark").val();
			
			if (name == "" || trialPeriod == "" || mark == "")
			{
				$("#alertInfo").text("*请填写信息");
				return false;
			}
			else if ($("#alertInfo").val() != "")
			{
				return false;
			}
			checkMark();
			return true;
		});
		
		$("#mark").blur(function(){
			checkMark();
		});
		
		function checkMark()
		{
			var mark = $("#mark").val();
			if (mark != "")
			{
				$.post("product-mark-check.action",
						{"mark" : mark},
						function(data){
							if (data.result == 1)
							{
								$("#alertInfo").text("*该标识已被使用");
							}
						},"json"
				);
			}
		}
		
	});
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<s:form action="product-submit.action" id="productSub" >
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">新增产品页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<tr>
								<td align="left">
									<input type="submit" value="保存" class="button" />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
							<tr>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>添加产品</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr>
												<td nowrap align="right" width="13%">产品名称:</td>
												<td width="41%"><input name="product.name" id="name" class="text" style="width: 250px" type="text" size="40" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">试用期天数:</td>
												<td><input name="product.trialPeriod" class="text" id="trialPeriod" size="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onfocus="cleanInfo()"/>天<span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right">产品标识:</td>
												<td><input name="product.mark" id="mark" class="text" size="10" onfocus="cleanInfo()"/><span class="red">*</span></td>
											</tr>
											<tr>
												<td nowrap align="right" height="120px">产品描述:</td>
												<td colspan="3"><textarea id="productDescribe" name="product.productDescribe" rows="5" cols="80" onfocus="cleanInfo()"></textarea></td>
											</tr>
											<tr>
												<td align="right" ></td>
												<td align="center" style="color: red;"><div id="alertInfo"></div></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" />
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
	</s:form>
	
</body>
</html>
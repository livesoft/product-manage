<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<%@ taglib prefix="pm" uri="/authority-tags"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/layer/layer.js" type="text/javascript"></script>
<script src="../page/js/toast.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>

<script type="text/javascript">
	
	function checkFor()
	{
		var status = $("#status").val();
		var productId = $("#product").val();
		if (status != "" && productId != "")
		{
			document.location.href = "product-use-manage.action?product.id=" + productId + "&status=" + status;
		} 
		else if (productId != "")
		{
			document.location.href = "product-use-manage.action?product.id=" + productId;
		}
		else if (status != "")
		{
			document.location.href = "product-use-manage.action?status=" + status;
		}
	}
	
	function customerVisit(id)
	{
		$.post("check-customer-visit.action",
				{"trialId" : id},
				function(data){
					if (data.result == 1)
					{
						var url = "visit-record-edit.action?trialId=" + id;
						popupNewWin(url,"700px","450px");
					}
					else
					{
						promptInfo("没有要填写的回访记录");
					}
				},"json"
		);
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="40" align="right"><img src="../page/images/ico07.gif" width="20" height="18" /></td>
									<td>
										&nbsp;状态:
										<s:select list="statusMap" listKey="key" emptyOption="true" listValue="value" theme="simple" id="status" />
										&nbsp;产品:
										<s:select list="products" listKey="id" listValue="name" emptyOption="true" theme="simple" id="product" />
										<input name="submit" type="button" class="query-button" value="查 询" onclick="checkFor();"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="7" align="center" bgcolor="#EEEEEE" class="tablestyle_title">产品使用情况</td>
                    				  </tr>
					                  <tr>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">客户名称</td>
					                    <td width="20%" align="center" bgcolor="#EEEEEE">产品名称</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">状态</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">剩余天数</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">使用次数</td>
					                    <td width="12%" align="center" bgcolor="#EEEEEE">回访情况</td>
					                    <pm:authority name="productMgmt:user:visit">
					                    	<td width="4%" align="center" bgcolor="#EEEEEE">操作</td>
					                    </pm:authority>
					                  </tr>
					                  <s:if test="productTrials.size() > 0">
						                  <s:iterator value="productTrials" id="prod">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:a href="product-details.action?productTrial.id=%{id}"><s:property value="customerInfo.companyName"/></s:a></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:text name="%{status}"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="registerCode.trialDay"/>天</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="registerCode.counts.size()"/>次</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="@com.dayatang.product.webapp.action.product.ProductUseManageAction@getVisitAmount(#prod)"/>次</div></td>
							                    <pm:authority name="productMgmt:user:visit">
							                    	<td bgcolor="#FFFFFF"><div align="center"><s:a href="javascript:customerVisit(%{id})"><img src="../page/images/system/taskDo.png" title="记录客户回访结果"/></s:a></div></td>
							                    </pm:authority>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="7">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
     	 </table>
     	 <s:include value="../page.jsp" />
      </td>
  </tr>
</table>
	
</body>
</html>
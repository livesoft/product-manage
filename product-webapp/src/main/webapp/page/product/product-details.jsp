<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="pm" uri="/authority-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	
	function registerCodeSend(id) {
		if (id > 0) {
			alert("发送中...请稍候查看");
			document.location.href="register-code-email-send-failure.action?registerInfo.id=" + id;
		}
	}
	
</script>
</head>
<body>
		<s:include value="../right_top.jsp"/>
		<div class="MainDiv">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" class="CContent" align="center">
				<tr>
					<th class="tablestyle_title">产品使用详情页面</th>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>使用详情</legend>
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">客户名称:</td>
												<td width="20%"><s:property value="productTrial.customerInfo.companyName"/></td>
												<td width="20%" nowrap align="center" >产品名称:</td>
												<td><s:property value="productTrial.product.name"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">试用号:</td>
												<td width="20%"><s:property value="productTrial.trialCode.trialNum"/></td>
												<td width="20%" nowrap align="center" >注册码:</td>
												<td><s:property value="productTrial.registerCode.registerCode"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">有效期:</td>
												<td width="20%"><s:property value="productTrial.trialCode.validity"/>天</td>
												<td width="20%" nowrap align="center" >试用期:</td>
												<td><s:property value="productTrial.trialPeriod"/>天</td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">有效期是否过期:</td>
												<td width="20%">
													<s:if test="productTrial.trialCode.isEffective">否</s:if>
													<s:else>是</s:else>
												</td>
												<td width="20%" nowrap align="center" >试用期是否结束:</td>
												<td>
													<s:if test="productTrial.registerCode.isOutDate">是</s:if>
													<s:else>否</s:else>
												</td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">试用号是否激活:</td>
												<td width="20%">
													<s:if test="productTrial.trialCode.isActivate">是</s:if>
													<s:else>否</s:else>
												</td>
												<td width="20%" nowrap align="center" >注册码是否激活:</td>
												<td>
													<s:if test="productTrial.registerCode.isActivate">是</s:if>
													<s:else>否</s:else>
												</td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">有效期剩余天数:</td>
												<td width="20%"><s:property value="productTrial.trialCode.validityDay"/>天</td>
												<td width="20%" nowrap align="center" >试用期剩余天数:</td>
												<td><s:property value="productTrial.registerCode.trialDay"/>天</td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">试用号激活时间:</td>
												<td width="20%">
													<s:if test="productTrial.trialCode.activateDate != null"><s:date name="productTrial.trialCode.activateDate"/></s:if>
													<s:else>暂未激活</s:else>
												</td>
												<td width="20%" nowrap align="center" >产品激活时间:</td>
												<td>
													<s:if test="productTrial.registerCode.activateDate != null"><s:date name="productTrial.registerCode.activateDate"/></s:if>
													<s:else>暂未激活</s:else>
												</td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">使用次数:</td>
												<td width="20%"><s:property value="productTrial.registerCode.counts.size()"/>次
												<pm:authority name="productMgmt:user:count">
													<input name="submit" type="button" class="button" value="查看详情" onclick="document.location.href='product-use-count.action?registerCode.id=<s:property value="productTrial.registerCode.id"/>'" />
												</pm:authority>
												</td>
												<td width="20%" nowrap align="center" >状态:</td>
												<td><s:text name="%{productTrial.status}"/></td>
											</tr>
										</table>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>注册信息</legend>
										<s:if test="registerInfo != null">
										<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">客户名称:</td>
												<td width="20%"><s:property value="registerInfo.companyName"/></td>
												<td width="20%" nowrap align="center" >电话:</td>
												<td><s:property value="registerInfo.phone"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">联系人:</td>
												<td width="20%"><s:property value="registerInfo.contact"/></td>
												<td width="20%" nowrap align="center" >手机:</td>
												<td><s:property value="registerInfo.mobile"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">邮箱:</td>
												<td width="20%"><s:property value="registerInfo.email"/></td>
												<td width="20%" nowrap align="center" >地址:</td>
												<td><s:property value="registerInfo.address"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">关注度:</td>
												<td width="20%">
													<s:if test="registerInfo.importance == 1">
							                    			<img src="../page/images/select.gif"/>
							                    		</s:if>
							                    		<s:elseif test="registerInfo.importance == 2">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="registerInfo.importance == 3">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="registerInfo.importance == 4">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
							                    		<s:elseif test="registerInfo.importance == 5">
							                    			<img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/><img src="../page/images/select.gif"/>
							                    		</s:elseif>
												</td>
												<td width="20%" nowrap align="center" >备注:</td>
												<td><s:property value="registerInfo.customerDescribe"/></td>
											</tr>
											<tr height="30px" align="center">
												<td nowrap align="center" width="20%">注册码邮件详情:</td>
												<td width="20%">
													<s:if test="registerInfo.isSend">
														发送成功
													</s:if>
													<s:else>
														发送失败<input name="submit" type="button" class="button" value="重新发送" onclick="registerCodeSend(<s:property value="registerInfo.id"/>)" />
													</s:else>
												</td>
												<s:if test="!registerInfo.isSend">
													<td width="20%" nowrap align="center" >失败原因:</td>
													<td><s:property value="registerInfo.error"/></td>
												</s:if>
											</tr>
										</table>
										</s:if>
										<s:else>
											暂未填写注册信息
										</s:else>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 80%" align="center">
							<tr>
								<td width="80%">
									<fieldset style="height: 100%;">
										<legend>回访记录</legend>
										<s:if test="visits.size() > 0">
											<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
							                  <tr>
							                    <td width="20%" align="center" bgcolor="#EEEEEE">回访主题</td>
							                    <td width="30%" align="center" bgcolor="#EEEEEE">回访内容</td>
							                    <td width="35%" align="center" bgcolor="#EEEEEE">回访结果</td>
							                    <td width="10%" align="center" bgcolor="#EEEEEE">回访时间</td>
							                    <td width="5%" align="center" bgcolor="#EEEEEE">经手人</td>
							                  </tr>
								                  <s:iterator value="visits">
													  <tr>
									                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="visitSubject.subject"/></div></td>
														<td bgcolor="#FFFFFF"><s:property value="visitSubject.content" escape="false"/></td>
									                    <td bgcolor="#FFFFFF"><s:property value="visitResult.result" escape="false"/></td>
									                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="visitResult.visitDate" format="yyyy-MM-dd"/></div></td>
									                  	<td bgcolor="#FFFFFF"><div align="center"><s:property value="visitResult.reportPerson" /></div></td>
									                  </tr>
								                  </s:iterator>
		                					</table>
										</s:if>
										<s:else>
											暂无回访记录
										</s:else>
										<br />
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="left">
						<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
					</td>
				</tr>
			</table>
		</div>
	
</body>
</html>
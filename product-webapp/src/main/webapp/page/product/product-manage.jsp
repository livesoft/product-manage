<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<%@ taglib prefix="pm" uri="/authority-tags"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function removeProduct(id)
	{
		$.post("product-remove.action",
				{"product.id" : id},
				function(data){
					if (data.result == 1)
					{
						alert("该产品已被引用");
					}
					else
					{
						document.location.href = 'product-manage.action';
					}
				},"json"
		);
	}
	
</script>
</head>
<body>
	<s:include value="../right_top.jsp"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<pm:authority name="productMgmt:list:edit">
								<input type="button" class="button" value="添加产品" onclick="document.location.href='product-edit.action'"/>
							</pm:authority>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
    			<table id="subtree1" width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="9" align="center" bgcolor="#EEEEEE"class="tablestyle_title">产品列表</td>
                    				  </tr>
					                  <tr>
					                    <td width="25%" align="center" bgcolor="#EEEEEE">产品名称</td>
					                    <td width="15%" align="center" bgcolor="#EEEEEE">发布时间</td>
					                    <td width="14%" align="center" bgcolor="#EEEEEE">试用期</td>
					                    <td width="6%" align="center" bgcolor="#EEEEEE">标识</td>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">描述</td>
					                    <td width="10" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="products.size() > 0">
						                  <s:iterator value="products">
											  <tr>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:date name="releaseDate" format="yyyy-MM-dd"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="trialPeriod"/>天</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="mark"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="productDescribe.substring(0,20)"/>......</div></td>
							                    <td bgcolor="#FFFFFF"><div align="center">
							                    	<pm:authority name="productMgmt:list:edit">
							                    		<s:a href="product-edit.action?product.id=%{id}">编辑</s:a> 
							                    	</pm:authority>
							                    	<pm:authority name="productMgmt:list:remove">
							                    		<s:a href="javascript:removeProduct(%{id})">删除</s:a>
							                    	</pm:authority>
							                    	</div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="6">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
      </table>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td height="6"><img src="../images/spacer.gif" width="1" height="1" /></td>
        </tr>
        <tr>
          <td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
              <tr>
                <td width="50%">共 <span class="right-text09">1</span> 页 | 第 <span class="right-text09">1</span> 页</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
</table>
	
</body>
</html>
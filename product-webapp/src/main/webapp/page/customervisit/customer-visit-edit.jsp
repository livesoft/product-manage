<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../page/style/content.css" rel="stylesheet" type="text/css" />
<script src="../page/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../page/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function checkFor()
	{
		var status = $("#status").val();
		var productId = $("#product").val();
		if (status != "" && productId != "")
		{
			document.location.href = "customer-visit-edit.action?product.id=" + productId + "&status=" + status;
		} 
		else if (productId != "")
		{
			document.location.href = "customer-visit-edit.action?product.id=" + productId;
		}
		else if (status != "")
		{
			document.location.href = "customer-visit-edit.action?status=" + status;
		} 
		else
		{
			document.location.href = "customer-visit-edit.action";
		}
	}
	
	function getDel(id){
		$("#"+id).remove();    
	}
	
	function selectSubject()
	{
		var trialLength = $("#trialLength").val();
		var subjectIndex = $("#subject").get(0).selectedIndex;
		if (trialLength > 0) {
			for (var i = 0; i < trialLength; i++) {
				if ($("#subjectId" + i).length > 0) {
					$("#subjectId" + i).get(0).selectedIndex = subjectIndex;
				}
			}
		}
	}
	
	$(function(){
		$("#cvForm").submit(function(){
			var title = $("#title").val();
			if (title == "")
			{
				$("#alertInfo").text("*请填写标题");
				return false;
			}
			var trialLength = $("#trialLength").val();
			if (trialLength > 0) {
				for (var i = 0; i < trialLength; i++) {
					if ($("#subjectId" + i).length > 0) {
						if ($("#subjectId" + i).get(0).selectedIndex == 0) {
							$("#alertInfo").text("*请为回访客户选择主题");
							return false;
						}
					}
				}
			} else {
				$("#alertInfo").text("*请选择回访客户");
				return false;
			}
			return true;
		});
		
	});

</script>
</head>
<body>
	<s:include value="../right_top.jsp" />
	<s:form action="customer-visit-submit.action" id="cvForm" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
    		<td>
    			<table id="subtree1" width="80%" border="0" cellspacing="0" cellpadding="0" align="left">
    			<tr>
					<td>
						<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
							<tr>
								<td height="35">
									<input type="submit" value="生成客户回访" class="button" />
									<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
							<tr>
								<td height="35">
									标题:
									<input type="text" class="text" size="80" name="title" id="title" onfocus="cleanInfo();"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
							<tr>
								<td height="35">
									主题:
									<s:select list="visitSubjects" listKey="id" listValue="subject" emptyOption="true" theme="simple" id="subject" onchange="selectSubject()" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
        		<tr>
          			<td>
          				<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              				<tr>
               					<td height="40" class="font42">
               					<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
					                  <tr>
 						                  <td height="20" colspan="10" align="center" bgcolor="#EEEEEE" class="tablestyle_title">选择回访客户
 						                  	&nbsp;状态:
										<s:select list="statusMap" listKey="key" emptyOption="true" listValue="value" theme="simple" id="status" />
										&nbsp;产品:
										<s:select list="products" listKey="id" listValue="name" emptyOption="true" theme="simple" id="product" />
										<input type="button" class="query-button" value="查 询" onclick="checkFor();"/>
 						                  </td>
                    				  </tr>
					                  <tr>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">客户名称</td>
					                    <td width="30%" align="center" bgcolor="#EEEEEE">产品名称</td>
						                <td width="30%" align="center" bgcolor="#EEEEEE">主题</td>
					                    <td width="10%" align="center" bgcolor="#EEEEEE">操作</td>
					                  </tr>
					                  <s:if test="productTrials.size() > 0">
					                  	  <s:hidden id="trialLength" value="%{productTrials.size()}" />
						                  <s:iterator value="productTrials" status="pro">
											  <tr id="<s:property value="id"/>">
											  	<s:hidden name="trialIds" value="%{id}"/>
												<td bgcolor="#FFFFFF"><div align="center"><s:property value="customerInfo.companyName"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:property value="product.name"/></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:select list="visitSubjects" listKey="id" listValue="subject" emptyOption="true" name="subjectIds" theme="simple" id="subjectId%{#pro.index}" /></div></td>
							                    <td bgcolor="#FFFFFF"><div align="center"><s:a href="javascript:getDel(%{id});">删除</s:a></div></td>
							                  </tr>
						                  </s:iterator>
					                  </s:if>
					                  <s:else>
					                  	 <tr>
					                  		<td bgcolor="#FFFFFF" colspan="10">暂无数据</td>
					                  	 </tr>
					                  </s:else>
                				</table>
                			</td>
             			 </tr>
            		</table>
            	</td>
        	</tr>
        	<tr>
        		<td align="center" style="color: red;"><div id="alertInfo"></div></td>
        	</tr>
        	<tr>
			<td>
				<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td height="35">
							<input type="submit" value="生成客户回访" class="button" />
							<input type="button" value="返回" class="button" onclick="window.history.go(-1);" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
      </table>
	</td>
  </tr>
</table>
</s:form>
	
</body>
</html>
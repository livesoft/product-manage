function downloadFile(attachmentid){
	$.post("../common/check-attachment-exist.action",
			{"attachment.id" : attachmentid},
			function(data){
				if (data.result == 0) {
					alert('文件已被删除');
				}else{
					document.location.href = '../common/download.action?attachment.id='+attachmentid;
				}
			},"json"
	);
}

function cleanInfo()
{
	$("#alertInfo").text("");
}

function popupNewWin(url,width,height)
{
	$.layer({	
		v_box : 2,
		v_istitle:false,
	    v_src : url,
	    v_area : [width,height],
	    v_btns : 0
	});
}

function popupNewWinTitle(url,width,height,title)
{
	$.layer({	
		v_box : 2,
		v_title:title,
	    v_src : url,
	    v_area : [width,height],
	    v_btns : 0
	});
}


function closeWin(mes)
{
	closeWinRE(mes);
	setTimeout("location.reload()",800);
}

function closeWinRE(mes)
{
	layer_close();
	promptInfo(mes);
}

function promptRedirect(mes)
{
	promptInfo(mes);
	setTimeout("location.reload()",800);
}

function promptInfo(mes)
{
	new Toast({context:$('body'),message:mes}).show();
}

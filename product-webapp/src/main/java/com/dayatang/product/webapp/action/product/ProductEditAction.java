package com.dayatang.product.webapp.action.product;


import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductEditAction extends BaseAction {

	private static final long serialVersionUID = -5998687880486918132L;

	private Product product;
	
	@PMLog(moduleName="产品列表",option="新增产品")
	public String execute() throws Exception {
		if (product != null) {
			product = Product.get(Product.class, product.getId());
		}
		navBar = "产品管理  → 产品列表  → 新增产品";
		return SUCCESS;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}

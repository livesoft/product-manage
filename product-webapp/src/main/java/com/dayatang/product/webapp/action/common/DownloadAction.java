package com.dayatang.product.webapp.action.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Attachment;
import com.dayatang.product.webapp.action.BaseAction;

public class DownloadAction extends BaseAction {

	private static final long serialVersionUID = 3890081552053542388L;

	private Attachment attachment;

	public InputStream getInputStream() throws FileNotFoundException {
		return new FileInputStream(new File(attachment.getPath()));
	}

    public String getDownloadFileName() throws UnsupportedEncodingException{
        return new String(attachment.getFileName().getBytes(), "ISO8859-1");
    }

	@Action(results = {
			@Result(name = "success", type = "stream", params = {"inputName", "inputStream","contentDisposition","attachment;filename=${downloadFileName}"}),
			@Result(name = "input", type = "redirect",location="/mail/html-mail-manage.action") })
	public String execute() throws Exception {
		attachment = Attachment.get(Attachment.class, attachment.getId());
		return SUCCESS;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

}

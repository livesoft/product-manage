package com.dayatang.product.webapp.action.customer;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.domain.InfoCategory;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/trial/trial-edit.action")
@ParentPackage(value = "web")
public class CustomerSubmitAction extends BaseAction {

	private static final long serialVersionUID = -5341610927431100426L;

	private CustomerInfo customer;
	
	private int result;
	
	@PMLog(moduleName = "客户信息管理", option = "新增客户")
	public String execute() throws Exception {
		if (customer != null) {
			customer.setCategory(InfoCategory.CUSTOMERINFO);
			productApplication.saveEntity(customer);
		}
		return SUCCESS;
	}

	@Action(value = "/customer/customer-remove", results = { @Result(name = "success", type = "json")})
	public String removeDictionary() {
		if (customer.getId() > 0) {
			try {
				customer = CustomerInfo.get(CustomerInfo.class, customer.getId());
				productApplication.removeEntity(customer);
				return SUCCESS;
			} catch (DataIntegrityViolationException e) {
				result = 1;
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public CustomerInfo getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerInfo customer) {
		this.customer = customer;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}

package com.dayatang.product.webapp.components;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.components.ContextBean;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import com.dayatang.product.domain.Permission;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * 文件名:Authority.java
 * 描述:权限组件
 * 创建时间:2012-6-13 上午10:24:53
 * 创建者:mluo
 * 版本号:v2.0
 */
public class Authority extends ContextBean {

	private String name;
	
	public Authority(ValueStack stack) {
		super(stack);
	}

	public String getName() {
		return name;
	}

	@StrutsTagAttribute(description="The Permission name", required=true)
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean start(Writer writer) {
		List<String> results = InitAccessString();
		for (String each : results) {
			if (each.contains(name)) {
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private List<String> InitAccessString() {
		List<String> results = new ArrayList<String>();
		List<Permission> permissions = (List<Permission>) ActionContext.getContext().getSession().get("permissions");
		if (permissions != null) {
			for (Permission each : permissions) {
				results.add(each.getName());
			}
		}
		return results;
	}
	
}



package com.dayatang.product.webapp.action.mail;

import java.util.List;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.webapp.annotation.PMLog;

public class HtmlMailManageAction extends MailBaseAction {

	private static final long serialVersionUID = 7213311238366909022L;

	@PMLog(moduleName="邮件管理",option="html邮件")
	public String execute() throws Exception {
		navBar = "邮件管理  → html邮件";
		return SUCCESS;
	}
	
	public List<HtmlMail> getMails() {
		return HtmlMail.findAll(HtmlMail.class);
	}
	
}

package com.dayatang.product.webapp.executor;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.MailReceiver;
import com.dayatang.product.domain.SendMail;
import com.dayatang.product.webapp.util.MailUtil;

public class MailTaskExecutor extends BaseTaskExecutor {
	
	private class MailSendTask implements Runnable {

		private MailReceiver mailReceiver;
		private HtmlMail htmlMail;
		
		public MailSendTask(HtmlMail htmlMail, MailReceiver mailReceiver) {
			this.mailReceiver = mailReceiver;
			this.htmlMail = htmlMail;
		}

		public void run() {
			MailUtil.sendMail(htmlMail, mailReceiver);
		}
		
	}
	
    public void sendMail(SendMail sendMail) {
    	if (sendMail != null && !sendMail.getIsSend()) {
    		for (MailReceiver mailReceiver : sendMail.getMailReceivers()) {
    			if (!mailReceiver.getIsSuccess()) {
    				taskExecutor.execute(new MailSendTask(sendMail.getHtmlMail(),mailReceiver));
    			}
    		}
    	}
    }
    
}

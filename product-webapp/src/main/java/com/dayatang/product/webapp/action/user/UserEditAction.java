package com.dayatang.product.webapp.action.user;

import java.util.HashMap;
import java.util.Map;

import com.dayatang.product.domain.Gender;
import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:UserEditAction.java
 * 描述:用户编辑
 * 创建时间:2012-5-28 下午4:52:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserEditAction extends BaseAction {

	private static final long serialVersionUID = 1555163587419313203L;

	private PersonUser personUser;
	
	public String execute() throws Exception {
		if (personUser != null) {
			navBar = "系统设置  → 用户管理  → 编辑用户";
			personUser = PersonUser.get(PersonUser.class, personUser.getId());
		} else {
			navBar = "系统设置  → 用户管理  → 新增用户";
		}
		return SUCCESS;
	}

	public PersonUser getPersonUser() {
		return personUser;
	}

	public void setPersonUser(PersonUser personUser) {
		this.personUser = personUser;
	}
	
	public Gender[] getGenders() {
		return Gender.values();
	}

	public Map<String, String> getMapStates() {
		Map<String, String> maps = new HashMap<String, String>();
		maps.put("false", "失效");
		maps.put("true", "激活");
		return maps;
	}

	
}



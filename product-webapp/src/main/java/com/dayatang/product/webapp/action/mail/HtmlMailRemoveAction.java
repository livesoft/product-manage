package com.dayatang.product.webapp.action.mail;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.webapp.annotation.PMLog;

@ParentPackage(value = "web")
public class HtmlMailRemoveAction extends MailBaseAction {

	private static final long serialVersionUID = 3036982855646983202L;

	private HtmlMail htmlMail;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName="邮件管理",option="删除html邮件")
	public String execute() throws Exception {
		if (htmlMail.getId() > 0) {
			try {
				productApplication.removeHtmlMail(HtmlMail.get(HtmlMail.class, htmlMail.getId()));
				return SUCCESS;
			} catch (DataIntegrityViolationException e) {
				result = 1;
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public HtmlMail getHtmlMail() {
		return htmlMail;
	}

	public void setHtmlMail(HtmlMail htmlMail) {
		this.htmlMail = htmlMail;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}

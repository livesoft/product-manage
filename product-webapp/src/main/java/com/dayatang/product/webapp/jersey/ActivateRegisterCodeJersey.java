package com.dayatang.product.webapp.jersey;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.RegisterCode;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/activate-register-code/{registerCode}")
@Component
@Scope("request")
public class ActivateRegisterCodeJersey extends BaseAction {

	private static final long serialVersionUID = -604802745142112302L;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String activateRegisterCode(@PathParam("registerCode") String registerCode) {
		RegisterCode code = RegisterCode.getRegisterCodeByCode(registerCode);
		if (code != null) {
			if (code.getIsActivate()) {
				return ERROR;
			} else {
				productApplication.activeRegisterCode(code);
				return OK;
			}
		}
		return "error";
	}
	
}

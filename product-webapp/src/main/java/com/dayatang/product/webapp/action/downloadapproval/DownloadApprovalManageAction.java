package com.dayatang.product.webapp.action.downloadapproval;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.domain.DownloadApprovalCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:DownloadTrailManageAction.java
 * 描述:下载试用审批
 * 创建时间:2012-6-5 上午10:46:40
 * 创建者:mluo
 * 版本号:v2.0
 */
public class DownloadApprovalManageAction extends BaseAction {

	private static final long serialVersionUID = 881709233067498454L;

	private List<DownloadApproval> approvals;
	private Product product;
	private DownloadApprovalCategory category;
	
	@PMLog(moduleName="产品管理",option="试用审批")
	public String execute() throws Exception {
		
		QuerySettings<DownloadApproval> querySettings = QuerySettings.create(DownloadApproval.class);
		
		if (product != null) {
			querySettings.eq("product", product);
		} 
		if (category != null ) {
			querySettings.eq("category", category);
		}
		if (product == null && category == null) {
			querySettings.eq("category", DownloadApprovalCategory.APPLY);
		}
		navBar = "产品管理  → 试用审批";
		approvals = DownloadApproval.findByQuerySettings(querySettings);
		return SUCCESS;
	}

	public List<DownloadApproval> getApprovals() {
		return approvals;
	}
	
	public Map<DownloadApprovalCategory, String> getCategorys() {
		Map<DownloadApprovalCategory, String> map = new HashMap<DownloadApprovalCategory, String>();
		for (DownloadApprovalCategory each : DownloadApprovalCategory.values()) {
			map.put(each, getText(each.toString()));
		}
		return map;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public DownloadApprovalCategory getCategory() {
		return category;
	}

	public void setCategory(DownloadApprovalCategory category) {
		this.category = category;
	}

}



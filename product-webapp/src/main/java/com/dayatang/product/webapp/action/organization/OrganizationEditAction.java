package com.dayatang.product.webapp.action.organization;

import java.util.Map;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.domain.Organization;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:OrganizationEditAction.java
 * 描述:组织机构编辑
 * 创建时间:2012-5-23 下午4:38:27
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationEditAction extends BaseAction {

	private static final long serialVersionUID = 1838152769318887139L;

	private Organization organization;
	private long parentId;
	
	public String execute() throws Exception {
		if (organization != null) {
			organization = Organization.get(Organization.class, organization.getId());
		}
		return SUCCESS;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Map<String,String> getOrgDictionarys() {
		return Dictionary.getMap(DictionaryCategory.ORGANIZATION_TYPE);
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
}




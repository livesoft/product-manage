package com.dayatang.product.webapp.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 文件名:PMLog.java
 * 描述:产品系统日志注解
 * 创建时间:2012-6-14 下午3:28:37
 * 创建者:mluo
 * 版本号:v2.0
 */

@Retention(RetentionPolicy.RUNTIME)  
@Target({ElementType.METHOD})  
public @interface PMLog {
	//模块名  
    String moduleName();  
    //操作内容  
    String option(); 
}


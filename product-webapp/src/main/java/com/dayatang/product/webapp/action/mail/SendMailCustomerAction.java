package com.dayatang.product.webapp.action.mail;

import java.util.List;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.domain.HtmlMail;

public class SendMailCustomerAction extends MailBaseAction {

	private static final long serialVersionUID = 7406955986415042517L;

	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public List<HtmlMail> getMails() {
		return HtmlMail.findAll(HtmlMail.class);
	}
	
	public List<Dictionary> getDictionarys() {
		return Dictionary.findByCategory(DictionaryCategory.PROFESSION_TYPE);
	}

}

package com.dayatang.product.webapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("init");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpSession session = httpServletRequest.getSession();
		String username = (String) session.getAttribute("username");
		if (username == null && httpServletRequest.getRequestURI().lastIndexOf("login.jsp") == -1) {
			request.getRequestDispatcher("/login.jsp").forward(request,response);
		}
		chain.doFilter(request, response);
	}

	public void destroy() {
		System.out.println("destroy");
	}

}

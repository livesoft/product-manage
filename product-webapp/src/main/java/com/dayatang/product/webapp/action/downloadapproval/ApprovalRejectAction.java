package com.dayatang.product.webapp.action.downloadapproval;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:ApprovalRejectAction.java
 * 描述:审批不通过
 * 创建时间:2012-6-5 下午5:52:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public class ApprovalRejectAction extends BaseAction {

	private static final long serialVersionUID = 8420350689343951129L;

	private DownloadApproval approval;
	
	public String execute() throws Exception {
		return SUCCESS;
	}

	public DownloadApproval getApproval() {
		return approval;
	}

	public void setApproval(DownloadApproval approval) {
		this.approval = approval;
	}
	
}



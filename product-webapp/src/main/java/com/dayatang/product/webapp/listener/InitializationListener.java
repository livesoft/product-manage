package com.dayatang.product.webapp.listener;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

import com.dayatang.domain.InstanceFactory;
import com.dayatang.spring.factory.SpringProvider;

public class InitializationListener extends ContextLoaderListener {

    public void contextInitialized(ServletContextEvent event) {  
    	super.contextInitialized(event);
		SpringProvider springProvider = new SpringProvider(getCurrentWebApplicationContext());
		InstanceFactory.setInstanceProvider(springProvider);
    }
    
}

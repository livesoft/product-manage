package com.dayatang.product.webapp.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.domain.EntityRepository;
import com.dayatang.domain.InstanceFactory;
import com.dayatang.product.Constants;
import com.dayatang.product.Page;
import com.dayatang.product.application.LogApplication;
import com.dayatang.product.application.ProductApplication;
import com.dayatang.product.application.SecurityApplication;
import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.domain.Product;
import com.dayatang.utils.WritableConfiguration;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = -8076768072825288275L;
	
	protected static final String CHAREST = "UTF-8";
	protected static final String OK = "ok";
	protected static final String ERROR = "error";
	protected static final String MAILSENDSUCCESS = "mailSendSuccess";
	protected static final String CODEISACTIVATE = "CodeIsActivate";
	protected static final String CODENOTEXIST = "CodeNotExist";
	protected static final String REGISTERCODENOTEXIST = "registerCodeNotExist";
	protected static final String CODEINVALID = "codeInvalid";
	protected static final String LOGIN = "LOGIN";
	protected static final String USERNAMENOEXISTS = "usernameNoExists";
	protected static final String PASSWORDERROR = "passwordError";
	protected static final String USERDISABLE = "userDisable"; 
	protected static final String NOPERMISSION = "noPermission";
	protected static final String NOROLE = "noRole";
	protected static final String FAILURE = "failure";
	protected static final String LOGINUSER = "LOGINUSER";
	protected static final String PERMISSIONS = "permissions";
	protected static final String RESOURCES = "resources";
	
	protected static final int ZERO = 0;
	protected static final int ONE = 1;
	protected static final int TWO = 2;
	
	private static WritableConfiguration configuration;
	
	protected String navBar;
	
	protected Page page = new Page();
	
	@Inject
	protected ProductApplication productApplication;
	
	@Inject
	protected SecurityApplication securityApplication;
	
	@Inject
	protected LogApplication logApplication;
	
	@Inject
	private EntityRepository repository;

	private Map<DictionaryCategory, Map<String, String>> dictionaryMap = new HashMap<DictionaryCategory, Map<String,String>>();
	
	public void setProductApplication(ProductApplication productApplication) {
		this.productApplication = productApplication;
	}
	
	public void setSecurityApplication(SecurityApplication securityApplication) {
		this.securityApplication = securityApplication;
	}
	
	public void setLogApplication(LogApplication logApplication) {
		this.logApplication = logApplication;
	}

	@JSON(serialize = false)
	public EntityRepository getRepository() {
		if (repository == null) {
			repository = InstanceFactory.getInstance(EntityRepository.class);
		}
		return repository;
	}

	public void setRepository(EntityRepository repository) {
		this.repository = repository;
	}

	@JSON(serialize = false)
	private WritableConfiguration getConfiguration() {
		if (configuration == null) {
			configuration = InstanceFactory.getInstance(WritableConfiguration.class);
		}
		return configuration;
	}

	public void setConfiguration(WritableConfiguration configuration) {
		BaseAction.configuration = configuration;
	}
	
	// 如果页面要取这个方法的值是不能有参数的
	public String getNavBar() {
		return "您现在所在的位置：" + navBar;
	}
	
	@JSON(serialize = false)
	public Page getPage() {
		return page;
	}

	@JSON(serialize = false)
	public List<Product> getProducts() {
		return Product.findAll(Product.class);
	}
	
	@JSON(serialize = false)
	public String getDictionary(DictionaryCategory category, String key) {
		Map<String, String> dictionaries = dictionaryMap.get(category);
		if (dictionaries == null) {
			dictionaries = Dictionary.getMap(category);
			dictionaryMap.put(category, dictionaries);
		}
		return dictionaries.get(key);
	}
	
	@JSON(serialize = false)
	public String getUploadDir() {
		return getConfiguration().getString(Constants.UPLOAD_DIR, "/tmp/product");
	}

	@JSON(serialize = false)
	public String getCharset() {
		return getConfiguration().getString(Constants.CHARSET, "utf-8");
	}
	
	@JSON(serialize = false)
	public String getProfessionTypeName(String key) {
		return getDictionary(DictionaryCategory.PROFESSION_TYPE, key);
	}

	@JSON(serialize = false)
	public Map<String,String> getDictionaryMaps() {
		return Dictionary.getMap(DictionaryCategory.PROFESSION_TYPE);
	}
	
	@JSON(serialize = false)
	protected String getRequestIP() {
		HttpServletRequest req = (HttpServletRequest)ServletActionContext.getRequest();
        String lastLoginIP = req.getHeader("x-forwarded-for");  
        if(lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {  
            lastLoginIP = req.getHeader("Proxy-Client-IP");  
        }  
        if(lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {  
            lastLoginIP = req.getHeader("WL-Proxy-Client-IP");  
        }  
        if(lastLoginIP == null || lastLoginIP.length() == 0 || "unknown".equalsIgnoreCase(lastLoginIP)) {  
            lastLoginIP = req.getRemoteAddr();  
        }
		return lastLoginIP;
	}
	
	@JSON(serialize = false)
	protected Object getSessinoValue(String sessionKey) {
		Object sessionValue = ActionContext.getContext().getSession().get(sessionKey);
		return  sessionValue == null ? "" : sessionValue;
	}
	
	protected void setSessiolnValue(String sessionKey, Object sessionValue) {
		ActionContext.getContext().getSession().put(sessionKey, sessionValue);
	}
	
	@JSON(serialize = false)
	protected PersonUser getPersonUser() {
		return (PersonUser)getSessinoValue(LOGINUSER);
	}

}

package com.dayatang.product.webapp.interceptor;

import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.domain.Resource;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -2407388582331353737L;

	public String intercept(ActionInvocation invocation) throws Exception {
		PersonUser personUser = (PersonUser) invocation.getInvocationContext().getSession().get("LOGINUSER");
		if (personUser != null) {
			return invocation.invoke();
		}
		for (Resource resource : Resource.findCommonResource()) {
			if (invocation.getAction().toString().contains(resource.getResourceType())) {
				return invocation.invoke();
			}
		}
		return Action.LOGIN;
	}

}

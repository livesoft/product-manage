package com.dayatang.product.webapp.action.trial;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/trial/trial-manage.action")
public class TrialDaySubmitAction extends BaseAction {

	private static final long serialVersionUID = 3905415409966389049L;

	private ProductTrial productTrial;
	
	private long trialDay;
	
	@PMLog(moduleName = "试用号管理", option = "修改试用期")
	public String execute() throws Exception {
		if (productTrial.getId() > 0) {
			productTrial = ProductTrial.get(ProductTrial.class, productTrial.getId());
			productApplication.modifyTrialDay(productTrial, trialDay);
		}
		return SUCCESS;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}

	public long getTrialDay() {
		return trialDay;
	}

	public void setTrialDay(long trialDay) {
		this.trialDay = trialDay;
	}
	
}

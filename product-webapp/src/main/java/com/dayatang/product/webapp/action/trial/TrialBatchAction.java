package com.dayatang.product.webapp.action.trial;

import java.util.List;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.webapp.action.BaseAction;

public class TrialBatchAction extends BaseAction {

	private static final long serialVersionUID = 5388090604851111943L;
	
	public String execute() throws Exception {
		navBar = "试用号管理  → 批量生产试用号";
		return SUCCESS;
	}
	
	public List<Dictionary> getDictionarys() {
		return Dictionary.findByCategory(DictionaryCategory.PROFESSION_TYPE);
	}

}

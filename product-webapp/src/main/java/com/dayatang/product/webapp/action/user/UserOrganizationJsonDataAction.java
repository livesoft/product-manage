package com.dayatang.product.webapp.action.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.codehaus.jettison.json.JSONException;

import com.dayatang.product.domain.Organization;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.action.common.TreeJson;

/**
 * 文件名:UserOrganizationJsonDataAction.java
 * 描述:加载部门数据(json)
 * 创建时间:2012-6-1 上午11:24:56
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class UserOrganizationJsonDataAction extends BaseAction {

	private static final long serialVersionUID = -6888087397544571076L;

	private long userId;

	private List<TreeJson> results;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		results = new ArrayList<TreeJson>();
		getDatas();
		return SUCCESS;
	}

	public void getDatas() throws JSONException, org.json.JSONException {
		Organization root = Organization.getRootOrg();
		TreeJson json = new TreeJson();
		json.setId(root.getId());
		json.setpId(0);
		json.setName(root.getName());
		json.setOpen(root.getChildren().size() > 0 ? true : false);
		results.add(json);
		for (Organization each : root.getChildren()) {
			getChildRenData(each, root.getId());
		}
	}
	
	public void getChildRenData(Organization childRen, long parentId) {
		TreeJson json = new TreeJson();
		json.setId(childRen.getId());
		json.setpId(parentId);
		json.setName(childRen.getName());
		json.setOpen(childRen.getChildren().size() > 0 ? true : false);
		results.add(json);
		if (childRen.getChildren().size() > 0) {
			for (Organization each : childRen.getChildren()) {
				getChildRenData(each, childRen.getId());
			}
		}
	}

	public List<TreeJson> getResults() {
		return results;
	}

	@JSON(serialize =false)
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}



package com.dayatang.product.webapp.action.customervisit;

import java.util.List;

import com.dayatang.product.domain.CustomerVisit;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:CustomerVisitManageAction.java
 * 描述:客户回访管理
 * 创建时间:2012-6-27 上午11:13:13
 * 创建者:mluo
 * 版本号:v2.0
 */
public class CustomerVisitManageAction extends BaseAction {

	private static final long serialVersionUID = -748505957308006443L;

	private List<CustomerVisit> customerVisits;
	
	@PMLog(moduleName = "产品管理", option = "客户回访")
	public String execute() throws Exception {
		navBar = "产品管理  → 客户回访";
		customerVisits = CustomerVisit.findAll(CustomerVisit.class);
		return SUCCESS;
	}

	public List<CustomerVisit> getCustomerVisits() {
		return customerVisits;
	}
	
}



package com.dayatang.product.webapp.action.customer;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.webapp.action.BaseAction;

public class CustomerEditAction extends BaseAction {

	private static final long serialVersionUID = 536502566485591283L;

	private CustomerInfo customer;
	
	public String execute() throws Exception {
		if (customer != null) {
			navBar = "客户信息管理  → 客户列表  → 编辑客户";
			customer = CustomerInfo.get(CustomerInfo.class, customer.getId());
		} else {
			navBar = "客户信息管理  → 客户列表  → 新增客户";
		}
		return SUCCESS;
	}

	public CustomerInfo getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerInfo customer) {
		this.customer = customer;
	}
	
}

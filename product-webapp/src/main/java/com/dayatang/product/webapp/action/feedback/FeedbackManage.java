package com.dayatang.product.webapp.action.feedback;

import java.util.List;

import com.dayatang.product.domain.FeedbackWeb;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class FeedbackManage extends BaseAction {

	private static final long serialVersionUID = 6692267139602390961L;

	private Product product;
	private List<FeedbackWeb> feedbackWebs;
	
	@PMLog(moduleName = "反馈信息管理", option = "反馈信息")
	public String execute() throws Exception {
		if (product != null) {
			feedbackWebs = FeedbackWeb.getFeedbackWebsByProduct(product);
		} else {
			feedbackWebs = FeedbackWeb.findAll(FeedbackWeb.class);
		}
		navBar = "反馈信息管理  → 反馈信息";
		return SUCCESS;
	}
	
	public List<FeedbackWeb> getFeedbackWebs() {
		return feedbackWebs;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}

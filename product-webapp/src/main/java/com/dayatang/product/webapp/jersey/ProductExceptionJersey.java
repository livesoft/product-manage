package com.dayatang.product.webapp.jersey;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.ExceptionDisposeCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductException;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/product-exception")
@Component
@Scope("request")
public class ProductExceptionJersey extends BaseAction {

	private static final long serialVersionUID = -2911929623592044478L;

	@QueryParam("mark") String mark;
	@QueryParam("trialCode") String trialCode;
	@QueryParam("exceptionText") String exceptionText;
	@QueryParam("actionName") String actionName;
	
	@GET
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public String productException() {
		if (mark != null && trialCode != null) {
			ProductException productException = new ProductException();
			productException.setProduct(Product.getProductByRemark(mark));
			productException.setProductTrial(ProductTrial.getProductTrialByTrialCode(trialCode));
			productException.setIp(getRequestIP());
			productException.setExceptionText(exceptionText);
			productException.setActionName(actionName);
			productException.setExceptionDate(new Date());
			productException.setCategory(ExceptionDisposeCategory.NODISPOSE);
			productApplication.saveEntity(productException);
			return "ok";
		}
		return "error";
	}
}

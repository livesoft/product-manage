package com.dayatang.product.webapp.action.user;

import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:UserPasswordResetAction.java
 * 描述:用户密码重置
 * 创建时间:2012-5-29 下午5:42:42
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserPasswordResetAction extends BaseAction {

	private static final long serialVersionUID = 3725130540288030667L;

	private PersonUser personUser;
	
	@Override
	public String execute() throws Exception {
		if (personUser.getId() > 0) {
			personUser = PersonUser.get(PersonUser.class, personUser.getId());
		}
		return SUCCESS;
	}

	public PersonUser getPersonUser() {
		return personUser;
	}

	public void setPersonUser(PersonUser personUser) {
		this.personUser = personUser;
	}
	
}



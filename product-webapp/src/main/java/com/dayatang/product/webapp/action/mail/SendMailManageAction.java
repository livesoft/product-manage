package com.dayatang.product.webapp.action.mail;

import java.util.ArrayList;
import java.util.List;

import com.dayatang.product.domain.MailReceiver;
import com.dayatang.product.domain.SendMail;
import com.dayatang.product.webapp.annotation.PMLog;

public class SendMailManageAction extends MailBaseAction {

	private static final long serialVersionUID = 1972597886186602688L;

	@PMLog(moduleName="邮件管理",option="发送详情")
	public String execute() throws Exception {
		navBar = "邮件管理  → 发送详情";
		return SUCCESS;
	}
	
	public List<SendMail> getSendMails() {
		List<SendMail> sendMails = new ArrayList<SendMail>();
		for (SendMail sendMail : SendMail.findAll(SendMail.class)) {
			int i = 0;
			for (MailReceiver mailReceiver : sendMail.getMailReceivers()) {
				if (mailReceiver.getIsSuccess()) {
					i++;
				}
			}
			if (i == sendMail.getMailReceivers().size()) {
				sendMail.setSend(true);
			}
			sendMails.add(sendMail);
 		}
		return sendMails;
	}
}

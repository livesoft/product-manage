package com.dayatang.product.webapp.action.product;

import com.dayatang.product.domain.RegisterCode;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductUseCountAction extends BaseAction {

	private static final long serialVersionUID = 5742357754414399823L;

	private RegisterCode registerCode;
	
	@PMLog(moduleName="使用情况",option="使用次数详情")
	public String execute() throws Exception {
		if (registerCode != null) {
			registerCode = RegisterCode.get(RegisterCode.class, registerCode.getId());
		}
		navBar = "产品管理  → 使用情况  → 使用次数详情";
		return SUCCESS;
	}

	public RegisterCode getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(RegisterCode registerCode) {
		this.registerCode = registerCode;
	}
	
}

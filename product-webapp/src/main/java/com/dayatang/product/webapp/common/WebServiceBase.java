package com.dayatang.product.webapp.common;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class WebServiceBase {
	
	public static URI getURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	public static ClientResponse getWebServicesResult(String url, MultivaluedMap<String, String> queryParams) {
		ClientConfig config = new DefaultClientConfig();  
		Client client = Client.create(config);
		WebResource resource = client.resource(getURI(url));
		if (queryParams == null) {
			return resource.type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
		}
        return resource.queryParams(queryParams).type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
	}
	
}

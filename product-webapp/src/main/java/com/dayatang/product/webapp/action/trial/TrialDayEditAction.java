package com.dayatang.product.webapp.action.trial;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.webapp.action.BaseAction;

public class TrialDayEditAction extends BaseAction {

	private static final long serialVersionUID = -4342908913661113430L;
	
	private ProductTrial productTrial;
	
	public String execute() throws Exception {
		if (productTrial.getId() > 0) {
			productTrial = ProductTrial.get(ProductTrial.class, productTrial.getId());
		}
		return SUCCESS;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}
	
}

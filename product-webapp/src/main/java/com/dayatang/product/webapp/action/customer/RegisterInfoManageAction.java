package com.dayatang.product.webapp.action.customer;

import java.util.List;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class RegisterInfoManageAction extends BaseAction {

	private static final long serialVersionUID = -9155377479465368756L;

	private Product product;
	
	private long importance;
	
	private List<RegisterInfo> registerInfos;
	
	@PMLog(moduleName = "客户信息管理", option = "注册信息")
	public String execute() throws Exception {
		
		QuerySettings<RegisterInfo> querySettings = QuerySettings.create(RegisterInfo.class);

		if (product != null && importance > 0) {
			querySettings.eq("productTrial.product", product).eq("importance", importance);
		} else if (product != null) {
			querySettings.eq("productTrial.product", product);
		} else if (importance > 0) {
			querySettings.eq("importance", importance);
		} else {
			querySettings.desc("id");
		}
		
		navBar = "客户信息管理  → 注册信息";
		registerInfos = RegisterInfo.findByQuerySettings(querySettings);
		
		return SUCCESS;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getImportance() {
		return importance;
	}

	public void setImportance(long importance) {
		this.importance = importance;
	}

	public List<RegisterInfo> getRegisterInfos() {
		return registerInfos;
	}

	public void setRegisterInfos(List<RegisterInfo> registerInfos) {
		this.registerInfos = registerInfos;
	}
	
}

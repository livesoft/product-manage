package com.dayatang.product.webapp.action.exception;

import java.util.List;

import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductException;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductExceptionManageAction extends BaseAction {

	private static final long serialVersionUID = 4241083907181885314L;
	
	private Product product;
	private List<ProductException> productExceptions;
	
	@PMLog(moduleName="产品管理",option="异常报错")
	public String execute() throws Exception {
		
		if (product != null) {
			productExceptions = ProductException.getProductExceptionsByProduct(product);
		} else {
			productExceptions = ProductException.findAll(ProductException.class);
		}
		navBar = "产品管理  → 异常报错";
		
		return SUCCESS;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<ProductException> getProductExceptions() {
		return productExceptions;
	}
	
	public String getActionName(String name) {
		return name.substring(name.lastIndexOf("."), name.indexOf("@"));
	}
}

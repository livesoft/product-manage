package com.dayatang.product.webapp.action.exception;

import com.dayatang.product.domain.ProductException;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductExceptionDetailAction extends BaseAction {

	private static final long serialVersionUID = -7031700107503523854L;

	private ProductException productException;
	
	@PMLog(moduleName="异常报错",option="报错详情")
	public String execute() throws Exception {
		if (productException.getId() > 0) {
			productException = ProductException.get(ProductException.class, productException.getId());
		}
		return SUCCESS;
	}

	public ProductException getProductException() {
		return productException;
	}

	public void setProductException(ProductException productException) {
		this.productException = productException;
	}
	
}

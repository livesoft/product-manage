package com.dayatang.product.webapp.action.user;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:UserSubmitAction.java
 * 描述:新增用户
 * 创建时间:2012-5-28 下午11:17:09
 * 创建者:mluo
 * 版本号:v2.0
 */
@Result(name = "success", type = "redirect", location = "/user/user-manage.action")
public class UserSubmitAction extends BaseAction {

	private static final long serialVersionUID = -6235298263177533897L;

	private PersonUser personUser;
	private String password;
	private long[] orgIds;
	private long[] roleIds;
	
	@PMLog(moduleName = "用户管理", option = "新增用户")
	public String execute() throws Exception {
		if (personUser != null ) {
			personUser.setPassword(password);
			securityApplication.createPersonUser(personUser,orgIds,roleIds);
		}
		return SUCCESS;
	}

	public PersonUser getPersonUser() {
		return personUser;
	}

	public void setPersonUser(PersonUser personUser) {
		this.personUser = personUser;
	}

	public long[] getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(long[] orgIds) {
		this.orgIds = orgIds;
	}

	public long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(long[] roleIds) {
		this.roleIds = roleIds;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}

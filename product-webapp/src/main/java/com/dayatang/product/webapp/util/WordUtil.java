package com.dayatang.product.webapp.util;

import java.util.Set;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.domain.Visit;
import com.dayatang.product.domain.VisitSubject;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:WordUtil.java
 * 描述:word文档工具
 * 创建时间:2012-6-29 上午10:55:39
 * 创建者:mluo
 * 版本号:v2.0
 */
public class WordUtil extends BaseAction {

	private static final long serialVersionUID = 7782730896877413317L;
	
	public static XWPFDocument createWord(String fileName, Set<VisitSubject> visitSubjects, Set<Visit> visits) {
		XWPFDocument doc = createXWPFDocument();
		createWordTitle(doc,fileName);
		int i = 1;
		for (VisitSubject visitSubject : visitSubjects) {
			createSubject(doc,visitSubject,i,visits);
			i++;
		}
		return doc;
	}
	
	private static XWPFDocument createXWPFDocument() {
		return new XWPFDocument();
	}
	
	private static void createWordTitle(XWPFDocument doc, String fileName) {
		XWPFParagraph p = doc.createParagraph();  
        p.setAlignment(ParagraphAlignment.CENTER);  
        XWPFRun r = p.createRun();  
        r.setText(fileName + "客户回访帮助文档");  
        r.setBold(true);  
        r.setFontFamily("Courier"); 
        r.setFontSize(20);
        r.setTextPosition(80);
	}
	
	private static void createSubject(XWPFDocument doc, VisitSubject visitSubject, int subjectNum, Set<Visit> visits) {
		createSubjectParagraph(doc,"主题" + subjectNum + ":" + visitSubject.getSubject());
		createVisitProblem(doc,visitSubject);
		eachSubjectCustomer(doc,visitSubject,visits);
	}
	
	private static void eachSubjectCustomer(XWPFDocument doc, VisitSubject visitSubject, Set<Visit> visits) {
		int i = 1;
		for (Visit visit : visits) {
			if (visit.getVisitSubject().equals(visitSubject)) {
				createCustomer(doc,visit,i);
				i++;
			}
		}
	}
	
	private static void createCustomer(XWPFDocument doc, Visit visit, int customerNum) {
		RegisterInfo registerInfo = RegisterInfo.getRegisterByTrial(visit.getProductTrial());
		String text = "";
        if (registerInfo != null) {
        	text = "      客户" + customerNum + ":" + registerInfo.getCompanyName() + "      电话:" + registerInfo.getPhone() + "      负责人:" + registerInfo.getContact() + "(" + registerInfo.getMobile() + ")";
        } else {
        	text = "      客户" + customerNum + ":" + visit.getProductTrial().getCustomerInfo().getCompanyName() + "      电话:" + visit.getProductTrial().getCustomerInfo().getPhone();
        }
        createCustomerParagraph(doc,text);
        createVisitRecord(doc,visit);
	}
	
	private static void createVisitRecord(XWPFDocument doc, Visit visit) {
		Visit lastVisit = Visit.findLastVisit(visit);
		if (lastVisit != null) {
			createCustomerParagraph(doc,"            上次回访主题:" + lastVisit.getVisitSubject().getSubject());
			createCustomerParagraph(doc,"            回访结果:" );
			int i = 0;
			for (String each : lastVisit.getVisitResult().getResult().split("<br>")) {
				if (i == 0) {
					createCustomerParagraph(doc,"             " + each);
				} else {
					createCustomerParagraph(doc,"            " + each);
				}
				i++;
			}
		}
	}
	
	private static void createVisitProblem(XWPFDocument doc, VisitSubject visitSubject) {
		int i = 0;
		for (String text : visitSubject.getContent().split("<br>")) {
			if (i == 0) {
				createProblemParagraph(doc,"       " + text);
			} else {
				createProblemParagraph(doc,"      " + text);
			}
			i++;
		}
	}

	private static XWPFRun createParagraph(XWPFDocument doc, String text) {
		XWPFParagraph p = doc.createParagraph();  
        XWPFRun r = p.createRun();  
        r.setBold(true);  
        r.setText(text);
        r.setFontFamily("Courier");  
        return r;
	}
	
	private static void createSubjectParagraph(XWPFDocument doc, String text) {
		XWPFRun r = createParagraph(doc,text);
		r.setFontSize(16);
	}
	
	private static void createProblemParagraph(XWPFDocument doc, String text) {
		XWPFRun r = createParagraph(doc,text);
		r.setFontSize(14);
	}
	
	
	private static void createCustomerParagraph(XWPFDocument doc, String text) {
		XWPFRun r = createParagraph(doc,text);
		r.setFontSize(12);
		r.setBold(false);
	}
	
}



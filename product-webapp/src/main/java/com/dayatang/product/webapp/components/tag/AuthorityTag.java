package com.dayatang.product.webapp.components.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ContextBeanTag;

import com.dayatang.product.webapp.components.Authority;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * 文件名:AuthorityTag.java
 * 描述:权限标签
 * 创建时间:2012-6-13 上午11:19:38
 * 创建者:mluo
 * 版本号:v2.0
 */
public class AuthorityTag extends ContextBeanTag {

	private static final long serialVersionUID = -3526494532846693602L;

	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Component getBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
		return new Authority(stack);
	}

	protected void populateParams() {
		Authority authority = (Authority) getComponent();
		authority.setName(getName());
	}
	
}



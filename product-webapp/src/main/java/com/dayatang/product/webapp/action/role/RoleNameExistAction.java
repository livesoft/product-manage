package com.dayatang.product.webapp.action.role;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:RoleNameExistAction.java
 * 描述:查角色名称是否存在
 * 创建时间:2012-5-28 上午9:43:30
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class RoleNameExistAction extends BaseAction {

	private static final long serialVersionUID = -7238403930714670410L;

	private String roleName;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (roleName != null) {
			result = Role.findByName(roleName) == null ? 0 : 1;
 		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}



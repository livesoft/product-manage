package com.dayatang.product.webapp.action.downloadapproval;

import javax.inject.Inject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.domain.DownloadApprovalCategory;
import com.dayatang.product.domain.EmailInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;
import com.dayatang.product.webapp.executor.ApprovalTaskExecutor;

/**
 * 文件名:ApprovalRejectSubmitAction.java
 * 描述:审批不通过,并发邮件给客户说明原因
 * 创建时间:2012-6-5 下午11:32:19
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class ApprovalRejectSubmitAction extends BaseAction {

	private static final long serialVersionUID = -6259163404861574855L;

	private DownloadApproval approval;
	private String reasonText;
	private String sendMail;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName="试用审批",option="审批不通过")
	public String execute() throws Exception {
		if (approval != null) {
			approval = DownloadApproval.get(DownloadApproval.class, approval.getId());
			approval.setCategory(DownloadApprovalCategory.APPROVAL_REJECT);
			approval.setReasonText(reasonText);
			productApplication.saveEntity(approval);
			if (sendMail != null) {
				approvalTaskExecutor.sendMail(EmailInfo.getDefault(), approval);
			}
		}
		return SUCCESS;
	}

	public DownloadApproval getApproval() {
		return approval;
	}

	public void setApproval(DownloadApproval approval) {
		this.approval = approval;
	}

	public String getReasonText() {
		return reasonText;
	}

	public void setReasonText(String reasonText) {
		this.reasonText = reasonText;
	}

	public String getSendMail() {
		return sendMail;
	}

	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}

	@Inject
	private ApprovalTaskExecutor approvalTaskExecutor;

	public void setApprovalTaskExecutor(ApprovalTaskExecutor approvalTaskExecutor) {
		this.approvalTaskExecutor = approvalTaskExecutor;
	}
	
}

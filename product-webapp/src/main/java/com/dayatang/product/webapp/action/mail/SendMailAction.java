package com.dayatang.product.webapp.action.mail;

import java.util.List;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.webapp.annotation.PMLog;

public class SendMailAction extends MailBaseAction {

	private static final long serialVersionUID = -1152103453749281662L;

	@PMLog(moduleName="邮件管理",option="发送邮件")
	public String execute() throws Exception {
		navBar = "邮件管理  → 发送邮件";
		return SUCCESS;
	}
	
	public List<HtmlMail> getMails() {
		return HtmlMail.findAll(HtmlMail.class);
	}
	
}

package com.dayatang.product.webapp.action.remoteclient;

import com.dayatang.product.domain.MobileClientCategory;
import com.dayatang.product.webapp.action.BaseAction;

public class RemoteClientEditAction extends BaseAction {

	private static final long serialVersionUID = -1241431758788142294L;

	public String execute() throws Exception {
		navBar = "产品管理  → 手机客户端  → 发布客户端";
		return SUCCESS;
	}
	
	public MobileClientCategory[] getCategories() {
		return MobileClientCategory.values();
	}

}

package com.dayatang.product.webapp.action.systemexception;

import com.dayatang.product.domain.SystemException;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:SystemExceptionDetailAction.java
 * 描述:异常详情
 * 创建时间:2012-7-2 下午11:31:15
 * 创建者:mluo
 * 版本号:v2.0
 */
public class SystemExceptionDetailAction extends BaseAction {

	private static final long serialVersionUID = 8439098006372404411L;

	private SystemException systemException;
	
	@PMLog(moduleName = "系统异常", option = "异常详情")
	public String execute() throws Exception {
		navBar = "系统设置  → 系统异常  → 异常详情";
		if (systemException != null) {
			systemException = SystemException.get(SystemException.class, systemException.getId());
		}
		return SUCCESS;
	}

	public SystemException getSystemException() {
		return systemException;
	}

	public void setSystemException(SystemException systemException) {
		this.systemException = systemException;
	}
	
}

package com.dayatang.product.webapp.action.user;

import java.util.List;

import com.dayatang.product.domain.PersonOrganization;
import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.domain.RoleAssignment;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:UserManageAction.java
 * 描述:用户管理
 * 创建时间:2012-5-28 上午11:10:58
 * 创建者:mluo
 * 版本号:v2.0
 */
public class UserManageAction extends BaseAction {

	private static final long serialVersionUID = 3738160024393810943L;

	private List<PersonUser> personUsers;
	
	@PMLog(moduleName = "系统设置", option = "用户管理")
	public String execute() throws Exception {
		navBar = "系统设置  → 用户管理";
		personUsers = PersonUser.findAll(PersonUser.class);
		return SUCCESS;
	}

	public List<PersonUser> getPersonUsers() {
		return personUsers;
	}

	public static String getOrganizations(long personId) {
		String result = "";
		if (personId > 0) {
			for (PersonOrganization each : PersonOrganization.findByPerson(personId)) {
				result += each.getOrganization().getName() + ",";
			}
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
	
	public static String getRoles(PersonUser personUser) {
		String result = "";
		if (personUser != null) {
			for (RoleAssignment each : RoleAssignment.findByUser(personUser)) {
				result += each.getRole().getName() + ",";
			}
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
}



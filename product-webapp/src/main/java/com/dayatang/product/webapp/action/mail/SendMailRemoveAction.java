package com.dayatang.product.webapp.action.mail;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.SendMail;

@ParentPackage(value = "web")
public class SendMailRemoveAction extends MailBaseAction {

	private static final long serialVersionUID = 6399418841844729544L;

	private SendMail sendMail;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (sendMail.getId() > 0) {
			try {
				productApplication.removeSendMail(SendMail.get(SendMail.class, sendMail.getId()));
			} catch (DataIntegrityViolationException e) {
				result = 1;
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public SendMail getSendMail() {
		return sendMail;
	}

	public void setSendMail(SendMail sendMail) {
		this.sendMail = sendMail;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}

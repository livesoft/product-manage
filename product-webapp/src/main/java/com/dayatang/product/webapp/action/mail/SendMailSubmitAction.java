package com.dayatang.product.webapp.action.mail;

import javax.inject.Inject;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.SendMail;
import com.dayatang.product.webapp.annotation.PMLog;
import com.dayatang.product.webapp.executor.MailTaskExecutor;

@Result(name = "success", type = "redirect", location = "/mail/send-mail-manage.action")
public class SendMailSubmitAction extends MailBaseAction {

	private static final long serialVersionUID = 1036266553089569425L;

	private long sendMailId;
	
	@Inject
	private MailTaskExecutor mailTaskExecutor;
	
	@PMLog(moduleName="邮件管理",option="邮件发送")
	public String execute() throws Exception {
		mailTaskExecutor.sendMail(SendMail.get(SendMail.class, sendMailId));
		return SUCCESS;
	}

	public void setMailTaskExecutor(MailTaskExecutor mailTaskExecutor) {
		this.mailTaskExecutor = mailTaskExecutor;
	}

	public long getSendMailId() {
		return sendMailId;
	}

	public void setSendMailId(long sendMailId) {
		this.sendMailId = sendMailId;
	}
	
}

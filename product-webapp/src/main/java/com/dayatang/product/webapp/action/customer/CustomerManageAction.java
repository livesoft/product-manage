package com.dayatang.product.webapp.action.customer;

import java.util.List;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class CustomerManageAction extends BaseAction {

	private static final long serialVersionUID = -2733330035676689300L;

	private String professionType;
	
	private List<CustomerInfo> customers;
	
	@PMLog(moduleName = "客户信息管理", option = "客户列表")
	public String execute() throws Exception {
		navBar = "客户信息管理  → 客户列表";
		
		if (professionType != null) {
			customers = CustomerInfo.findByProfessionType(professionType,page);
		} else {
			customers = CustomerInfo.findAll(CustomerInfo.class,page);
			page.setUrl("customer-manage.action?");
		}
		
		return SUCCESS;
	}
	
	public List<CustomerInfo> getCustomers() {
		return customers;
	}
	
	public String getProfessionType() {
		return professionType;
	}

	public void setProfessionType(String professionType) {
		this.professionType = professionType;
	}

}

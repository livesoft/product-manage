package com.dayatang.product.webapp.action.organization;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;


import com.dayatang.product.domain.Organization;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:OrganizationSubmitAction.java
 * 描述:新增组织机构
 * 创建时间:2012-5-24 上午9:58:04
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class OrganizationSubmitAction extends BaseAction {

	private static final long serialVersionUID = -5769455402153643988L;

	private Organization organization;
	
	private Organization parent;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "组织机构", option = "添加组织机构")
	public String execute() throws Exception {
		if (parent.getId() == 0) {
			productApplication.saveEntity(organization);
		} else if (organization != null && parent != null) {
			parent = Organization.get(Organization.class, parent.getId());
			productApplication.createLowerOrganization(organization, parent);
		} 
		return SUCCESS;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Organization getParent() {
		return parent;
	}

	public void setParent(Organization parent) {
		this.parent = parent;
	}
	
}

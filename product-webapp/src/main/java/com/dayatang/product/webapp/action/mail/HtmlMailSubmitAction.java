package com.dayatang.product.webapp.action.mail;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/mail/html-mail-manage.action")
public class HtmlMailSubmitAction extends AttachmentBaseAction {

	private static final long serialVersionUID = 2298934002617734477L;

	private HtmlMail htmlMail;
	
	private List<File> files; // 多附件
	private List<String> filesFileName; // 附件名称
	
	private HttpServletResponse response = ServletActionContext.getResponse();
	
	@PMLog(moduleName="邮件管理",option="新增html邮件")
	public String execute() throws Exception {
		
		if (htmlMail != null) {
			htmlMail.setCreateDate(new Date());
			productApplication.saveEntity(htmlMail);
		}
		
		if (files != null ) {
			String root = getUploadDir() + "/mail/id" + HtmlMail.getLashHtmlMail() + "/";
			htmlMail.setAttachments(uploadFiles(root, files, filesFileName));
			productApplication.saveEntity(htmlMail);
		}
		
		response.setCharacterEncoding(getCharset());
		response.getWriter().print("保存成功！");
		
		if (files != null) {
			return null;
		} else {
			return SUCCESS;
		}
		
	}

	public HtmlMail getHtmlMail() {
		return htmlMail;
	}

	public void setHtmlMail(HtmlMail htmlMail) {
		this.htmlMail = htmlMail;
	}
	
	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public List<String> getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(List<String> filesFileName) {
		this.filesFileName = filesFileName;
	}

}

package com.dayatang.product.webapp.action.remoteclient;

import java.util.List;

import com.dayatang.product.domain.ProductMobileClient;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class RemoteClientManageAction extends BaseAction {

	private static final long serialVersionUID = -6457306394295387954L;

	@PMLog(moduleName="产品管理",option="手机客户端")
	public String execute() throws Exception {
		navBar = "产品管理  → 手机客户端";
		return SUCCESS;
	}
	
	public List<ProductMobileClient> getProductMobileClients() {
		return ProductMobileClient.findAll(ProductMobileClient.class);
	}
}

package com.dayatang.product.webapp.action.mail;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class GetCustomerInfoAction extends BaseAction {

	private static final long serialVersionUID = 8078130981463366521L;

	private String professionType;
	
	private List<CustomerInfo> customerInfos;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		customerInfos = new ArrayList<CustomerInfo>();
		for (CustomerInfo customerInfo : CustomerInfo.findByProfessionType(professionType)) {
			System.out.println("==");
			String email = customerInfo.getEmail();
			if (email != null && !email.equals("")) {
				customerInfos.add(customerInfo);
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getProfessionType() {
		return professionType;
	}

	public void setProfessionType(String professionType) {
		this.professionType = professionType;
	}

	public List<CustomerInfo> getCustomerInfos() {
		return customerInfos;
	}

}

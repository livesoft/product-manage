package com.dayatang.product.webapp.action.trial;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.TrialCode;
import com.dayatang.product.webapp.action.BaseAction;

@Result(name = "success", type = "redirect", location = "/trial/trial-manage.action")
public class TrialValidityActivateAction extends BaseAction {

	private static final long serialVersionUID = -5036315038509769383L;

	private TrialCode trialCode;
	
	public String execute() throws Exception {
		if (trialCode.getId() > 0) {
			trialCode = TrialCode.get(TrialCode.class, trialCode.getId());
			if (!trialCode.getIsEffective()) {
				trialCode.setEffective(true);
				trialCode.setValidityDay(trialCode.getValidity());
				productApplication.saveEntity(trialCode);
			}
		}
		return SUCCESS;
	}

	public TrialCode getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(TrialCode trialCode) {
		this.trialCode = trialCode;
	}

}

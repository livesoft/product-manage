package com.dayatang.product.webapp.action.remoteclient;

import com.dayatang.product.domain.ProductMobileClient;
import com.dayatang.product.webapp.action.BaseAction;

public class RemoteClientUpdateAction extends BaseAction {

	private static final long serialVersionUID = 5776995496867186545L;

	private ProductMobileClient productMobileClient;
	
	public String execute() throws Exception {
		if (productMobileClient.getId() > 0) {
			productMobileClient = ProductMobileClient.get(ProductMobileClient.class, productMobileClient.getId());
		}
		navBar = "产品管理  → 手机客户端  → 更新客户端";
		return SUCCESS;
	}

	public ProductMobileClient getProductMobileClient() {
		return productMobileClient;
	}

	public void setProductMobileClient(ProductMobileClient productMobileClient) {
		this.productMobileClient = productMobileClient;
	}
	
}

package com.dayatang.product.webapp.action.visitsubject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.VisitSubject;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:VisitSubjectSubmitAction.java
 * 描述:添加主题
 * 创建时间:2012-6-27 上午9:44:19
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class VisitSubjectSubmitAction extends BaseAction {

	private static final long serialVersionUID = 5604337760417839074L;

	private VisitSubject visitSubject;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "回访主题", option = "新增主题")
	public String execute() throws Exception {
		if (visitSubject != null) {
			productApplication.saveEntity(visitSubject);
		}
		return SUCCESS;
	}

	public VisitSubject getVisitSubject() {
		return visitSubject;
	}

	public void setVisitSubject(VisitSubject visitSubject) {
		this.visitSubject = visitSubject;
	}
	
}



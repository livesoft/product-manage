package com.dayatang.product.webapp.action.dictionary;

import java.util.SortedMap;
import java.util.TreeMap;

import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.webapp.action.BaseAction;

public class DictionaryBaseAction extends BaseAction {

	private static final long serialVersionUID = -2970347967307899683L;

	public SortedMap<DictionaryCategory,String> getCategories() {
		SortedMap<DictionaryCategory,String> categoriesMap= new TreeMap<DictionaryCategory, String>();
		for(DictionaryCategory category : DictionaryCategory.values()){
			categoriesMap.put(category,getText(category.toString()));
		}
		return categoriesMap;
	}
	
}

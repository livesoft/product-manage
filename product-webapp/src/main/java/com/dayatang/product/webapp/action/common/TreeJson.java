package com.dayatang.product.webapp.action.common;
/**
 * 文件名:TreeJson.java
 * 描述:结构树json
 * 创建时间:2012-7-3 下午10:16:33
 * 创建者:mluo
 * 版本号:v2.0
 */
public class TreeJson {
	
	private long id;
	private long pId;
	private String name;
	private boolean open;
	private boolean checked;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getpId() {
		return pId;
	}
	public void setpId(long pId) {
		this.pId = pId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}


}

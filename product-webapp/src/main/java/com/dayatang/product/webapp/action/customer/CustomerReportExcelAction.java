package com.dayatang.product.webapp.action.customer;

import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class CustomerReportExcelAction extends BaseAction {

	private static final long serialVersionUID = -4246544763057117285L;
	
	@PMLog(moduleName = "客户信息管理", option = "导入数据")
	public String execute() throws Exception {
		navBar = "客户信息管理  → 导入数据";
		return SUCCESS;
	}
}

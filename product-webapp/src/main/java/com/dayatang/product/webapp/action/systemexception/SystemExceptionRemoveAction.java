package com.dayatang.product.webapp.action.systemexception;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.SystemException;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:SystemExceptionRemoveAction.java
 * 描述:删除系统异常
 * 创建时间:2012-7-2 下午11:28:19
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class SystemExceptionRemoveAction extends BaseAction {

	private static final long serialVersionUID = 5836998146377795957L;

	private long exceptionId;
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "系统异常", option = "删除异常")
	public String execute() throws Exception {
		if (exceptionId > 0) {
			try {
				productApplication.removeEntity(SystemException.get(SystemException.class, exceptionId));
			} catch (DataIntegrityViolationException e) {
				result = ONE;
			}
		}
		return SUCCESS;
	}

	public void setExceptionId(long exceptionId) {
		this.exceptionId = exceptionId;
	}

	public int getResult() {
		return result;
	}
	
}

package com.dayatang.product.webapp.executor;

import javax.inject.Inject;

import org.springframework.core.task.TaskExecutor;

/**
 * 文件名:BaseTaskExecutor.java
 * 描述:执行器基类
 * 创建时间:2012-6-6 上午10:18:01
 * 创建者:mluo
 * 版本号:v2.0
 */
public class BaseTaskExecutor {
	
	protected static final String CHAREST = "UTF-8";

	@Inject
	protected TaskExecutor taskExecutor;
	
    public void setTaskExecutor(TaskExecutor taskExecutor) {  
        this.taskExecutor = taskExecutor;  
    }  
	
}



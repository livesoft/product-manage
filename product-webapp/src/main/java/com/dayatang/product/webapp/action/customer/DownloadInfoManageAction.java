package com.dayatang.product.webapp.action.customer;

import java.util.List;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:DownloadInfoManageAction.java
 * 描述:下载试用客户信息
 * 创建时间:2012-6-5 下午11:55:22
 * 创建者:mluo
 * 版本号:v2.0
 */
public class DownloadInfoManageAction extends BaseAction {

	private static final long serialVersionUID = 7362061666789550706L;

	@PMLog(moduleName = "客户信息管理", option = "下载客户")
	public String execute() throws Exception {
		navBar = "客户信息管理  → 下载客户";
		return SUCCESS;
	}
	
	public List<DownloadApproval> getApprovals() {
		return DownloadApproval.findAll(DownloadApproval.class);
	}
}

package com.dayatang.product.webapp.action.role;

import java.util.HashMap;
import java.util.Map;

import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:RoleEditAction.java
 * 描述:角色编辑
 * 创建时间:2012-5-24 下午10:24:35
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleEditAction extends BaseAction {

	private static final long serialVersionUID = -635877497220229763L;

	private Role role;
	
	public String execute() throws Exception {
		if (role != null) {
			role = Role.get(Role.class, role.getId());
		}
		return SUCCESS;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public Map<String, String> getMapStates() {
		Map<String, String> maps = new HashMap<String, String>();
		maps.put("false", "失效");
		maps.put("true", "激活");
		return maps;
	}
}

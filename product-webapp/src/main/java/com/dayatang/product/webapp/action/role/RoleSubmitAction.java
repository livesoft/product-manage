package com.dayatang.product.webapp.action.role;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:RoleSubmitAction.java
 * 描述:添加角色
 * 创建时间:2012-5-24 下午11:20:15
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class RoleSubmitAction extends BaseAction {

	private static final long serialVersionUID = 7052590474919844136L;

	private Role role;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "角色设置", option = "添加角色")
	public String execute() throws Exception {
		if (role != null) {
			productApplication.saveEntity(role);
		}
		return SUCCESS;
	}

	@Action(value = "/role/role-remove",results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "角色设置", option = "删除角色")
	public String removeRole() {
		try {
			if (role.getId() != 0) {
				role = Role.get(Role.class, role.getId());
				productApplication.removeEntity(role);
			}
		} catch (DataIntegrityViolationException e) {
			result = ONE;
		}
		return SUCCESS;
	}
	
	@JSON(serialize = false)
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getResult() {
		return result;
	}

}

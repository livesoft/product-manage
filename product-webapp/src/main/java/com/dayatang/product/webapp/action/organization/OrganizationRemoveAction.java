package com.dayatang.product.webapp.action.organization;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.Organization;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:OrganizationRemoveAction.java
 * 描述:删除组织机构
 * 创建时间:2012-5-24 下午2:20:18
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class OrganizationRemoveAction extends BaseAction {

	private static final long serialVersionUID = 5752900286184598384L;

	private Organization organization;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "组织机构", option = "删除组织机构")
	public String execute() throws Exception {
		if (organization != null) {
			if (organization.getId() > 0) {
				try {
					organization = Organization.get(Organization.class, organization.getId());
					if (organization.getChildren().size() > 0) {
						result = TWO;
						return SUCCESS;
					}
					productApplication.removeEntity(organization);
				} catch (DataIntegrityViolationException e) {
					result = ONE;
				}
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}



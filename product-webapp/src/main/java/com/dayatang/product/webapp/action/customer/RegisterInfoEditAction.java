package com.dayatang.product.webapp.action.customer;

import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;

public class RegisterInfoEditAction extends BaseAction {

	private static final long serialVersionUID = 2689420285510611758L;

	private RegisterInfo registerInfo;
	
	private String type;
	
	public String execute() throws Exception {
		if (registerInfo.getId() > 0) {
			registerInfo = RegisterInfo.get(RegisterInfo.class, registerInfo.getId());
		}
		return SUCCESS;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}

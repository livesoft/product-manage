package com.dayatang.product.webapp.action.customervisit;

import java.util.List;

import com.dayatang.product.domain.VisitSubject;
import com.dayatang.product.webapp.action.common.ProductStateDataBaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:CustomerVisitEditAction.java
 * 描述:新增客户回访
 * 创建时间:2012-6-27 上午11:27:36
 * 创建者:mluo
 * 版本号:v2.0
 */
public class CustomerVisitEditAction extends ProductStateDataBaseAction {

	private static final long serialVersionUID = 7993332868032032962L;

	@PMLog(moduleName = "客户回访", option = "新增客户回访")
	public String execute() throws Exception {
		loadData();
		navBar = "产品管理  → 客户回访  → 新增客户回访";
		return SUCCESS;
	}
	
	public List<VisitSubject> getVisitSubjects() {
		return VisitSubject.findStateByActive();
	}
}



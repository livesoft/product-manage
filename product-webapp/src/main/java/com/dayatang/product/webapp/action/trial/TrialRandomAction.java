package com.dayatang.product.webapp.action.trial;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class TrialRandomAction extends BaseAction {

	private static final long serialVersionUID = 6286750686351776414L;

	private Product product;
	
	private String trialCode;

	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		
		if (product != null) {
			product = Product.get(Product.class, product.getId()); 
			trialCode = productApplication.createTrailCode(product.getMark());
		} else {
			trialCode = null;
		}
		
		return SUCCESS;
	}
	
	@JSON(serialize = false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(String trialCode) {
		this.trialCode = trialCode;
	}
	
}

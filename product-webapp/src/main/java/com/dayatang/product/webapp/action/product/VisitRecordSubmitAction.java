package com.dayatang.product.webapp.action.product;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.VisitResult;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:VisitRecordSubmitAction.java
 * 描述:提交回访结果
 * 创建时间:2012-6-28 上午10:28:08
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class VisitRecordSubmitAction extends BaseAction {

	private static final long serialVersionUID = 4476070403451345296L;

	private VisitResult visitResult;
	private long subjectId;
	private long trialId;
	private String result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (visitResult != null && subjectId > 0 && trialId > 0) {
			visitResult.setReportPerson("test");
			visitResult.setResult(result);
			productApplication.recordVisitResult(visitResult, subjectId, trialId);
		}
		return SUCCESS;
	}

	public void setVisitResult(VisitResult visitResult) {
		this.visitResult = visitResult;
	}
	
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

	public void setTrialId(long trialId) {
		this.trialId = trialId;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}



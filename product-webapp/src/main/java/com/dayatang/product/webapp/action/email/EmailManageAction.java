package com.dayatang.product.webapp.action.email;

import java.util.List;

import com.dayatang.product.domain.EmailInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class EmailManageAction extends BaseAction {

	private static final long serialVersionUID = 6774422996341581920L;

	@PMLog(moduleName = "系统设置", option = "邮箱管理")
	public String execute() throws Exception {
		navBar = "系统设置  → 邮箱管理";
		return SUCCESS;
	}
	
	public List<EmailInfo> getEmailInfos() {
		return EmailInfo.findAll(EmailInfo.class);
	}
	
}

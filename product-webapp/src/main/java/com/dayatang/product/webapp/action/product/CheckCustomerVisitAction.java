package com.dayatang.product.webapp.action.product;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.Visit;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:CheckCustomerVisitAction.java
 * 描述:检测是否有要填写的客户回访记录
 * 创建时间:2012-6-28 上午9:50:07
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class CheckCustomerVisitAction extends BaseAction {

	private static final long serialVersionUID = -4229673750099229348L;

	private long trialId;
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (trialId > 0) {
			List<Visit> visits = Visit.findByProductTrialNotVisit(ProductTrial.get(ProductTrial.class, trialId));
			if (visits.size() > 0) {
				result = ONE;
			}
		}
		return SUCCESS;
	}

	public int getResult() {
		return result;
	}

	public void setTrialId(long trialId) {
		this.trialId = trialId;
	}
	
}



package com.dayatang.product.webapp.action.dictionary;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class CheckExistAction extends BaseAction {

	private static final long serialVersionUID = 8971034002501812162L;

	private int result;

	private String text;
	
	private String serialNumber;
	
	private DictionaryCategory category;
	
	private int sortOrder;
	
	@Action(results = { @Result(name = "success", type = "json") })
	public String execute() {
		if(Dictionary.isTextExist(text)) {
			result = 1;
		}
		return SUCCESS;
	}
	
	@Action(value="/dictionary/serialNumber-exist", results = {@Result(name = "success", type = "json")})
	public String isSerialNumberExist() {
		if(Dictionary.isSerialNumberExist(serialNumber)) {
			result = 1;
		}
		return SUCCESS;
	}
	
	@Action(value="/dictionary/sortOrder-exist", results = {@Result(name = "success", type = "json")})
	public String isSortOrderExist() {
		if(Dictionary.isSortOrderExist(sortOrder,category)) {
			result = 1;
		}
		return SUCCESS;
	}
	
	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public DictionaryCategory getCategory() {
		return category;
	}

	public void setCategory(DictionaryCategory category) {
		this.category = category;
	}

}

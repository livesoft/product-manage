package com.dayatang.product.webapp.action.logrecord;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:LogRecordRemoveAction.java
 * 描述:删除日志记录
 * 创建时间:2012-7-2 上午11:30:28
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class LogRecordRemoveAction extends BaseAction {

	private static final long serialVersionUID = -1632648003528656165L;

	private long logId;
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (logId > 0) {
			try {
				logApplication.removeEntity(logId);
			} catch (DataIntegrityViolationException e) {
				result = ONE;
			}
		}
		return SUCCESS;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public int getResult() {
		return result;
	}
	
}



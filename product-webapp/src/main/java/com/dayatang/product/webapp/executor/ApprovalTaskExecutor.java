package com.dayatang.product.webapp.executor;

import javax.mail.MessagingException;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.domain.EmailInfo;

/**
 * 文件名:ApprovalTaskExecutor.java
 * 描述:审批结果告知客户执行器
 * 创建时间:2012-6-6 上午10:17:19
 * 创建者:mluo
 * 版本号:v2.0
 */
public class ApprovalTaskExecutor extends BaseTaskExecutor {
	
	private class ApprovalTask implements Runnable {

		private DownloadApproval approval;
		private EmailInfo emailInfo;
		
		public ApprovalTask(EmailInfo emailInfo, DownloadApproval approval) {
			this.emailInfo = emailInfo;
			this.approval = approval;
		}

		public void run() {
			sendMessageMail(emailInfo,approval);
		}

	}
	
	public void sendMail(EmailInfo emailInfo, DownloadApproval approval) {
		taskExecutor.execute(new ApprovalTask(emailInfo,approval));
	}
	
	private void sendMessageMail(EmailInfo emailInfo, DownloadApproval approval) {
		try {
			Email email = new SimpleEmail();
			email.setHostName(emailInfo.getSmtpName());
			email.setSmtpPort(emailInfo.getSmtpPort());
			email.setAuthenticator(new DefaultAuthenticator(emailInfo.getUsername(),emailInfo.getPassword()));
			email.setTLS(false);
			email.setFrom(emailInfo.getUsername());
			email.addTo(approval.getDownloadInfo().getEmail());
			email.setCharset(CHAREST);
			email.setSubject("客户申请试用审批回复邮件");
			email.buildMimeMessage();
			email.getMimeMessage().setText(approval.getDownloadInfo().getCompanyName() + "您好,您申请试用" + approval.getProduct().getName() +"审批没通过,原因是" + approval.getReasonText() + ",感谢您对广州大雅堂信息科技有限公司的支持,需要咨询请致电020-87266226！", CHAREST);
			email.sendMimeMessage();
		} catch (EmailException e) {
			e.toString();
		} catch (MessagingException e) {
			e.toString();
		} 
	}
}



package com.dayatang.product.webapp.action.product;

import java.util.List;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.Visit;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:VisitRecordEditAction.java
 * 描述:添加回访记录
 * 创建时间:2012-6-28 上午10:16:40
 * 创建者:mluo
 * 版本号:v2.0
 */
public class VisitRecordEditAction extends BaseAction {

	private static final long serialVersionUID = 6644669457421676767L;

	private long trialId;
	private List<Visit> visits;
	
	public String execute() throws Exception {
		if (trialId > 0) {
			visits = Visit.findByProductTrialNotVisit(ProductTrial.get(ProductTrial.class, trialId));
		}
		return SUCCESS;
	}

	public void setTrialId(long trialId) {
		this.trialId = trialId;
	}

	public long getTrialId() {
		return trialId;
	}

	public List<Visit> getVisits() {
		return visits;
	}
	
}



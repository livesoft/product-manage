package com.dayatang.product.webapp.action.mail;

import com.dayatang.product.webapp.annotation.PMLog;

public class HtmlMailEditAction extends MailBaseAction {

	private static final long serialVersionUID = 5498834024789081284L;

	@PMLog(moduleName="html邮件",option="新增html邮件")
	public String execute() throws Exception {
		navBar = "邮件管理  → html邮件  → 新增html邮件";
		return SUCCESS;
	}
}


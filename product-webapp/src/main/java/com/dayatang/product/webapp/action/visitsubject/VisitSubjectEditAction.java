package com.dayatang.product.webapp.action.visitsubject;

import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:VisitSubjectEditAction.java
 * 描述:主题编辑
 * 创建时间:2012-6-27 上午12:13:00
 * 创建者:mluo
 * 版本号:v2.0
 */
public class VisitSubjectEditAction extends BaseAction {

	private static final long serialVersionUID = 5673763008823383791L;

	public String execute() throws Exception {
		return SUCCESS;
	}
	
}

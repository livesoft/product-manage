package com.dayatang.product.webapp.action.organization;

import com.dayatang.product.domain.Organization;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:OrganizationManageAction.java
 * 描述:组织机构加载
 * 创建时间:2012-5-22 上午9:31:21
 * 创建者:mluo
 * 版本号:v2.0
 */
public class OrganizationManageAction extends BaseAction {

	private static final long serialVersionUID = -3991276318190790617L;

	private String orgHtml;
	
	@PMLog(moduleName = "系统设置", option = "组织机构")
	public String execute() throws Exception {
		navBar = "系统设置  → 组织机构";
		orgHtml = getRootOrgToHtml(Organization.getRootOrg());
		return SUCCESS;
	}
	
	private String getRootOrgToHtml(Organization root) {
		StringBuilder builder = new StringBuilder();
		builder.append("<ul id='org' style='display:none'>").append("<li id='" + root.getId() + "'>").append(root.getName()).append("<ul>");;
		for (Organization childRen : root.getChildren()) {
			builder.append(getChildRenOrgToHtml(childRen));
		}
		builder.append("</ul>").append("</li>").append("</ul>");
		return builder.toString();
	}
	
	private StringBuilder getChildRenOrgToHtml(Organization childRen) {
		StringBuilder builder = new StringBuilder();
		builder.append("<li id='" + childRen.getId() + "'>").append(childRen.getName());
		if (childRen.getChildren().size() > 0) {
			builder.append("<ul>");
			for (Organization child : childRen.getChildren()) {
				builder.append(getChildRenOrgToHtml(child));
			}
			builder.append("</ul>");
		}
		builder.append("</li>");
		return builder;
	}
	
	public String getOrgHtml() {
		return orgHtml;
	}

	public void setOrgHtml(String orgHtml) {
		this.orgHtml = orgHtml;
	}
	
}



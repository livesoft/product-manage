package com.dayatang.product.webapp.action.trial;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.TrialStatus;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class TrialManageAction extends BaseAction {

	private static final long serialVersionUID = -784863858893592687L;
	
	private TrialStatus status;
	
	private Product product;
	
	private List<ProductTrial> productTrials;
	
	@PMLog(moduleName = "试用号管理", option = "试用号列表")
	public String execute() throws Exception {
		
		QuerySettings<ProductTrial> querySettings = QuerySettings.create(ProductTrial.class);
		
		if (product != null && status != null) {
			product = Product.get(Product.class, product.getId());
			querySettings.eq("product", product).eq("status", status);
		} else if (product != null) {
			product = Product.get(Product.class, product.getId());
			querySettings.eq("product", product);
		} else if (status != null) {
			querySettings.eq("status", status);
		} else {
			querySettings.desc("trialCode.registerDate");
		}
		
		navBar = "试用号管理  → 试用号列表";
		productTrials = ProductTrial.findByQuerySettings(querySettings);
		return SUCCESS;
	}
	
	public List<ProductTrial> getProductTrials() {
		return productTrials;
	}

	public void setProductTrials(List<ProductTrial> productTrials) {
		this.productTrials = productTrials;
	}

	public TrialStatus getStatus() {
		return status;
	}

	public void setStatus(TrialStatus status) {
		this.status = status;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Map<TrialStatus, String> getStatusMap() {
		Map<TrialStatus, String> map = new HashMap<TrialStatus, String>();
		for (TrialStatus status : TrialStatus.values()) {
			map.put(status, getText(status.toString()));
		}
		return map;
	}
}

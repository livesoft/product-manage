package com.dayatang.product.webapp.interceptor;

import java.util.List;

import com.dayatang.product.domain.Resource;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 文件名:AuthorityInterceptor.java
 * 描述:权限拦截器
 * 创建时间:2012-6-12 下午4:27:09
 * 创建者:mluo
 * 版本号:v2.0
 */
public class AuthorityInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -7434356936413767288L;
	private static final String AUTHORITY = "authority";

	@SuppressWarnings("unchecked")
	public String intercept(ActionInvocation invocation) throws Exception {
		List<String> resources = (List<String>) invocation.getInvocationContext().getSession().get("resources");
		if (resources != null) {
			for (String resource : resources) {
				if (invocation.getAction().toString().contains(resource)) {
					return invocation.invoke();
				}
			}
		}
		
		for (Resource resource : Resource.findCommonResource()) {
			if (invocation.getAction().toString().contains(resource.getResourceType())) {
				return invocation.invoke();
			}
		}
		return AUTHORITY;
	}

}



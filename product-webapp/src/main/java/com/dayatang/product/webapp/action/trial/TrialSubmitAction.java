package com.dayatang.product.webapp.action.trial;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/trial/trial-manage.action")
@ParentPackage(value = "web")
public class TrialSubmitAction extends BaseAction {

	private static final long serialVersionUID = 4959404615731461069L;

	private ProductTrial productTrial;
	private long trialPeriod;
	private int result;
	
	@PMLog(moduleName = "试用号管理", option = "新增试用号")
	public String execute() throws Exception {
		if (productTrial != null) {
			productApplication.createProductTrial(productTrial,trialPeriod);
		}
		return SUCCESS;
	}
	
	@Action(value = "/trial/trial-remove", results = { @Result(name = "success", type = "json")})
	public String removeProductTrial() {
		if (productTrial.getId() > 0) {
			try {
				productTrial = ProductTrial.get(ProductTrial.class, productTrial.getId());
				productApplication.removeProductTiral(productTrial);
			} catch (DataIntegrityViolationException e) {
				result = 1;
			}
		}
		return SUCCESS;
	}
	
	@JSON(serialize = false)
	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@JSON(serialize = false)
	public long getTrialPeriod() {
		return trialPeriod;
	}

	public void setTrialPeriod(long trialPeriod) {
		this.trialPeriod = trialPeriod;
	}

}

package com.dayatang.product.webapp.action.mail;

import java.util.List;

import com.dayatang.product.domain.EmailInfo;
import com.dayatang.product.webapp.action.BaseAction;

public class MailBaseAction extends BaseAction {

	private static final long serialVersionUID = 256915239254222263L;

	public List<EmailInfo> getEmailInfos() {
		return EmailInfo.getUsableEmialInfo();
	}
	
}

package com.dayatang.product.webapp.action.customer;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.dayatang.excel.DataType;
import com.dayatang.excel.ExcelReader;
import com.dayatang.excel.ReadingRange;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@ParentPackage(value = "web")
public class CustomerUploadExcelAction extends BaseAction {

	private static final long serialVersionUID = -8209294864362253032L;

	private File files;
	
	private HttpServletResponse response = ServletActionContext.getResponse();
	
	@PMLog(moduleName = "导入数据", option = "导入了一份excel")
	public String execute() throws Exception {
		
		List<Object[]> datas = null;
		
		if (files != null) {
			ExcelReader importer = new ExcelReader(files);
			int[] columnIndexes = new int[] {0, 1, 2, 3, 4, 5, 6};
			DataType[] columnTypes = new DataType[] {DataType.STRING, DataType.STRING, DataType.STRING, DataType.NUMERIC, DataType.STRING, DataType.STRING, DataType.STRING};
			ReadingRange range = new ReadingRange.Builder().sheetAt(0).rowFrom(1).columns(columnIndexes, columnTypes).build();
			datas = importer.read(range);
			productApplication.reportCustomerInfo(datas);
		}
		
		response.setCharacterEncoding(getCharset());
		response.getWriter().print("共" + datas.size() + "条数据,成功导入" + datas.size() + "条");
		
		return null;
	}

	public File getFiles() {
		return files;
	}

	public void setFiles(File files) {
		this.files = files;
	}

}

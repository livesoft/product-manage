package com.dayatang.product.webapp.action.systemexception;

import java.util.List;

import com.dayatang.product.domain.SystemException;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:SystemExceptionManageAction.java
 * 描述:系统异常列表
 * 创建时间:2012-7-2 下午10:42:27
 * 创建者:mluo
 * 版本号:v2.0
 */
public class SystemExceptionManageAction extends BaseAction {

	private static final long serialVersionUID = 1968292418589270597L;

	private List<SystemException> systemExceptions;
	
	@PMLog(moduleName = "系统设置", option = "系统异常")
	public String execute() throws Exception {
		navBar = "系统设置  → 系统异常";
		systemExceptions = SystemException.findAll(SystemException.class);
		return SUCCESS;
	}

	public List<SystemException> getSystemExceptions() {
		return systemExceptions;
	}
	
}

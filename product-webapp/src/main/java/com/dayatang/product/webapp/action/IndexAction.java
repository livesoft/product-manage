package com.dayatang.product.webapp.action;

import com.dayatang.product.webapp.annotation.PMLog;

public class IndexAction extends BaseAction {

	private static final long serialVersionUID = 9147837504564031919L;
	
	@PMLog(moduleName="人员管理",option="添加用户")  
	public String execute() throws Exception {
		navBar = "个人首页";
		return SUCCESS;
	}
	
}

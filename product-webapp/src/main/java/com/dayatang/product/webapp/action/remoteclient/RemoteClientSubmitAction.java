package com.dayatang.product.webapp.action.remoteclient;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dayatang.product.domain.ProductMobileClient;
import com.dayatang.product.webapp.action.mail.AttachmentBaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class RemoteClientSubmitAction extends AttachmentBaseAction {

	private static final long serialVersionUID = -339414063198529703L;

	private ProductMobileClient productMobileClient;
	private List<File> files; // 多附件
	private List<String> filesFileName; // 附件名称
	
	private HttpServletResponse response = ServletActionContext.getResponse();
	
	@PMLog(moduleName="手机客户端",option="发布手机客户端")
	public String execute() throws Exception {
		
		if (productMobileClient != null) {
			productApplication.saveEntity(productMobileClient);
		}
		
		if (files != null ) {
			String root = getUploadDir() + "/remoteclient/id" + ProductMobileClient.getLastProductMobileClientId() + "/";
			productMobileClient.setAttachments(uploadFiles(root, files, filesFileName));
			productApplication.saveEntity(productMobileClient);
		}
		
		response.setCharacterEncoding(getCharset());
		response.getWriter().print("保存成功！");
		
		return null;
	}

	public ProductMobileClient getProductMobileClient() {
		return productMobileClient;
	}

	public void setProductMobileClient(ProductMobileClient productMobileClient) {
		this.productMobileClient = productMobileClient;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public List<String> getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(List<String> filesFileName) {
		this.filesFileName = filesFileName;
	}

}

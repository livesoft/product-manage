package com.dayatang.product.webapp.quartz;

import javax.inject.Inject;

import com.dayatang.product.application.ProductApplication;

public class BaseQuartz {
	
	@Inject
	protected ProductApplication productApplication;

	public void setProductApplication(ProductApplication productApplication) {
		this.productApplication = productApplication;
	}
	
}

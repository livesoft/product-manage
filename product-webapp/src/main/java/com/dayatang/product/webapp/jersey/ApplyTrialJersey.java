package com.dayatang.product.webapp.jersey;

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.DownloadInfo;
import com.dayatang.product.domain.InfoCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.executor.MessageTaskExecutor;

/**
 * 文件名:ApplyTrialJersey.java
 * 描述:申请试用产品api
 * 创建时间:2012-6-4 上午10:11:03
 * 创建者:mluo
 * 版本号:v2.0
 */
@Path("/apply-trial")
@Component
@Scope("request")
public class ApplyTrialJersey extends BaseAction {

	private static final long serialVersionUID = -5263025649255386952L;

	@QueryParam("companyName") String companyName;
	@QueryParam("phone") String phone;
	@QueryParam("email") String email;
	@QueryParam("contact") String contact;
	@QueryParam("address") String address;
	@QueryParam("mobile") String mobile;
	@QueryParam("productId") long productId;
	@QueryParam("trench") String trench;
	
	@GET
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public String applyTrial() {
		DownloadInfo downloadInfo = new DownloadInfo();
		downloadInfo.setCompanyName(companyName);
		downloadInfo.setPhone(phone);
		downloadInfo.setContact(contact);
		downloadInfo.setMobile(mobile);
		downloadInfo.setEmail(email);
		downloadInfo.setAddress(address);
		downloadInfo.setApplyDate(new Date());
		downloadInfo.setCategory(InfoCategory.DOWNLOADINFO);
		downloadInfo.setTrench(trench);
		
		Product product = Product.get(Product.class, productId);
		if (product != null) {
			productApplication.applyTrial(downloadInfo, product);
			messageTaskExecutor.sendMail(companyName);
		}
		
		return OK;
	}
	
	@Inject
	private MessageTaskExecutor messageTaskExecutor;

	public void setMessageTaskExecutor(MessageTaskExecutor messageTaskExecutor) {
		this.messageTaskExecutor = messageTaskExecutor;
	}
	
}



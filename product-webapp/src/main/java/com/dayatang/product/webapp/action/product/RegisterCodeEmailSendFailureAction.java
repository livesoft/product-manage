package com.dayatang.product.webapp.action.product;

import javax.inject.Inject;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.executor.RegisterCodeMailTaskExecutor;

@Result(name = "success", type = "redirect", location = "/product/product-use-manage.action")
public class RegisterCodeEmailSendFailureAction extends BaseAction {

	private static final long serialVersionUID = -4097958443723220142L;

	private RegisterInfo registerInfo;
	
	public String execute() throws Exception {
		if (registerInfo.getId() > 0) {
			registerInfo = RegisterInfo.get(RegisterInfo.class, registerInfo.getId());
			registerCodeMailTaskExecutor.sendMail(registerInfo.getProductTrial().getRegisterCode().getRegisterCode(), registerInfo, HtmlMail.get(HtmlMail.class, Long.valueOf(1)));
		}
		return SUCCESS;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}
	
	@Inject
	private RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor;

	public void setRegisterCodeMailTaskExecutor(
			RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor) {
		this.registerCodeMailTaskExecutor = registerCodeMailTaskExecutor;
	}
	
}

package com.dayatang.product.webapp.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.TrialCode;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/check-trial-code/{trialCode}")
@Component
@Scope("request")
public class CheckTrialCodeJersey extends BaseAction {

	private static final long serialVersionUID = 4575776760280032755L;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String checkTrialCode(@PathParam("trialCode") String trialCode) {
		TrialCode code = TrialCode.getTrialCodeByTrialCode(trialCode);
		if (code != null) {
			if (code.getIsActivate()) {
				return CODEISACTIVATE;
			} else {
				return OK;
			}
		} 
		return CODENOTEXIST;
	}
	
}

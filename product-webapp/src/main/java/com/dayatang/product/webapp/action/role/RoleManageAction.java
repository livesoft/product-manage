package com.dayatang.product.webapp.action.role;

import java.util.List;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:RoleManageAction.java
 * 描述:角色管理
 * 创建时间:2012-5-24 下午5:39:14
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleManageAction extends BaseAction {

	private static final long serialVersionUID = 7968522422396194616L;

	private String roleName;
	private List<Role> roles;
	
	@PMLog(moduleName = "系统设置", option = "角色设置")
	public String execute() throws Exception {
		navBar = "系统设置  → 角色设置";
		if (roleName == null) {
			roles = Role.findAll(Role.class);
		} else {
			QuerySettings<Role> querySettings = QuerySettings.create(Role.class).containsText("name", roleName);
			roles = Role.findByQuerySettings(querySettings);
		}
		return SUCCESS;
	}
	
	public List<Role> getRoles() {
		return roles;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}



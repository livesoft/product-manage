package com.dayatang.product.webapp.action.product;


import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductManageAction extends BaseAction {

	private static final long serialVersionUID = -540562547612617487L;
	
	@PMLog(moduleName="产品管理",option="产品列表")
	public String execute() throws Exception {
		navBar = "产品管理  → 产品列表";
		return SUCCESS;
	}
	
}

package com.dayatang.product.webapp.action.visitsubject;

import java.util.List;

import com.dayatang.product.domain.VisitSubject;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:VisitSubjectManageAction.java
 * 描述:回访主题
 * 创建时间:2012-6-26 下午9:21:45
 * 创建者:mluo
 * 版本号:v2.0
 */
public class VisitSubjectManageAction extends BaseAction {

	private static final long serialVersionUID = 1109939834183685110L;

	private List<VisitSubject> visitSubjects;
	
	@PMLog(moduleName = "产品设置", option = "回访主题")
	public String execute() throws Exception {
		navBar = "产品设置  → 回访主题";
		visitSubjects = VisitSubject.findAll(VisitSubject.class);
		return SUCCESS;
	}

	public List<VisitSubject> getVisitSubjects() {
		return visitSubjects;
	}
	
}

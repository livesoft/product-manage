package com.dayatang.product.webapp.action.visitsubject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.VisitSubject;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:VisitSubjectRemoveAction.java
 * 描述:删除主题
 * 创建时间:2012-6-27 上午10:32:36
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class VisitSubjectRemoveAction extends BaseAction {

	private static final long serialVersionUID = 4333549905804322960L;
	
	private VisitSubject visitSubject;
	private int result;
	private boolean state;

	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "回访主题", option = "删除主题")
	public String execute() throws Exception {
		try {
			if (visitSubject != null) {
				visitSubject = VisitSubject.get(VisitSubject.class, visitSubject.getId());
				productApplication.removeEntity(visitSubject);
			}
		} catch (DataIntegrityViolationException e) {
			result = ONE;
		}
		return SUCCESS;
	}
	
	@Action(value = "/visitsubject/visit-subject-state",results = {@Result(name = "success", type = "json")})
	public String forbiddenSubject() {
		if (visitSubject != null) {
			visitSubject = VisitSubject.get(VisitSubject.class, visitSubject.getId());
			System.out.println(state + "+++++++");
			visitSubject.setState(!state);
			productApplication.saveEntity(visitSubject);
		}
		return SUCCESS;
	}
	
	public int getResult() {
		return result;
	}

	public void setVisitSubject(VisitSubject visitSubject) {
		this.visitSubject = visitSubject;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
}



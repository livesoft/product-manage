package com.dayatang.product.webapp.action.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:UserPasswordSubmitAction.java
 * 描述:保存密码
 * 创建时间:2012-5-29 下午5:48:31
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class UserPasswordSubmitAction extends BaseAction {

	private static final long serialVersionUID = 1537985656229215065L;

	private PersonUser personUser;
	private String newPassword;
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "用户管理", option = "重置密码")
	public String execute() throws Exception {
		if (personUser.getId() > 0) {
			personUser = PersonUser.get(PersonUser.class, personUser.getId());
			securityApplication.resetPassword(personUser, newPassword);
			result = 1;
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public PersonUser getPersonUser() {
		return personUser;
	}

	public void setPersonUser(PersonUser personUser) {
		this.personUser = personUser;
	}

	@JSON(serialize = false)
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}



package com.dayatang.product.webapp.action.common;

import java.io.File;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.Attachment;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class CheckAttachmentExistAction extends BaseAction {

	private static final long serialVersionUID = 7721974310426171163L;

	private Attachment attachment;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		attachment = Attachment.get(Attachment.class, attachment.getId());
		File file = new File(attachment.getPath());
		if(file.exists()){
			result = 1;
		}else{
			result = 0;
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}

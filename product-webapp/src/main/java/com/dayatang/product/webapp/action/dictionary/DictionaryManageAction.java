package com.dayatang.product.webapp.action.dictionary;

import java.util.List;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.webapp.annotation.PMLog;

public class DictionaryManageAction extends DictionaryBaseAction {

	private static final long serialVersionUID = -4054169574018165017L;

	private DictionaryCategory category;
	
	@PMLog(moduleName = "系统设置", option ="数据字典")
	public String execute() throws Exception {
		navBar = "系统设置  → 数据字典";
		return SUCCESS;
	}
	
	public List<Dictionary> getDictionaries() {
		if (category == null) {
			return Dictionary.findAll(Dictionary.class);
		} else {
			return Dictionary.findByCategory(category);
		}
	}
	
	public DictionaryCategory getCategory() {
		return category;
	}

	public void setCategory(DictionaryCategory category) {
		this.category = category;
	}
	
}

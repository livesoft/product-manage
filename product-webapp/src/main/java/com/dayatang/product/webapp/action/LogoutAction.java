package com.dayatang.product.webapp.action;

import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionContext;

@Result(name = "success", type = "redirect", location = "index.action")
public class LogoutAction extends BaseAction {

	private static final long serialVersionUID = 1961269014074871783L;

	public String execute() throws Exception {
		ActionContext.getContext().getSession().put(LOGINUSER, null);
		return SUCCESS;
	}
}

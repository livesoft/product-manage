package com.dayatang.product.webapp.action.product;

import java.util.List;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.domain.Visit;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class ProductDetailsAction extends BaseAction {

	private static final long serialVersionUID = 1858005568445305381L;

	private ProductTrial productTrial;
	private RegisterInfo registerInfo;
	private List<Visit> visits;
	
	@PMLog(moduleName = "使用情况", option = "具体情况")
	public String execute() throws Exception {
		navBar = "产品管理  → 使用情况  → 详情";
		if (productTrial != null) {
			productTrial = ProductTrial.get(ProductTrial.class, productTrial.getId());
			registerInfo = RegisterInfo.getRegisterByTrial(productTrial);
			visits = Visit.findByProductTrialAlreadyVisit(productTrial);
		}
		return SUCCESS;
	}

	public ProductTrial getProductTrial() {
		return productTrial;
	}

	public void setProductTrial(ProductTrial productTrial) {
		this.productTrial = productTrial;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}

	public List<Visit> getVisits() {
		return visits;
	}
	
}

package com.dayatang.product.webapp.action.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.webapp.util.VerifyImageUtil;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件名:RandomVerifyAction.java
 * 描述:验证码生成类
 * 创建时间:2012-6-11 下午5:04:31
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RandomVerifyAction extends ActionSupport {

	private static final long serialVersionUID = 7303343989495444883L;

	private ByteArrayInputStream inputStream;
	private String sessionKey;

	@Action(results = {
			@Result(name = "success", type = "stream", params = {"inputName", "inputStream","contentType","image/jpeg"})
	})
	public String execute() throws Exception {
		BufferedImage image = VerifyImageUtil.getRandImage(sessionKey);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageOutputStream imageOut = ImageIO.createImageOutputStream(output);
		ImageIO.write(image, "JPEG", imageOut);
		imageOut.close();
		ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
		this.setInputStream(input);
		return SUCCESS;
	}
	
	public void setInputStream(ByteArrayInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ByteArrayInputStream getInputStream() {
		return inputStream;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
}



package com.dayatang.product.webapp.action.mail;

import com.dayatang.product.domain.HtmlMail;

public class HtmlMailDetailAction extends MailBaseAction {

	private static final long serialVersionUID = -3695539588506254893L;

	private HtmlMail htmlMail;
	
	public String execute() throws Exception {
		if (htmlMail.getId() > 0) {
			htmlMail = HtmlMail.get(HtmlMail.class, htmlMail.getId());
		}
		return SUCCESS;
	}

	public HtmlMail getHtmlMail() {
		return htmlMail;
	}

	public void setHtmlMail(HtmlMail htmlMail) {
		this.htmlMail = htmlMail;
	}
	
}

package com.dayatang.product.webapp.action.product;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class ProductMarkCheckAction extends BaseAction {

	private static final long serialVersionUID = 1415996492732964594L;

	private String mark;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (mark != null) {
			for (Product product : getProducts()) {
				if (product.getMark().equals(mark)) {
					result = ONE;
				}
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}

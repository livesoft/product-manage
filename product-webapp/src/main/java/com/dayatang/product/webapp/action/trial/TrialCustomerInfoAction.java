package com.dayatang.product.webapp.action.trial;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.webapp.action.BaseAction;

@ParentPackage(value = "web")
public class TrialCustomerInfoAction extends BaseAction {

	private static final long serialVersionUID = 5885533933325508269L;

	private String professionType;
	
	private List<CustomerInfo> customerInfos;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (professionType != null) {
			customerInfos = CustomerInfo.findByProfessionType(professionType);
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getProfessionType() {
		return professionType;
	}

	public void setProfessionType(String professionType) {
		this.professionType = professionType;
	}

	public List<CustomerInfo> getCustomerInfos() {
		return customerInfos;
	}

	public void setCustomerInfos(List<CustomerInfo> customerInfos) {
		this.customerInfos = customerInfos;
	}
	
}

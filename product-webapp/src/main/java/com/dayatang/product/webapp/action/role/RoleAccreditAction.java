package com.dayatang.product.webapp.action.role;

import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:RoleAccreditAction.java
 * 描述:角色授权
 * 创建时间:2012-7-3 下午10:09:38
 * 创建者:mluo
 * 版本号:v2.0
 */
public class RoleAccreditAction extends BaseAction {

	private static final long serialVersionUID = 6505952689655985721L;

	private Role role;
	
	public String execute() throws Exception {
		if (role != null) {
			role = Role.get(Role.class, role.getId());
		}
		return SUCCESS;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}

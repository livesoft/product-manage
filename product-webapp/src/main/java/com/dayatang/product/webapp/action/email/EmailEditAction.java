package com.dayatang.product.webapp.action.email;

import com.dayatang.product.domain.EmailInfo;
import com.dayatang.product.webapp.action.BaseAction;

public class EmailEditAction extends BaseAction {

	private static final long serialVersionUID = 3223933139030532395L;

	private EmailInfo emailInfo;
	
	public String execute() throws Exception {
		if (emailInfo != null) {
			emailInfo = EmailInfo.get(EmailInfo.class, emailInfo.getId());
			navBar = "系统设置  → 邮箱管理  → 编辑邮箱";
		} else {
			navBar = "系统设置  → 邮箱管理  → 新增邮箱";
		}
		return SUCCESS;
	}

	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}
	
}

package com.dayatang.product.webapp.action.mail;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.RegisterInfo;

@ParentPackage(value = "web")
public class GetRegisterInfoAction extends MailBaseAction {

	private static final long serialVersionUID = 1172781549493376873L;

	private long productId;
	
	private List<RegisterInfo> registers;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (productId > 0 ) {
			Product product = Product.get(Product.class, productId);
			registers = new ArrayList<RegisterInfo>();
			for (ProductTrial productTrial : product.getProductTrials()) {
				RegisterInfo register = RegisterInfo.getRegisterByTrial(productTrial);
				if (register != null) {
					registers.add(register);
				}
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public List<RegisterInfo> getRegisters() {
		return registers;
	}

	public void setRegisters(List<RegisterInfo> registers) {
		this.registers = registers;
	}
	
}

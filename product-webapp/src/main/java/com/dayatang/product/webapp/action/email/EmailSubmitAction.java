package com.dayatang.product.webapp.action.email;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.EmailInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/email/email-manage.action")
@ParentPackage(value = "web")
public class EmailSubmitAction extends BaseAction {

	private static final long serialVersionUID = 8739262860547926455L;

	private EmailInfo emailInfo;
	
	private int result;
	
	@PMLog(moduleName = "邮箱管理", option ="添加邮箱")
	public String execute() throws Exception {
		if (emailInfo != null) {
			productApplication.saveEntity(emailInfo);
		}
		return SUCCESS;
	}

	@Action(value = "/email/email-remove", results = { @Result(name = "success", type = "json")})
	@PMLog(moduleName = "邮箱管理", option ="删除邮箱")
	public String removeEmailInfo() {
		if (emailInfo.getId() > 0) {
			try {
				productApplication.removeEntity(EmailInfo.get(EmailInfo.class, emailInfo.getId()));
			} catch (DataIntegrityViolationException e) {
				result = 1;
				return SUCCESS;
			}
		}
		return SUCCESS;
	}
	
	@JSON(serialize = false)
	public EmailInfo getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(EmailInfo emailInfo) {
		this.emailInfo = emailInfo;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}

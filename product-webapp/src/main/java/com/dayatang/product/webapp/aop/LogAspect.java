package com.dayatang.product.webapp.aop;

import java.lang.reflect.Method;
import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.dayatang.product.log.UserLog;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:LogAdvice.java
 * 描述:日志切面
 * 创建时间:2012-6-14 下午3:52:40
 * 创建者:mluo
 * 版本号:v2.0
 */
@Aspect
@Component
public class LogAspect extends BaseAction {
	
	private static final long serialVersionUID = 3344609953473750580L;

	@SuppressWarnings("unused")
	@Pointcut(" execution(* com.dayatang.product.webapp.action.**.*Action.*()) && " +   
            "!execution(* com.dayatang.product.webapp.action.**.*Action.set*()) && " +   
            "!execution(* com.dayatang.product.webapp.action.**.*Action.get*())")  
	private void anyMothed(){} 
	
	@Before("anyMothed()")
    public void doActionClassProfilling(JoinPoint  jp) throws Throwable {     
        Method method = getMethod(jp); 
        PMLog pmLog = method.getAnnotation(PMLog.class);
        if (pmLog != null) {
        	UserLog log = new UserLog();
        	log.setModuleName(pmLog.moduleName());
        	log.setOperate(pmLog.option());
        	log.setOptionDate(new Date());
        	log.setUsername(getPersonUser().getUsername());
        	log.setName(getPersonUser().getPerson().getName());
        	logApplication.saveEntity(log);
        }
    }  
	
	private Method getMethod(JoinPoint jp) throws NoSuchMethodException  
    {  
        Signature sig = jp.getSignature();  
        MethodSignature msig = (MethodSignature) sig;  
        return getClass(jp).getMethod(msig.getName(),  
                msig.getParameterTypes());  
    }  
      
    private Class<? extends Object> getClass(JoinPoint jp) throws NoSuchMethodException{  
        return jp.getTarget().getClass();  
    } 
	
}



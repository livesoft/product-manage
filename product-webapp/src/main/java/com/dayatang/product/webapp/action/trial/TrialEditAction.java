package com.dayatang.product.webapp.action.trial;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dayatang.product.domain.CustomerInfo;
import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.domain.DictionaryCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.webapp.action.BaseAction;

public class TrialEditAction extends BaseAction {

	private static final long serialVersionUID = -2476594333145963622L;

	private ProductTrial trial;
	private Product product;
	
	public String execute() throws Exception {
		navBar = "试用号管理  → 新增试用号";
		return SUCCESS;
	}

	public ProductTrial getTrial() {
		return trial;
	}

	public void setTrial(ProductTrial trial) {
		this.trial = trial;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Map<Long,String> getProductMaps() {
		Map<Long,String> productMaps = new HashMap<Long, String>();
		for (Product product : getProducts()) {
			productMaps.put(product.getId(), product.getName());
		}
		return productMaps;
	}
	
	public List<Dictionary> getDictionarys() {
		return Dictionary.findByCategory(DictionaryCategory.PROFESSION_TYPE);
	}
	
	public Map<Dictionary,List<CustomerInfo>> getCustomerMaps() {
		Map<Dictionary,List<CustomerInfo>> maps = new HashMap<Dictionary, List<CustomerInfo>>();
		for (Dictionary dictionary : getDictionarys()) {
			maps.put(dictionary, CustomerInfo.findByDictionary(dictionary));
		}
		return maps;
	}
}

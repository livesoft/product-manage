package com.dayatang.product.webapp.action.feedback;

import java.util.List;

import com.dayatang.product.domain.FeedbackClient;
import com.dayatang.product.domain.MobileClientCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class FeedbackClientManage extends BaseAction {

	private static final long serialVersionUID = -1360843967348007802L;

	private MobileClientCategory category;
	private Product product;
	private List<FeedbackClient> feedbackClients;
	
	@PMLog(moduleName = "反馈信息管理", option = "客户端反馈")
	public String execute() throws Exception {
		feedbackClients = FeedbackClient.getFeedbackClientsByProductAndCategory(product, category);
		navBar = "反馈信息管理  → 客户端反馈";
		return SUCCESS;
	}
	
	public List<FeedbackClient> getFeedbackClients() {
		return feedbackClients;
	}

	public MobileClientCategory getCategory() {
		return category;
	}

	public void setCategory(MobileClientCategory category) {
		this.category = category;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public MobileClientCategory[] getCategories() {
		return MobileClientCategory.values();
	}
}

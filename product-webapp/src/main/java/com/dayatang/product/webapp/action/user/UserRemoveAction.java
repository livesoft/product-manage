package com.dayatang.product.webapp.action.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.dayatang.product.domain.PersonUser;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

/**
 * 文件名:UserRemoveAction.java
 * 描述:删除用户
 * 创建时间:2012-5-29 下午3:03:55
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class UserRemoveAction extends BaseAction {

	private static final long serialVersionUID = 7644149834251139597L;

	private PersonUser personUser;
	
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName = "用户管理", option = "删除用户")
	public String execute() throws Exception {
		if (personUser.getId() > 0) {
			try {
				personUser = PersonUser.get(PersonUser.class, personUser.getId());
				productApplication.removeEntity(personUser);
			} catch (DataIntegrityViolationException e) {
				result = ONE;
			}
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public PersonUser getPersonUser() {
		return personUser;
	}

	public void setPersonUser(PersonUser personUser) {
		this.personUser = personUser;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
}



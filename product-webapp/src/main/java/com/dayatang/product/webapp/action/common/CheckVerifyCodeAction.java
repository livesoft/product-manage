package com.dayatang.product.webapp.action.common;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.json.annotations.JSON;

import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:CheckVerifyCodeAction.java
 * 描述:检测验证码
 * 创建时间:2012-6-11 下午5:10:01
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class CheckVerifyCodeAction extends BaseAction {

	private static final long serialVersionUID = -9059406877686543814L;

	private String code;
	private int result;
	private String sessionKey;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (code.equals(getSessinoValue(sessionKey))) {
			result = ONE;
		} else {
			result = ZERO;
		}
		return SUCCESS;
	}

	@JSON(serialize = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@JSON(serialize = false)
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

}



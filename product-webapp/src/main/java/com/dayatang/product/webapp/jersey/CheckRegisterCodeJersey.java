package com.dayatang.product.webapp.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.RegisterCode;
import com.dayatang.product.domain.TrialCode;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/check-register-code/{registerCode}")
@Component
@Scope("request")
public class CheckRegisterCodeJersey extends BaseAction {

	private static final long serialVersionUID = 398982404685207637L;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String checkRegisterCode(@PathParam("registerCode") String registerCode) {

		ProductTrial productTrial = ProductTrial.getProductTrialByRegisterCode(registerCode);
		if (productTrial != null) {
			TrialCode trialCode = productTrial.getTrialCode();
			RegisterCode code = productTrial.getRegisterCode();
			if (code == null || registerCode == null) {
				return REGISTERCODENOTEXIST;
			} else if (code.getIsActivate()) {
				return CODEINVALID;
			} else if (code.getTrialDay() > 0) {
				return trialCode.getTrialNum() + "." + code.getRegisterCode() + "." + code.getTrialDay();
			}
		}
		return REGISTERCODENOTEXIST;

	}

}

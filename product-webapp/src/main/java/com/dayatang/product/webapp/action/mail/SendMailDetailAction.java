package com.dayatang.product.webapp.action.mail;

import com.dayatang.product.domain.SendMail;

public class SendMailDetailAction extends MailBaseAction {

	private static final long serialVersionUID = -1568527220247686767L;

	private SendMail sendMail;
	
	public String execute() throws Exception {
		if (sendMail.getId() > 0) {
			sendMail = SendMail.get(SendMail.class, sendMail.getId());
		}
		return SUCCESS;
	}

	public SendMail getSendMail() {
		return sendMail;
	}

	public void setSendMail(SendMail sendMail) {
		this.sendMail = sendMail;
	}
	
}

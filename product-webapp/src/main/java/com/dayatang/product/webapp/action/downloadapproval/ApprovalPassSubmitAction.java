package com.dayatang.product.webapp.action.downloadapproval;

import javax.inject.Inject;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;
import com.dayatang.product.webapp.executor.RegisterCodeMailTaskExecutor;

/**
 * 文件名:ApprovalPassSubmitAction.java
 * 描述:审批通过,走试用流程
 * 创建时间:2012-6-5 下午2:55:13
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class ApprovalPassSubmitAction extends BaseAction {

	private static final long serialVersionUID = -391905334090538190L;

	private long trialPeriod;
	private long validity;
	private DownloadApproval approval;
	
	@Action(results = {@Result(name = "success", type = "json")})
	@PMLog(moduleName="试用审批",option="审批通过")
	public String execute() throws Exception {
		if (approval != null) {
			approval = DownloadApproval.get(DownloadApproval.class, approval.getId());
			RegisterInfo registerInfo = productApplication.approvalPass(approval, trialPeriod, validity);
			registerCodeMailTaskExecutor.sendMail(registerInfo.getProductTrial().getRegisterCode().getRegisterCode(),registerInfo,HtmlMail.get(HtmlMail.class, Long.valueOf(1)));
		}
		return SUCCESS;
	}

	public long getTrialPeriod() {
		return trialPeriod;
	}

	public void setTrialPeriod(long trialPeriod) {
		this.trialPeriod = trialPeriod;
	}

	public DownloadApproval getApproval() {
		return approval;
	}

	public void setApproval(DownloadApproval approval) {
		this.approval = approval;
	}

	public long getValidity() {
		return validity;
	}

	public void setValidity(long validity) {
		this.validity = validity;
	}
	
	@Inject
	private RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor;

	public void setRegisterCodeMailTaskExecutor(RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor) {
		this.registerCodeMailTaskExecutor = registerCodeMailTaskExecutor;
	}
	
}



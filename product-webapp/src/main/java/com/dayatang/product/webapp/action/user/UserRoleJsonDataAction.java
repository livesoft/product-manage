package com.dayatang.product.webapp.action.user;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:UserRoleJsonDataAction.java
 * 描述:加载角色
 * 创建时间:2012-6-7 下午4:12:25
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class UserRoleJsonDataAction extends BaseAction {

	private static final long serialVersionUID = 1815064565493891122L;

	private long userId;
	private List<Role> results;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		results = Role.findAll(Role.class);
		return SUCCESS;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<Role> getResults() {
		return results;
	}

	public void setResults(List<Role> results) {
		this.results = results;
	}
	
}



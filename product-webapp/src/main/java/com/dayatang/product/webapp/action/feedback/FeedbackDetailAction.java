package com.dayatang.product.webapp.action.feedback;

import com.dayatang.product.domain.FeedbackWeb;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class FeedbackDetailAction extends BaseAction {

	private static final long serialVersionUID = 4104979916259316735L;

	private FeedbackWeb feedbackWeb;
	
	@PMLog(moduleName = "反馈信息", option = "信息详情")
	public String execute() throws Exception {
		if (feedbackWeb.getId() > 0) {
			feedbackWeb = FeedbackWeb.get(FeedbackWeb.class, feedbackWeb.getId());
		}
		navBar = "反馈信息管理  → 反馈信息  → 信息详情";
		return SUCCESS;
	}

	public FeedbackWeb getFeedbackWeb() {
		return feedbackWeb;
	}

	public void setFeedbackWeb(FeedbackWeb feedbackWeb) {
		this.feedbackWeb = feedbackWeb;
	}
	
}

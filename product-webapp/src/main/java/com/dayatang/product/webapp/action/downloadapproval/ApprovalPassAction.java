package com.dayatang.product.webapp.action.downloadapproval;

import com.dayatang.product.domain.DownloadApproval;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:ApprovalPassAction.java
 * 描述:审批通过
 * 创建时间:2012-6-5 下午2:32:19
 * 创建者:mluo
 * 版本号:v2.0
 */
public class ApprovalPassAction extends BaseAction {

	private static final long serialVersionUID = -5715727814656902082L;

	private DownloadApproval approval;
	
	public String execute() throws Exception {
		if (approval != null) {
			approval = DownloadApproval.get(DownloadApproval.class, approval.getId());
		}
		return SUCCESS;
	}

	public DownloadApproval getApproval() {
		return approval;
	}

	public void setApproval(DownloadApproval approval) {
		this.approval = approval;
	}

}



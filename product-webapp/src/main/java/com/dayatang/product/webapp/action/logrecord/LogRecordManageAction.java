package com.dayatang.product.webapp.action.logrecord;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.dayatang.product.log.UserLog;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:LogRecordManageAction.java
 * 描述:日志记录管理
 * 创建时间:2012-6-30 上午1:02:00
 * 创建者:mluo
 * 版本号:v2.0
 */
public class LogRecordManageAction extends BaseAction {

	private static final long serialVersionUID = -4790521293559530721L;

	private List<UserLog> logs;
	private Date fromDate;
	private Date toDate;
	private DateFormat format= new SimpleDateFormat("yyyy-MM-dd");
	
	public String execute() throws Exception {
		navBar = "系统设置  → 日志记录";
		logs = logApplication.findAll(page,fromDate,toDate);
		if (fromDate != null && toDate != null) {
			page.setUrl("log-record-manage.action?fromDate=" + format.format(fromDate) + "&toDate=" + format.format(toDate) + "&");
		} else {
			page.setUrl("log-record-manage.action?");
		}
		return SUCCESS;
	}

	public List<UserLog> getLogs() {
		return logs;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
}

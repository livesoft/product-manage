package com.dayatang.product.webapp.components;

import java.io.IOException;
import java.io.Writer;

import org.apache.struts2.components.Component;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * 文件名:Pages.java
 * 描述:分页逻辑Bean
 * 创建时间:2012-6-21 下午3:36:25
 * 创建者:mluo
 * 版本号:v2.0
 */
public class Pages extends Component {
	
	private String currentPage;   
	private String total;   
	private String url;
	
	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Pages(ValueStack stack) {
		super(stack);
	}

	public boolean start(Writer writer) {
		boolean result = super.start(writer);  
		
		StringBuilder str = new StringBuilder();   
		boolean isValid = true;
			  
		 // 从ValueStack中取出数值  
        if (isValid) {  
            if (total.startsWith("%{") && total.endsWith("}")) {  
                total = total.substring(2, total.length() - 1);  
                total = String.valueOf(this.getStack().findValue(total));  
                isValid = total == null ? false : true;  
            } else {  
                isValid = false;  
            }  
        }  
        if (isValid) {  
            if (currentPage.startsWith("%{") && currentPage.endsWith("}")) {  
                currentPage = currentPage.substring(2, currentPage.length() - 1);  
                currentPage = String.valueOf(this.getStack().findValue(currentPage));  
                isValid = currentPage == null ? false : true;  
            } else {  
                isValid = false;  
            }  
        }  
        if (isValid) {  
            if (url.startsWith("%{") && url.endsWith("}")) {  
                url = url.substring(2, url.length() - 1);  
                url = String.valueOf(this.getStack().findValue(url));  
                isValid = url == null ? false : true;  
            } else {  
                isValid = false;  
            }  
        }  
        
        if (isValid) {  
        	Integer currentPageInt = Integer.valueOf(currentPage);
        	str.append("<span>");
        	
        	if (currentPage.equals(total)) {  
        		// 没有数据,不显示分页
            	if ("0".equals(total)) {
            		
            	}
                // 如果total = 1，则无需分页，显示“[第1页] [共1页]”  
            	else if ("1".equals(total)) {  
                    str.append("[第 " + currentPageInt + " 页]");  
                    str.append("&nbsp;[共 " + total + " 页]");  
                } else {  
                    // 到达最后一页,显示“[首页] [上一页] [末页]”  
                    str.append("<a href='");  
                    str.append(url);  
                    str.append("page.cpage=1&page.total=" + total);  
                    str.append("'>[首页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + (currentPageInt - 1) + "&page.total=" + total);  
                    str.append("'>[上一页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + total + "&page.total=" + total);  
                    str.append("'>[末页]</a>");  
                }  
            } else {  
            	// 当前页与总页数不相同  
            	if ("1".equals(currentPage)) {  
                    // 第一页，显示“[首页] [下一页] [末页]”  
                    str.append("<a href='");  
                    str.append(url);  
                    str.append("page.cpage=1&page.total=" + total);
                    str.append("'>[首页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + (currentPageInt + 1) + "&page.total=" + total);  
                    str.append("'>[下一页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + total + "&page.total=" + total );  
                    str.append("'>[末页]</a>");  
                } else {  
                    // 不是第一页，显示“[首页] [上一页] [下一页] [末页]”  
                    str.append("<a href='");  
                    str.append(url);  
                    str.append("page.cpage=1&page.total=" + total);  
                    str.append("'>[首页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + (currentPageInt - 1) + "&page.total=" + total);  
                    str.append("'>[上一页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + (currentPageInt + 1) + "&page.total=" + total);  
                    str.append("'>[下一页]</a> <a href='");  
                    str.append(url);  
                    str.append("page.cpage=" + total + "&page.total=" + total);  
                    str.append("'>[末页]</a>");  
                }  
            }
        	str.append("</span>");  
            try {
				writer.write(str.toString());
			} catch (IOException e) {
				e.printStackTrace();
			} 
        }
		
		return result;
	}
	
	
}



package com.dayatang.product.webapp.jersey;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.InfoCategory;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.executor.RegisterCodeMailTaskExecutor;

@Path("/get-register-code")
@Component
@Scope("request")
public class GetRegisterCodeJersey extends BaseAction {

	private static final long serialVersionUID = -3375497849962102280L;

	@QueryParam("trialCode") String trialCode;
	@QueryParam("companyName") String companyName;
	@QueryParam("phone") String phone;
	@QueryParam("email") String email;
	@QueryParam("contact") String contact;
	@QueryParam("address") String address;
	@QueryParam("mobile") String mobile;
	
	@GET
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public String getRegisterCode() {
		
		ProductTrial productTrial = ProductTrial.getProductTrialByTrialCode(trialCode);
		
		if (trialCode != null && productTrial != null) {
			
			RegisterInfo register = new RegisterInfo();
			register.setCompanyName(companyName);
			register.setPhone(phone);
			register.setEmail(email);
			register.setContact(contact);
			register.setAddress(address);
			register.setMobile(mobile);
			register.setCategory(InfoCategory.REGISTERINFO);
			register.setProductTrial(productTrial);
			
			productApplication.obtainRegisterCode(productTrial.getTrialCode(), register);
			
			registerCodeMailTaskExecutor.sendMail(productTrial.getRegisterCode().getRegisterCode(), register, HtmlMail.get(HtmlMail.class, Long.valueOf(1)));
			
			return MAILSENDSUCCESS;
		}
		return ERROR;
	}
	
	@Inject
	private RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor;

	public void setRegisterCodeMailTaskExecutor(RegisterCodeMailTaskExecutor registerCodeMailTaskExecutor) {
		this.registerCodeMailTaskExecutor = registerCodeMailTaskExecutor;
	}
	
}

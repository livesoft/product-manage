package com.dayatang.product.webapp.action.trial;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/trial/trial-manage.action")
public class TrialBatchSubmitAction extends BaseAction {

	private static final long serialVersionUID = 5342709836979705568L;

	private Product product;
	
	private long validity;
	private long trialPeriod;
	private long[] customers;
	
	@PMLog(moduleName = "试用号管理", option = "批量生成试用号")
	public String execute() throws Exception {
		if (product.getId() > 0) {
			product = Product.get(Product.class, product.getId());
			productApplication.batchCreateProductTrial(product,customers,validity,trialPeriod);
		}
		return SUCCESS;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getValidity() {
		return validity;
	}

	public void setValidity(long validity) {
		this.validity = validity;
	}

	public long[] getCustomers() {
		return customers;
	}

	public void setCustomers(long[] customers) {
		this.customers = customers;
	}

	public long getTrialPeriod() {
		return trialPeriod;
	}

	public void setTrialPeriod(long trialPeriod) {
		this.trialPeriod = trialPeriod;
	}
	
}


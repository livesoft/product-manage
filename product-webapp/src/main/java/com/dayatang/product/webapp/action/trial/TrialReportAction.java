package com.dayatang.product.webapp.action.trial;

import com.dayatang.product.webapp.action.common.ProductStateDataBaseAction;

public class TrialReportAction extends ProductStateDataBaseAction {

	private static final long serialVersionUID = -8187542992322355127L;

	public String execute() throws Exception {
		loadData();
		navBar = "试用号管理  → 导出试用号";
		return SUCCESS;
	}
	
}


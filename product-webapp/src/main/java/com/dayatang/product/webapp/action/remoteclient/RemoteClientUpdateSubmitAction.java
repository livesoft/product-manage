package com.dayatang.product.webapp.action.remoteclient;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.dayatang.product.domain.ProductMobileClient;
import com.dayatang.product.webapp.action.mail.AttachmentBaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

public class RemoteClientUpdateSubmitAction extends AttachmentBaseAction {

	private static final long serialVersionUID = -1368011456921038132L;

	private ProductMobileClient productMobileClient;
	private List<File> files; // 多附件
	private List<String> filesFileName; // 附件名称
	
	private String clientDescribe;
	private long clientVersion;

	private HttpServletResponse response = ServletActionContext.getResponse();
	
	@PMLog(moduleName="手机客户端",option="更新手机客户端")
	public String execute() throws Exception {
		
		if (productMobileClient.getId() > 0) {
			productMobileClient = ProductMobileClient.get(ProductMobileClient.class, productMobileClient.getId());
			productMobileClient.setClientDescribe(clientDescribe);
			productMobileClient.setClientVersion(clientVersion);
			productApplication.saveEntity(productMobileClient);
		}
		
		if (files != null ) {
			String root = getUploadDir() + "/remoteclient/id" + productMobileClient.getId() + "/";
			productApplication.updateMobileClient(productMobileClient, uploadFiles(root, files, filesFileName));
		}
		
		response.setCharacterEncoding(getCharset());
		response.getWriter().print("保存成功！");
		
		return null;
	}

	public ProductMobileClient getProductMobileClient() {
		return productMobileClient;
	}

	public void setProductMobileClient(ProductMobileClient productMobileClient) {
		this.productMobileClient = productMobileClient;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public List<String> getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(List<String> filesFileName) {
		this.filesFileName = filesFileName;
	}

	public String getClientDescribe() {
		return clientDescribe;
	}

	public void setClientDescribe(String clientDescribe) {
		this.clientDescribe = clientDescribe;
	}

	public long getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(long clientVersion) {
		this.clientVersion = clientVersion;
	}
	
}

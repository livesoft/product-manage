package com.dayatang.product.webapp.quartz;
/**
 * 文件名:CheckActivateTrialQuartz.java
 * 描述:
 * 创建时间:2012-7-6 上午10:03:45
 * 创建者:mluo
 * 版本号:v2.0
 */
public class CheckActivateTrialQuartz extends BaseQuartz {
	
	public void checkActivateTrial() {
		productApplication.checkActivateTrail();
	}
	
}



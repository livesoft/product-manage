package com.dayatang.product.webapp.executor;

import com.dayatang.product.domain.HtmlMail;
import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.util.MailUtil;

public class RegisterCodeMailTaskExecutor extends BaseTaskExecutor {
	
	private class RegisterCodeMailTask implements Runnable {

		private String registerCode;
		private RegisterInfo registerInfo;
		private HtmlMail htmlMail;
		
		public RegisterCodeMailTask(String registerCode, RegisterInfo registerInfo, HtmlMail htmlMail) {
			this.registerCode = registerCode;
			this.registerInfo = registerInfo;
			this.htmlMail = htmlMail;
		}

		public void run() {
			MailUtil.sendMail(registerCode,registerInfo,htmlMail);
		}
		
	}
	
    public void sendMail(String registerCode, RegisterInfo registerInfo, HtmlMail htmlMail) {
    	if (registerCode != null && registerInfo != null && htmlMail != null) {
    		taskExecutor.execute(new RegisterCodeMailTask(registerCode,registerInfo,htmlMail));
    	}
    }

	
	
}

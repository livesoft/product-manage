package com.dayatang.product.webapp.action.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dayatang.domain.QuerySettings;
import com.dayatang.product.domain.Product;
import com.dayatang.product.domain.ProductTrial;
import com.dayatang.product.domain.TrialStatus;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:ProductStateDataBaseAction.java
 * 描述:产品状态数据基类
 * 创建时间:2012-6-27 下午2:40:03
 * 创建者:mluo
 * 版本号:v2.0
 */
public class ProductStateDataBaseAction extends BaseAction {

	private static final long serialVersionUID = 2786401909949139967L;

	protected TrialStatus status;
	
	protected Product product;
	
	protected List<ProductTrial> productTrials;
	
	public void loadData() {
		
		QuerySettings<ProductTrial> querySettings = QuerySettings.create(ProductTrial.class);
		
		if (product != null && status != null) {
			product = Product.get(Product.class, product.getId());
			querySettings.eq("product", product).eq("status", status);
		} else if (product != null) {
			product = Product.get(Product.class, product.getId());
			querySettings.eq("product", product);
		} else if (status != null) {
			querySettings.eq("status", status);
		} else {
			querySettings.desc("trialCode.registerDate");
		}
		
		productTrials = ProductTrial.findByQuerySettings(querySettings);
	}
	
	public TrialStatus getStatus() {
		return status;
	}

	public void setStatus(TrialStatus status) {
		this.status = status;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<ProductTrial> getProductTrials() {
		return productTrials;
	}

	public void setProductTrials(List<ProductTrial> productTrials) {
		this.productTrials = productTrials;
	}

	public Map<TrialStatus, String> getStatusMap() {
		Map<TrialStatus, String> map = new HashMap<TrialStatus, String>();
		for (TrialStatus status : TrialStatus.values()) {
			map.put(status, getText(status.toString()));
		}
		return map;
	}

}



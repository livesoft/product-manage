package com.dayatang.product.webapp.jersey;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.FeedbackClient;
import com.dayatang.product.domain.MobileClientCategory;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/feedback-client-info")
@Component
@Scope("request")
public class FeedbackClientInfoJersey extends BaseAction {

	private static final long serialVersionUID = -8362426067407422221L;
	
	@QueryParam("mark") String mark;
	@QueryParam("category") MobileClientCategory category;
	@QueryParam("feedbackContent") String feedbackContent;
	@QueryParam("feedbackPerson") String feedbackPerson;
	
	@GET
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public String feedbackClientInfo() {
		if (mark != null && category != null) {
			FeedbackClient feedbackClient = new FeedbackClient();
			feedbackClient.setProduct(Product.getProductByRemark(mark));
			feedbackClient.setCategory(category);
			feedbackClient.setFeedbackContent(feedbackContent);
			feedbackClient.setFeedbackPerson(feedbackPerson);
			feedbackClient.setFeedbackDate(new Date());
			productApplication.saveEntity(feedbackClient);
			return OK;
		}
		return ERROR;
	}
	
}

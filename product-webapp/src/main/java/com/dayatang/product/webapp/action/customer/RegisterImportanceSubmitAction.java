package com.dayatang.product.webapp.action.customer;

import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.RegisterInfo;
import com.dayatang.product.webapp.action.BaseAction;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/customer/register-info-manage.action")
public class RegisterImportanceSubmitAction extends BaseAction {

	private static final long serialVersionUID = 2689420285510611758L;

	private RegisterInfo registerInfo;
	
	private long importance;
	
	@PMLog(moduleName = "注册信息", option = "修改客户关注度")
	public String execute() throws Exception {
		if (registerInfo.getId() > 0 && importance > 0) {
			registerInfo = RegisterInfo.get(RegisterInfo.class, registerInfo.getId());
			registerInfo.setImportance(importance);
			productApplication.saveEntity(registerInfo);
		}
		return SUCCESS;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}

	public long getImportance() {
		return importance;
	}

	public void setImportance(long importance) {
		this.importance = importance;
	}

}

package com.dayatang.product.webapp.components.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.dayatang.product.webapp.components.Pages;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * 文件名:PageTag.java
 * 描述:分页标签
 * 创建时间:2012-6-21 下午4:06:06
 * 创建者:mluo
 * 版本号:v2.0
 */
public class PageTag extends ComponentTagSupport {

	private static final long serialVersionUID = 6108957046839655727L;

	private String currentPage;   
	private String total;   
	private String url;
	
	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Component getBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
		return new Pages(stack);
	}
	
	protected void populateParams() {
		super.populateParams();
		Pages pages = (Pages) component;
		pages.setCurrentPage(currentPage);
		pages.setTotal(total);
		pages.setUrl(url);
	}

}



package com.dayatang.product.webapp.action.dictionary;

import com.dayatang.product.domain.Dictionary;

public class DictionaryEditAction extends DictionaryBaseAction {

	private static final long serialVersionUID = -8124651885247493703L;
	
	private Dictionary dictionary;
	
	public String execute() throws Exception {
		navBar = "系统设置  → 数据字典  → 新增字典";
		if (dictionary != null) {
			dictionary = Dictionary.get(Dictionary.class, dictionary.getId());
		}
		return SUCCESS;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}

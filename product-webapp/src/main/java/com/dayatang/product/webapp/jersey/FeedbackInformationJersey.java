package com.dayatang.product.webapp.jersey;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dayatang.product.domain.FeedbackWeb;
import com.dayatang.product.domain.Product;
import com.dayatang.product.webapp.action.BaseAction;

@Path("/feedback-information")
@Component
@Scope("request")
public class FeedbackInformationJersey extends BaseAction {

	private static final long serialVersionUID = 8978642111515401734L;
	
	@QueryParam("help") String help;
	@QueryParam("easy") String easy;
	@QueryParam("beautiful") String beautiful;
	@QueryParam("problem") String problem;
	@QueryParam("wholeEvaluate") String wholeEvaluate;
	@QueryParam("suggestion") String suggestion;
	@QueryParam("companyName") String companyName;
	@QueryParam("email") String email;
	@QueryParam("phone") String phone;
	@QueryParam("address") String address;
	@QueryParam("mark") String mark;

	@GET
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public String feedbackInfo() {
		if (mark != null) {
			FeedbackWeb feedbackWeb = new FeedbackWeb();
			feedbackWeb.setProduct(Product.getProductByRemark(mark));
			feedbackWeb.setHelp(help);
			feedbackWeb.setEasy(easy);
			feedbackWeb.setBeautiful(beautiful);
			feedbackWeb.setProblem(problem);
			feedbackWeb.setWholeEvaluate(wholeEvaluate);
			feedbackWeb.setSuggestion(suggestion);
			feedbackWeb.setCompanyName(companyName);
			feedbackWeb.setEmail(email);
			feedbackWeb.setPhone(phone);
			feedbackWeb.setAddress(address);
			feedbackWeb.setFeedbackDate(new Date());
			productApplication.saveEntity(feedbackWeb);
			return OK;
		}
		return ERROR;
	}
	
}

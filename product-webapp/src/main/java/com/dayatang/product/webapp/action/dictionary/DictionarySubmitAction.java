package com.dayatang.product.webapp.action.dictionary;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Dictionary;
import com.dayatang.product.webapp.annotation.PMLog;

@Result(name = "success", type = "redirect", location = "/dictionary/dictionary-manage.action", params = {"category","${dictionary.category}"})
public class DictionarySubmitAction extends DictionaryBaseAction {

	private static final long serialVersionUID = 1706940317031890975L;

	private Dictionary dictionary;
	
	@PMLog(moduleName = "数据字典", option ="添加字典")
	public String execute() throws Exception {
		if (dictionary != null) {
			productApplication.saveEntity(dictionary);
		}
		return SUCCESS;
	}

	@Action(value = "/dictionary/dictionary-remove")
	@PMLog(moduleName = "数据字典", option ="删除字典")
	public String removeDictionary() {
		if (dictionary != null) {
			dictionary = Dictionary.get(Dictionary.class, dictionary.getId());
			productApplication.removeEntity(dictionary);
		}
		return SUCCESS;
	}
	
	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	
}

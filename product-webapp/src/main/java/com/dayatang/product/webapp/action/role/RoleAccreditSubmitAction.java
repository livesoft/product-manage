package com.dayatang.product.webapp.action.role;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.product.domain.Permission;
import com.dayatang.product.domain.Role;
import com.dayatang.product.webapp.action.BaseAction;

/**
 * 文件名:RoleAccreditSubmitAction.java
 * 描述:提交角色授权
 * 创建时间:2012-7-4 上午9:42:24
 * 创建者:mluo
 * 版本号:v2.0
 */
@ParentPackage(value = "web")
public class RoleAccreditSubmitAction extends BaseAction {

	private static final long serialVersionUID = -5201476638992903814L;

	private Role role;
	private String permissions;
	private int result;
	
	@Action(results = {@Result(name = "success", type = "json")})
	public String execute() throws Exception {
		if (permissions != null && role != null) {
			role = Role.get(Role.class, role.getId());
			securityApplication.givePermissionToRole(role,getPermissionsByArray(permissions));
			result = ONE;
		}
		return SUCCESS;
	}
	
	private List<Permission> getPermissionsByArray(String strArray) {
		List<Permission> permissions = new ArrayList<Permission>();
		for (String str : strArray.split(":")) {
			permissions.add(Permission.get(Permission.class, Long.valueOf(str)));
		}
		return permissions;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public int getResult() {
		return result;
	}

}



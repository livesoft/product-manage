package com.dayatang.product.webapp.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.dayatang.product.application.ProductApplication;
import com.dayatang.utils.ConfigurationFileImpl;
import com.dayatang.utils.WritableConfiguration;

@Configuration
@EnableTransactionManagement
public class ApplicationConfig {
	
	@Bean
	public ProductApplication productApplication() {
		return new com.dayatang.product.application.impl.ProductApplicationImpl();
	}
	
	@Bean
	public WritableConfiguration configurationService() {
		return ConfigurationFileImpl.fromClasspath("/conf.properties");
	}

}

package com.dayatang.product.webapp.action.customer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.dayatang.excel.ExcelWriter;
import com.dayatang.product.webapp.action.BaseAction;

public class CustomerExportExcelAction extends BaseAction {
	
	private static final long serialVersionUID = 4829462469704471952L;
	
	private File outputFile;
	
	@Action(results = {
			@Result(name = "success", type = "stream", params = {"inputName", "inputStream","contentDisposition","attachment;filename=${downloadFileName}"}),
			@Result(name = "input", type = "redirect",location="/mail/html-mail-manage.action") })
	public String execute() throws Exception {
		outputFile = new File("客户信息模版.xls");
		ExcelWriter instance = new ExcelWriter(outputFile);
		List<Object[]> data = createData();
		instance.write("客户信息", 0, 0, data);
		return SUCCESS;
	}
	
	public InputStream getInputStream() throws FileNotFoundException {
		return new FileInputStream(outputFile);
	}

    public String getDownloadFileName() throws UnsupportedEncodingException{
        return new String("客户信息模版.xls".getBytes(), "ISO8859-1");
    }
    
    private List<Object[]> createData() {
		List<Object[]> results = new ArrayList<Object[]>();
		results.add(new Object[] {"客户名称", "电话", "邮箱", "行业类型", "地址", "公司网址", "客户说明"});
		return results;
	}

}
